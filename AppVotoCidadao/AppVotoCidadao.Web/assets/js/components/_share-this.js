/*
|--------------------------------------------------------------------------
| Share This
|--------------------------------------------------------------------------
*/

APP.component.ShareThis = {

    init : function (_url, _title, _image, _description, _message) {
        
        this.setup();
        this.setParametrosShare(_url, _title, _image, _description, _message);
                
    },


    setup : function () {

        //

    },

    setParametrosShare : function () {

        // $('.st-custom-button').data('data-url', _url);
        // $('.st-custom-button').data('data-short-url', _shortUrl);
        // $('.st-custom-button').data('data-title', _title);
        // $('.st-custom-button').data('data-image', _image);
        // $('.st-custom-button').data('data-description', _description);
        // $('.st-custom-button').data('data-username', _userName);
        // $('.st-custom-button').data('data-message', _message);

        this.sharethis();

    },

    sharethis : function() {

        $.getScript("//platform-api.sharethis.com/js/sharethis.js#property=5b0c66413dfc7c0011da0fc0&product=custom-share-buttons");
    	
    },


};

