
/*
|--------------------------------------------------------------------------
| Modal
|--------------------------------------------------------------------------
*/

APP.component.Modal = {

    init : function () {
        
        this.setup();
        this.modal();
        
    },


    setup : function () {

        //

    },

    modal : function() {
    
    	$('a[rel=modal]').on('click', function(event) {
            event.preventDefault();
            //$('.modal').center();
            $($(this).attr('href')+', #mask').fadeIn('fast', function() {
                //$('.modal').center();			
            });
        });

        $('body').on("click", ".modal .close, #mask", function(event) {
            event.preventDefault();
            if ($('.modal:visible').length > 1) {
                $('.modal#customAlert, #mask').fadeOut();                
            } else {
                $('.modal, #mask').fadeOut();
            }
            $('body').removeClass('lockScroll');
        });

        $('body').on("click", ".modal .close-img , #mask", function(event) {
            event.preventDefault();
            $('.modal, #mask').fadeOut();
            $('section').removeClass('blur');
            $('body').removeClass('lockScroll');
        });

        $('body').on("click", "#alert.modal", function(event) {
            event.preventDefault();
            $('.modal, #mask').fadeOut();
            $('body').removeClass('lockScroll');
        });

        $('.modal-termos').on('click', function(event) {
            event.preventDefault();

            $($(this).attr('href')+', #mask').fadeIn('fast', function() {
			
            });
        });

        $('body').on("click", ".modal-termos .close, #mask", function(event) {
            event.preventDefault();
            $('.modal-termos, #mask').fadeOut();
            $('body').removeClass('lockScroll');
        });

    	
    },

};

