/*
|--------------------------------------------------------------------------
| Check Connection
|--------------------------------------------------------------------------
*/

APP.component.CheckConnection = {

    init : function () {

        this.setup();
        //this.checkConnection();

    },

	setup : function () {

        window.addEventListener('online',  APP.component.CheckConnection.checkConnection);
        window.addEventListener('offline', APP.component.CheckConnection.checkConnection);
        APP.component.CheckConnection.noSignalEffect()
        //console.log(!navigator.onLine);

	},

	checkConnection : function () {

        if (!navigator.onLine) {
            $('body').addClass('lockScroll');
            $('body #check-connection').css('display', 'flex').hide().fadeIn(400);
        } else {
            $('body').removeClass('lockScroll');
            $('body #check-connection').fadeOut(400);		
        }

	},

    noSignalEffect : function () {
        var NoSignal = ( function () {
            var canvas;
            var ctx;
            var imgData;
            var pix;
            var WIDTH;
            var HEIGHT;
            var flickerInterval;

            var init = function () {
                canvas = document.getElementById('no-signal-canvas');
                ctx = canvas.getContext('2d');
                canvas.width = WIDTH = 700;
                canvas.height = HEIGHT = 500;
                ctx.fillStyle = 'white';
                ctx.fillRect(0, 0, WIDTH, HEIGHT);
                ctx.fill();
                imgData = ctx.getImageData(0, 0, WIDTH, HEIGHT);
                pix = imgData.data;
                flickerInterval = setInterval(flickering, 30);
            };

            var flickering = function () {
                for (var i = 0; i < pix.length; i += 4) {
                    var color = (Math.random() * 255) + 50;
                    pix[i] = color;
                    pix[i + 1] = color;
                    pix[i + 2] = color;
                }
                ctx.putImageData(imgData, 0, 0);
            };

            return {
                init: init
            };
        }());

        NoSignal.init();
    },

    clickTentarNovamente: function () {
        $('body').on('click', '#check-connection .tentarNovamente', function(event) {
            event.preventDefault();
            APP.component.CheckConnection.checkConnection();
        });
    },
    
};

