APP.component.user = {

	get : function () {
		var playerId = localStorage.getItem(playerId);
	    var user = "";

	    $.ajax({
	        type: "GET",
	        data: {'playerId' : playerId},
	        dataType: 'json',
	        async: false,
	        url: '/MinhaConta/BuscaUsuario',
	        beforeSend: function () {
	        },
	        success: function (result) {
	            user = btoa(result.email);
	        },
	        error: function (result) {
	            //APP.component.Alert.customAlert(result.statusText, "Erro");
	        },
	        complete: function (result) {
	        }
	    });

        return user;

	},

}