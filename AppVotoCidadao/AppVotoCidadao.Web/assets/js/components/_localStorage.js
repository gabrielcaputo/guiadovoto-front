/*
|--------------------------------------------------------------------------
| Local Storage
|--------------------------------------------------------------------------
*/

APP.component.LocalStorage = {

    init : function () {

        this.setup();
        this.localStorage();

    },

    setup : function () {



    },

    localStorage : function () {

        this.setPlayerId();

    },

    //PlayerID
    setPlayerId : function () {

        var platform = APP.component.Utils.getUrlParameter('platform');
        var playerID = APP.component.Utils.getUrlParameter('playerID');

        // var applicationDirectory = APP.component.Utils.getUrlParameter('applicationDirectory');
        
        // if (applicationDirectory != undefined) {
        //     APP.component.LocalStorage.setLocalStorage('applicationDirectory', applicationDirectory);
        // }
        if (platform != undefined) {
            APP.component.LocalStorage.setLocalStorage('platform', platform);
        }
        if (platform != undefined) {
            APP.component.LocalStorage.setLocalStorage('playerID', playerID);
            if (typeof APP.component.Utils.getUrlParameter('redirect') === "string") {
                // window.location.href = "/" + atob(APP.component.Utils.getUrlParameter('redirect')) + "&redirected=true";
            } else {
                // window.location.href = "/";
            }
        }

    },

    //SET
    setLocalStorage : function (_name, _value) {

        localStorage.setItem(_name, _value);

    },

    //GET
    getLocalStorage : function (_name) {

        var value = localStorage.getItem(_name);

        return value;

    },

    //Remove
    removeLocalStorage : function (_name) {
      
        localStorage.removeItem(_name);

    },

    //Grava Porcentagem Minhas Prioridades
    setPorcentagemMinhasPrioridades : function (pagina) {

        // Pega o usuário e vincula ao objeto
        var playerId = localStorage.getItem(playerId);

        if (typeof localStorage.porcentagemMP === "undefined") {
            var obj = {}
        } else {
            var obj = JSON.parse(localStorage.porcentagemMP)
        }
        if (typeof obj[user] === "undefined") {
            obj[user] = []
            obj[user].push(pagina);
        } else {
            if (obj[user].indexOf(pagina) === -1) {
                obj[user].push(pagina);
            }
        }

        localStorage.setItem('porcentagemMP', JSON.stringify(obj));

    },

     //Grava Porcentagem Minhas Prioridades
    removePorcentagemMinhasPrioridades : function (pagina) {
        // Pega o usuário e vincula ao objeto
        var playerId = localStorage.getItem(playerId);

        var newObj = {};
        newObj[user] = []

        if (typeof localStorage.porcentagemMP === "undefined") {
            var obj = {}
        } else {
            var obj = JSON.parse(localStorage.porcentagemMP)
        }

        $(obj[user]).each(function (index) {
            if (this != pagina) {
                var text = obj[user][index];
                newObj[user].push(text);
            }
        });

        localStorage.setItem('porcentagemMP', JSON.stringify(newObj));

    },

};


