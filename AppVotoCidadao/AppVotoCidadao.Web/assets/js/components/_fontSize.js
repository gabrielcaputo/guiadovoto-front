/*
|--------------------------------------------------------------------------
| Date
|--------------------------------------------------------------------------
*/

APP.component.FontSize = {

    init : function () {

        this.setup();
        this.fontsize();

    },

    setup : function () {

        //

    },

    fontsize : function () {

        this.aumentarFont();
        this.diminuirFont();

    },

    aumentarFont : function () {
        
        $('.letter i.fa-plus').on('click', function () {

            var size = $(".capitulo .texto p").css('font-size');
            size = size.replace('px', '');
            maxSize = 20;

            if (size < maxSize) {
                size = parseInt(size) + 2;
                $('.capitulo .texto p').animate({'font-size' : size + 'px'});
            }


        });

    },

    diminuirFont : function () {

        $('.letter i.fa-minus').on('click', function () {

            var size = $(".capitulo .texto p").css('font-size');
            size = size.replace('px', '');
            minxSize = 12;

            if (size > minxSize) {
                size = parseInt(size) - 2;
                $('.capitulo .texto p').animate({'font-size' : size + 'px'});
            }

        });
 
    },



};


