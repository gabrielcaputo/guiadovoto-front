APP.component.Captcha = {

    init : function () {
        this.setup();
        this.actions();
    },

	setup : function () {

		var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9');
		var i;
	    for (i = 0; i < 3; i++) {
	        var a = '<span style="transform: skew(' + APP.component.Captcha.randomNumber(-60,60) + 'deg)">' + alpha[Math.floor(Math.random() * alpha.length)] + '</span>';
	        var b = '<span style="transform: skew(' + APP.component.Captcha.randomNumber(-60,60) + 'deg)">' + alpha[Math.floor(Math.random() * alpha.length)] + '</span>';
	        var c = '<span style="transform: skew(' + APP.component.Captcha.randomNumber(-60,60) + 'deg)">' + alpha[Math.floor(Math.random() * alpha.length)] + '</span>';
	        var d = '<span style="transform: skew(' + APP.component.Captcha.randomNumber(-60,60) + 'deg)">' + alpha[Math.floor(Math.random() * alpha.length)] + '</span>';
	    }
	    var code = a + b + c + d;
	    $('#captchaCode').html(code);  

	},

	reset : function () {
		$('#captchaCode').removeClass('invalid');
		$('#captchaCode').removeClass('valid');
		$('#captchaInput').val("");

	},

	actions : function () {
		$(document).ready(function() {
			$('#captchaInput').on('keyup', function(event) {
		    	// event.preventDefault();
	    		$('#captchaCode').removeClass('invalid');
	    		$('#captchaCode').removeClass('valid');
		    	if ($(this).val().length > 3) {
			    	if (APP.component.Captcha.validate()) {
			    		$('#captchaCode').addClass('valid');
			    	} else {
			    		$('#captchaCode').addClass('invalid');
			    	}
		    	}
		    });

		    $('body').on('click', '#captchaRefresh', function(event) {
		    	event.preventDefault();
		    	APP.component.Captcha.setup()
		    	APP.component.Captcha.reset()
		    });
		});
	},

	randomNumber : function (min,max) {
	    return Math.floor(Math.random()*(max-min+1)+min);
	},

    validate : function () {
    	var val1 = $('#captchaCode').text();
	    var val2 = $('#captchaInput').val();
	    if (val1 == val2) {
			return true;
	    } else{        
			return false;
	    }
    },
    
};