APP.component.BannerApp = {

    init : function () {

        this.setup();

    },

    setup : function () {
        if ((isMobile.apple.device || isMobile.android.device) &&
            typeof localStorage.platform === "undefined" &&
            typeof localStorage.bannerApp === "undefined"
        ) {

            var storeName = isMobile.apple.device ? 'App Store' : 'Play Store';
            var storeUrl = isMobile.apple.device ? 'https://itunes.apple.com/br/app/guia-do-voto/id1388489512?mt=8' : 'https://play.google.com/store/apps/details?id=br.org.guiadovoto';

            $('body').append(`
                <div class="bannerApp">
                    <a href="#" class="close"><i class="fas fa-times"></i></a>
                    <img src="/assets/img/logo-blue.png"/>
                    <div class="desc">
                        <div class="txt">Use o aplicativo e vote com consciência</div>
                        <div class="gratis">Grátis na ${storeName}</div>
                    </div>
                    <a href="${storeUrl}" class="store" target="_system">Acessar</a>
                </div>
            `);

            $('body').on('click', '.bannerApp .close', function(event) {
                event.preventDefault();
                $('.bannerApp').remove();
                localStorage.setItem('bannerApp', true);
            });

        }
    },
    
};