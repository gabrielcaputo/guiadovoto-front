/*
|--------------------------------------------------------------------------
| Date
|--------------------------------------------------------------------------
*/

APP.component.Loading = {

    init : function () {

        this.setup();
        this.loading();

    },

    setup : function () {

        //

    },

    loading : function () {

    },

    show : function () {

        $('.box-loading').stop().fadeIn();

    },

    hide : function () {

        $('.box-loading').stop().fadeOut();

    },

};


