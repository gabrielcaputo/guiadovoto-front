﻿/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

APP.component.Compartilhar = {
    
    init : function () {
        
        this.setup();
        this.compartilhar();
        
    }, 
    
    setup : function () {
        
        //
        
    },
    
    compartilhar : function () {
        
        //this.createCanvasCompartilhar();
        this.setClickModalCompartilhar();
        this.setClickCompartilhar();
        this.setClickSalvarMeuVoto();
        this.setBrickVote();
        
    },
    
    createCanvasCompartilhar : function () {
        
        var _namePage = $('#compartilhar').data('compartilhar');
        APP.component.Compartilhar.setControllerHtml(_namePage);
        APP.component.Compartilhar.setCanvas();
        
    },
    
    setClickModalCompartilhar : function () {
        
        $("#compartilhar").on('click', function (event) {
            
            event.preventDefault();
            APP.component.Alert.compartilhar();
            
            var _namePage = $(this).data('compartilhar');
            APP.component.Compartilhar.setControllerHtml(_namePage);
            APP.component.Compartilhar.setCanvas();
            
        });
        
    },
    
    setClickCompartilhar : function () {
        
        $("[id^=compartilhar-btn]").on('click', function (event) {
            
            event.preventDefault();
            
            var _redeSocial = $(this).attr('class');
            
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                setTimeout(function() {
                    APP.component.Compartilhar.setUrlCompartilhar(_redeSocial)
                }, 500);
            }
            
        });
        
    },
    
    setCanvas : function () {
        
        var element = $("#preview-compartilhar")[0];
        var config = {
            width: 600,
            height: 315
        }
        
        html2canvas(element,config).then(function(canvas) {
            //$("#previewImage").append(canvas);
            
            var imgageData = canvas.toDataURL("image/png");
            $('.img-render').attr('src',imgageData)
            
            APP.component.Compartilhar.saveImgCompartilhar(imgageData);
            
            
        });
        
    },
    
    saveImgCompartilhar : function (img) {
        
        var erro = "";
        
        //Display the key/value pairs
        // for (var pair of avatar.entries()) {
        //     console.log(pair); 
        // }
        
        var formdata = new FormData();
        formdata.append("base64image", img);
        
        $.ajax({
            type: "POST",
            data: formdata,
            dataType: 'json',
            processData: false,
            contentType: false,
            url: "/Compartilhar/SalvarImagem",
            beforeSend: function () {
                //APP.component.Loading.show();
            },
            success: function (result) {
                
            },
            error: function (result) {
                //APP.component.Loading.hide();
            },
            complete: function (result) {
                //APP.component.Loading.hide();
            }
        }).promise().done(function(result) {
            //APP.component.Compartilhar.setUrlCompartilhar(result);
            $('#compartilhar').data('img',result.path);
        });
        
    },
    
    setUrlCompartilhar : function (_redeSocial) {
        
        var title = 'Guia do Voto';
        var desc = 'Veja a ordem das minhas prioridades e o que eu mais valorizo.';
        var img = $('#compartilhar').data('img');
        var url = 'http://guiadovoto.org.br';
        
        // var URLShareEncode = encodeURI(url + '/Compartilhar?title=' + title + '&desc=' + desc + '&url='+url+'&img='+img);
        // console.log(URLShareEncode);
        
        var URLShare = '';
        var URLEncode = encodeURIComponent(url + '/Compartilhar?title=' + title + '&desc=' + desc + '&url='+ url +'&img='+ img);
        
        switch(_redeSocial) {
            case 'facebook':
            URLShare = 'https://www.facebook.com/dialog/share?app_id=247591319322843&display=popup&href=' + URLEncode;
            break;
            case 'linkedin':
            URLShare = 'https://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(url + '/Compartilhar?title=' + title + '&desc=' + desc + '&url='+ url +'&img='+ img);
            break;
            case 'twitter':
            URLShare = 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(title + ' - ' + desc + ' - Acesse: www.guiadovoto.org.br');
            break;
            case 'whatsapp':
            URLShare = 'https://api.whatsapp.com/send?text=' + encodeURIComponent(title + ' - ' + desc + ' - Acesse: www.guiadovoto.org.br');
            break;
        }
        
        setTimeout(function() { 
            window.open(URLShare, '_system');
        }, 500);
        
    },
    
    /*
    |--------------------------------------------------------------------------
    | Controller de Copy
    |--------------------------------------------------------------------------
    */
    setControllerHtml : function (_namePage) {
        
        switch(_namePage) {
            case 'OQueEuValorizo':
            APP.component.Compartilhar.setCopyHtmlOQueEuValorizo();
            break;
            case 'MeuVoto':
            APP.component.Compartilhar.setCopyHtmlMeuVoto();
            break;
        }
        
    },
    
    setCopyHtmlOQueEuValorizo : function () {
        
        var copy = $('#step-03').find('.resultado-temas').html();
        var html = `
        <div class="container">
            <div class="infos">
                <h1>O que eu valorizo</h1>
            </div>
            <div class="carousel-steps">
            <div class="item" id="step-03">
            <div class="wrap">
                <p class="title">
                Pronto! Agora veja a ordem das suas prioridades e descubra o que você mais valoriza:
                </p>
                <div class="temas">
                <ul class="resultado-temas">
                ${copy}
                </ul>
                </div>
                </div>
                </div>
            </div>
        </div>
        `;
        
        $('#preview-compartilhar').html(html);
        
    },
    
    /*
    |--------------------------------------------------------------------------
    | Salvar Imagem
    |--------------------------------------------------------------------------
    */
    setClickSalvarMeuVoto : function () {
        
        $(".salvar-imagem").on('click', function (event) {
            
            event.preventDefault();
            
            var _namePage = $(this).data('compartilhar');
            APP.component.Compartilhar.setControllerHtml(_namePage);
            APP.component.Compartilhar.setCanvasSalvarImagem();
            
        });
        
    },
    
    
    setCopyHtmlMeuVoto : function () {
        
        $('#preview-download-img').html('');
        
        var lista = $('.lista-candidatos').html();
        
        var candidatos = '';
        
        $('.lista-candidatos .item').each(function () {
            var cargo = $(this).find('.cargo').text();
            var nome = $(this).find('span.nome').first().text();
            var numero = $(this).find('span.numero').first().text();
            
            if ($(this).find('.info-reduzida .nome').text() != '') {
                
                candidatos += `
                <div class="item">
                <div class="cargo">${cargo}</div>
                <div class="nome"><span>${nome}</span></div>
                <div class="numero selecionado">
                <span>${numero}</span>
                </div>
                <div class="confirma">
                <span>Confirma</span>
                </div>
                </div>
                `;
                
                
            } else {
                candidatos += `
                <div class="item">
                    <div class="cargo">${cargo}</div>
                    <div class="nome sem-candidato"></div>
                    <div class="numero sem-candidato">
                        <span></span>
                        <span></span>
                    ${(
                        cargo.toLowerCase().indexOf('senador') >= 0 ||
                        cargo.toLowerCase().indexOf('deputado federal') >= 0 ||
                        cargo.toLowerCase().indexOf('deputado estadual') >= 0
                        ? `<span></span>`
                        : ``
                        )}
                    ${(
                        cargo.toLowerCase().indexOf('deputado federal') >= 0 ||
                        cargo.toLowerCase().indexOf('deputado estadual') >= 0
                        ? `<span></span>`
                        : ``
                        )}
                        ${(
                            cargo.toLowerCase().indexOf('deputado estadual') >= 0
                            ? `<span></span>`
                            : ``
                            )}
                            </div>
                            </div>
                            `;
                        }
                    })
                    
                    // var lembrese = $('.lembrese').html();
                    var html = `
                    <div class="wrap-bordered">
                    <div class="wrap">
                    <div class="header">
                        <img src="/assets/img/logo-guia-do-voto.png" />
                        <span>Meu Voto</span>
                    </div>
                    <div class="lista-candidatos">
                        ${candidatos}
                    </div>
                    <div class="lembrese">
                        <p class="orange">Lembre-se:</p>
                        <ul>
                            <li>
                                <span class="circle"></span>
                                O voto é secreto.
                            </li>
                            <li>
                                <span class="circle"></span>
                                     O 2º turno das eleições 2018 acontece dia 28 de outubro,
                                    das 8 às 17h (horário local).
                            </li>
                            <li>
                                <span class="circle"></span>
                                Para votar é necessário apresentar um documento oficial com foto (recomenda-se levar também o título de eleitor).
                            </li>
                            <li>
                                <span class="circle"></span>
                                A Justiça Eleitoral proíbe o uso de celulares na cabine de votação e recomenda que os eleitores levem os dados dos candidatos anotados em papel.
                            </li>
                        </ul>
                        <p>Boa eleição!</p>
                    </div>
                    </div>
                    `;
                    
                    $('#preview-download-img').html(html);
                    
                },
                
                setCanvasSalvarImagem : function () {
                    
                    APP.component.Loading.show();
                    
                    var element = $("#preview-download-img")[0];
                    var config = {
                        
                        scale: 2,
                        // onrendered: function(canvas)
                        // {
                        //     alert('show');
                        // }
                        
                    }
                    html2canvas(element,config).then(function(canvas) {
                        
                        if (typeof cordova == "object") {
                            var base64String = canvas.toDataURL("image/jpeg");
                            var params = {
                                data: base64String,
                                prefix: 'Guia do Voto - Meu Voto - ',
                                format: 'JPG',
                                quality: 100,
                                mediaScanner: true
                            };
                            
                            window.imageSaver.saveBase64Image(params,
                                function (filePath) {
                                    APP.component.Alert.alert('Lista salva com sucesso!<br> Verifique em sua galeria de fotos.')
                                },
                                function (msg) {
                                    APP.component.Alert.alert('Ops... não foi possível salvar sua lista. Por favor, tente novamente.')
                                }
                                );
                                
                            } else {
                                
                                $('body').append('<a target="_blank" href="' + canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream") + '" class="downloadMeuVoto" download="Guia do Voto - Meu Voto.jpg"></a>');
                                $('body a.downloadMeuVoto')[0].click();
                                $('body a.downloadMeuVoto').remove();
                                
                            }
                            
                            
                            APP.component.Loading.hide();
                            
                            
                            
                        });
                        
                        $('.selecionado span').each(function(){
                            var _this = this;
                            if (_this.textContent.length == 5) {
                                $(this).addClass('cinco');
                            }
                            else if (_this.textContent.length == 4) {
                                $(this).addClass('quatro');
                            }
                            else if (_this.textContent.length == 3) {
                                $(this).addClass('tres');
                            }
                            else {
                                $(this).addClass('dois');
                            }
                            
                        })
                        
                        
                        
                    },
                    
                    setBrickVote: () => {
                        $('.selecionado span').each(function(){
                            var _this = this;
                            if (_this.textContent.length == 4) {
                                $(this).addClass('quatro');
                            }
                        })
                    }
                };