/*
|--------------------------------------------------------------------------
| Form
|--------------------------------------------------------------------------
*/

APP.component.Form = {

    init : function () {

        this.setup();
        this.formulario();

    },

    setup : function () {



    },

    formulario : function () {

        //this.setOptionsEstado();

    },

    setOptionsEstado : function () {

        $.getJSON('../json/estados_cidades.json', function (data) {
            var items = [];
            var options = '<option value="">escolha um estado</option>';	
            $.each(data, function (key, val) {
                options += '<option value="' + val.nome + '">' + val.nome + '</option>';
            });					
            $("#estados").html(options);				

            $("#estados").change(function () {				
            
                var options_cidades = '';
                var str = "";					
                
                $("#estados option:selected").each(function () {
                    str += $(this).text();
                });
                
                $.each(data, function (key, val) {
                    if(val.nome == str) {							
                        $.each(val.cidades, function (key_city, val_city) {
                            options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                        });							
                    }
                });
                $("#cidades").html(options_cidades);
                
            }).change();		
        
        }).then(function () {

            APP.component.Form.setSelect();

        });

    },

    setSelect : function () {

        $('select').customSelect({
            search: true
          });

    },
    
    setCustomSelect : function () {
        
        $('select').each(function(){
            var $this = $(this), numberOfOptions = $(this).children('option').length;
        
            $this.addClass('select-hidden'); 
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-styled"></div>');

            var $styledSelect = $this.next('div.select-styled');
            $styledSelect.text($this.children('option').eq(0).text());
        
            var $list = $('<ul />', {
                'class': 'select-options'
            }).insertAfter($styledSelect);
        
            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }
        
            var $listItems = $list.children('li');
        
            $styledSelect.click(function(e) {
                e.stopPropagation();
                $('div.select-styled.active').not(this).each(function(){
                    $(this).removeClass('active').next('ul.select-options').hide();
                });
                $(this).toggleClass('active').next('ul.select-options').toggle();
            });
        
            $listItems.click(function(e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                //console.log($this.val());
            });
        
            $(document).click(function() {
                $styledSelect.removeClass('active');
                $list.hide();
            });

        });
    },

};


