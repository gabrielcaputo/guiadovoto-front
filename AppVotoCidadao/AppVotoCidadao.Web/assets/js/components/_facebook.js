
/*
|--------------------------------------------------------------------------
| Facebook
|--------------------------------------------------------------------------
*/

APP.component.Facebook = {

    init : function () {

        this.setup();
        this.facebook();

    },


    setup : function () {



    },

    facebook : function () {

        APP.component.Facebook.app();
        //APP.component.Facebook.loginStatus();
        //APP.component.Facebook.connect();

    },

    connect : function () {

        // Após o aceite do app no perfil do facebook, retorna com uma url com o parametro code
        var code = APP.component.Utils.getUrlParameter('code');

        if (code != undefined) {

            APP.component.Facebook.setConnect();

        }

    },

    setConnect : function () {

        APP.component.Loading.show();

        //Verifica Controller pela URL
        var controller = $('meta[name=controller]').attr('content');
        
        // var client_id = '858598314350118'; // HOMOLOG 
        var client_id = '247591319322843'; // PROD

        // var redirect_uri = 'https://13f56272.ngrok.io/'+controller; // HOMOLOG 
        // var redirect_uri = 'https://4fd95b97.ngrok.io/'+controller+''; // HOMOLOG 
        var redirect_uri = 'https://guiadovoto.org.br/'+controller; // PROD

        // var secretKey = 'b96f7a40cf1a970ac871efde8a58827f'; // HOMOLOG 
        var secretKey = '83a4f07e37afe198fa4a2fa8746c42eb'; // PROD

        var code = APP.component.Utils.getUrlParameter('code');

        // Dá um post na graph api do facebook pra pegar o access token a partir do parametro code
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": 'https://graph.facebook.com/v3.0/oauth/access_token?' 
                    + 'client_id='+ client_id 
                    + '&redirect_uri=' + redirect_uri 
                    + '&client_secret=' + secretKey
                    + '&code=' + code,
            "method": "GET"
        }

        $.ajax(settings).done(function (response) {

            // console.log(FB)

            // Retorna o access token do Facebook pra poder pegar os dados do usuário
            FB.api('/me', {access_token: response.access_token, fields: 'email'}, function(response) {
                // Usa a api javascript pra pegar o email e o id do usuário

                //Verifica se o usuário já tem conta, caso não tenha preenche o cadastro
                APP.component.Facebook.verificarFacebook(response, controller);
                APP.component.Loading.hide();

            });

        }).fail(function(response) {

            // window.location = window.location.pathname;

            // Expirou a chamada ou perdeu acesso
            // console.log(response)
            // console.log(JSON.parse(response.responseText).error.message);
        });

    },

    verificarFacebook : function (response, controller) {

        var url = '';
        var data = {};

        if (controller == 'Cadastro'){
            url = '/Cadastro/VerificarFacebook';
            data = {
                'email': response.email,
                'facebookId': response.id, 
                'playerId': APP.component.LocalStorage.getLocalStorage('playerID'),
            }
        } else {
            url = '/Login/LoginFacebook';
            data = {
                'facebookId': response.id, 
                'playerId': APP.component.LocalStorage.getLocalStorage('playerID'),
            }
        }

        $.ajax({
            type: "POST",
            data: data,
            async: false,
            dataType: 'json',
            url: url,
            beforeSend: function () {
                
            },
            success: function (result) {

                if (result.authenticated == true) {
                    window.location.href = "/Home";
                } else {
                    $('input#email').val(response.email);
                    $('input#email').prop('readonly', true)
                    $('.btn.facebook').addClass('connected');
                    localStorage.setItem('email-fb', response.email);
                    localStorage.setItem('id-fb', response.id);
                }
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                
            }
        });

    },

    app : function () {

        window.fbAsyncInit = function() {
            FB.init({
              //appId            : '858598314350118',
              appId            : '247591319322843',
              autoLogAppEvents : true,
              xfbml            : true,
              version          : 'v3.0'
            });

            APP.component.Loading.hide();
            APP.component.Facebook.connect();
          };
        
          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));

    },

    loginStatus : function () {

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    
    },

    login : function () {

        FB.getLoginStatus(function(response) {

            if (response.status === 'connected') {
              //console.log('Logged in.');
            }
            else {
                //console.log('Login.');
              FB.login();
            }
          });

    },
    
};

