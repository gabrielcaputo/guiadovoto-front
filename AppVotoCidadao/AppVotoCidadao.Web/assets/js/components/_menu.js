/*
|--------------------------------------------------------------------------
| Menu
|--------------------------------------------------------------------------
*/

APP.component.Menu = {

    init : function () {
        
        this.setup();
        this.menu();

    },

    setup : function () {

        

    },

    menu : function () {

        this.openMenu();
        this.goBack();
        this.logout();

    },

    openMenu : function () {

        $('.open-menu').on('click', function (event) {

            event.preventDefault();

            $('.menu').toggleClass('active');
            $('.open-menu').toggleClass('active');
            $('section').toggleClass('blur');

        });

    },

    goBack : function () {

        $('.back').on('click', function (event) {
            
            event.preventDefault();
            var voltar = $('.back').attr('href');
            if (voltar == '#') {
                window.history.back();
            } else {
                window.location.href = ''+voltar+'';
            }

        });

    },

    //Logout
    logout : function () {

        $('.menu .sair').on('click', function (event) {
            event.preventDefault();

            var title = 'Atenção';
            var msg = 'As atividades realizadas por você no Guia do Voto não são armazenadas. Ao clicar em <strong>“SAIR”</strong>, os dados da sua navegação não estarão disponíveis em seu próximo acesso neste ou em outro aparelho, e você deverá refazer as atividades.';

            var htmlForm = `
                <a href="/Login/Logout" class="btn sair">
                    <span>SAIR</span>
                </a>
                <a href="#" class="btn close">
                    <span>CANCELAR</span>
                </a>
            `;

            APP.component.Alert.customGuiaDoVoto(title, msg, htmlForm, false, false, '/assets/img/login/alerta_dados_sair.png');
            
            // $.ajax({
            //     type: "GET",
            //     dataType: 'json',
            //     url: "/Login/Logout",
            //     beforeSend: function () {
                    
            //     },
            //     success: function (result) {
            //         localStorage.clear();
            //         window.location.href = '/Inicio';
            //     },
            //     error: function (result) {
                    
            //     },
            //     complete: function (result) {
                    
            //     }
            // });

        });

        $('#customGuiaDoVoto').on('click', '.sair', function(event) {

            if (platform != 'null' && typeof localStorage.platform !== "undefined") {
                var platform = localStorage.getItem('platform');
            }
            if (playerId != 'null' && typeof localStorage.playerId !== "undefined") {
                var playerId = localStorage.getItem('playerID');
            }
            if (bannerApp != 'null' && typeof localStorage.bannerApp !== "undefined") {
                var bannerApp = localStorage.getItem('bannerApp');
            }
          
            localStorage.clear();

            if (typeof platform !== "undefined") {
                localStorage.setItem('platform',platform);
            }
            if (typeof playerId !== "undefined") {
                localStorage.setItem('playerID',playerID);
            }
            if (typeof bannerApp !== "undefined") {
                localStorage.setItem('bannerApp',bannerApp);
            }
            
        });

    },

};


