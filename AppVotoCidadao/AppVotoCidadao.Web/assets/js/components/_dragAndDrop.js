/*
|--------------------------------------------------------------------------
| Drag And Drop
|--------------------------------------------------------------------------
*/

APP.component.DragAndDrop = {

    init : function () {

        this.setup();
        this.dragAndDrop();

    },

    setup : function () {

        //

    },

    dragAndDrop : function () {

        var obj = document.getElementById('ajuda');
        obj.addEventListener('touchmove', function(event) {
        // If there's exactly one finger inside this element
        if (event.targetTouches.length == 1) {
            var touch = event.targetTouches[0];
            // Place element where the finger is
            obj.style.left = touch.pageX + 'px';
            obj.style.top = touch.pageY + 'px';
        }
        }, false);

    }

};


