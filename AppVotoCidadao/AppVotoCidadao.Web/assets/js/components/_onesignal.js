APP.component.OneSignal = {

    init : function () {

        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false)
        
    },

    onDeviceReady : function() {

        // var ambiente = "https://19ee38b6.ngrok.io/"
        // var ambiente = "http://guiadovoto.fullbarhomologa.com.br/"
        var ambiente = "https://guiadovoto.org.br/"
    
        var url = "";

        var notificationOpenedCallback = function(jsonData) {

            function onConfirm(buttonIndex) {
                if (buttonIndex === 1) {
                    url = jsonData.notification.payload.additionalData.url;
                    if (url.indexOf('?') > 0) {
                        window.location = ambiente + url + "&redirected=true";
                    } else {
                        window.location = ambiente + url + "?redirected=true";
                    }
                }
            }

            navigator.notification.confirm(
                jsonData.notification.payload.body,
                onConfirm,
                jsonData.notification.payload.title,
                ['Ok','Cancelar']
            );


        };

        var notificationReceivedCallback = function(jsonData) {

            function onConfirm(buttonIndex) {
                if (buttonIndex === 1) {
                    url = jsonData.payload.additionalData.url;
                    if (url.indexOf('?') > 0) {
                        window.location = ambiente + url + "&redirected=true";
                    } else {
                        window.location = ambiente + url + "?redirected=true";
                    }
                }
            }

            navigator.notification.confirm(
                jsonData.payload.body,
                onConfirm,
                jsonData.payload.title,
                ['Ok','Cancelar']
            );
        };

        window.plugins.OneSignal
            .startInit("edd4305d-8d3a-44e1-b783-ffde36160094")
            .handleNotificationOpened(notificationOpenedCallback)
            .handleNotificationReceived(notificationReceivedCallback)
            .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
            .endInit();        
        
    },

};
