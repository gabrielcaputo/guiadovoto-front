
/*
|--------------------------------------------------------------------------
| Alert
|--------------------------------------------------------------------------
*/

APP.component.Alert = {

    init : function () {

        this.setup();

    },


    setup : function () {



    },

    generico : function (_name) {

        var m = $('.modal'+_name+'');

        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });

    },

    alert : function (message, title, action, actionTxt) {

        var m = $(".modal#alert");
    
        // Reset
        m.find(".title, .actions").hide();
        m.find(".actions a.action").attr("class", "btn action");
        m.find(".message, .title, .actions a.action").html("");
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }
        if (action) {
            m.find(".actions").show();
            m.find(".actions a.action").addClass(action);
            m.find(".actions a.action").html(actionTxt);
        }
        m.find(".message").html(message);
    
        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });
    
    },

    customAlert : function (message, title, action, actionTxt) {

        var m = $(".modal#customAlert");
    
        // Reset
        m.find(".title, .actions").hide();
        m.find(".actions a.action").attr("class", "btn action");
        m.find(".message, .title, .actions a.action").html("");
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }
        if (action) {
            m.find(".actions").show();
            m.find(".actions a.action").addClass(action);
            m.find(".actions a.action").html(actionTxt);
        }
        m.find(".message").html(message);
    
        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });
    
    },

    //Alerta de Login caso não logado
    alertaLogin : function (title, message, classe, html) {

        var m = $(".modal#alerta-login");
    
        // Reset
        m.find(".title, .message, .html").hide();
        m.find(".title, .message, .html").html("");
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }
        if (message) {
            m.find(".message").show();
            m.find(".message").html(message);
        }
        if (html) {
            m.find(".html").show();
            m.find(".html").html(html);
        }
        if (classe) {
            m.find(".btn").addClass(classe);
        }
    
        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });
    
    },
    
    customConfirm : function (message, title, confirmAction, confirmTxt, cancelAction, cancelTxt) {
        
        var m = $(".modal#customConfirm");
    
        // Reset
        m.find(".title").hide();
        m.find(".actions").show();
        m.find(".actions a.false").attr("class", "btn false");
        m.find(".actions a.true").attr("class", "btn true");
        m.find(".message, .title, .actions a.btn").html("");
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }
    
        m.find(".actions a.false").addClass(cancelAction);
        m.find(".actions a.false").html(cancelTxt);
    
        m.find(".actions a.true").addClass(confirmAction);
        m.find(".actions a.true").html(confirmTxt);
    
        m.find(".message").html(message);
    
        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });
    
    },

    customGuiaDoVoto : function (title, message, html, nameBtn, classBtn, img) {
        
        var m = $(".modal#customGuiaDoVoto");
        
        // Reset
        m.find(".title").hide();
        m.find(".html").hide();
        m.find(".btn").hide();
        m.find(".message, .title, .html").html("");
        m.find("a.btn span").text("");
        
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }

        if (html) {
            m.find(".html").show();
            m.find(".html").html(html);
        }

        if (nameBtn) {
            m.find(".btn").show();
            m.find(".btn span").text(nameBtn);
            m.find(".btn").addClass(classBtn);
        }

        if (img) {
            m.find('div.persona img').attr('src', img)
        } else {
            m.find('div.persona img').attr('src', '/assets/img/login/esqueci-minha-senha.png')
        }
    
        m.find(".message").html(message);
    
        // $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        // });

        if ($(window).width() > 960) {
            m.addClass('modal-desk');
            m.find('.close img').attr('src', '/assets/img/icons/icon-close-orange.png');
        }
    
    },

    customHTML : function (title, message, html) {

        var m = $(".modal#customHTML");
    
        // Reset
        m.find(".title").hide();
        m.find(".message, .title").html("");
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }
        if (message) {
            m.find(".message").show();
            m.find(".message").html(message);
        }
        if (html) {
            m.find(".html").show();
            m.find(".html").html(html);
        }

       
    
        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });
    
    },

    customTermos : function (title, message) {
        
        var m = $(".modal-termos#customTermos");
        
        // Reset
        m.find(".title").hide();
        m.find(".message").hide();
        m.find(".message, .title").html("");
        
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }

        if (message) {
            m.find(".message").show();
            m.find(".message").html(message);
        }
    
        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });
    
    },

    modalRankingPartidos : function (sigla, nome, imgNome, link) {

        var m = $(".modal#partido");

        m.find('.desc-partido img').attr('src', '/assets/img/seeufosseumpolitico/partidos/'+imgNome+'.png');
        m.find('.desc-partido p .sigla').text(sigla);
        m.find('.desc-partido p .nome').text(nome);
        m.find('.desc-partido a').attr('href', 'http://' + link);
    
        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });

    },

    dadosNaoRegistrados : function () {

        var m = $(".modal#nao-registrados");

        $("#mask").fadeIn(200, function() {
            m.center();
            $('body').addClass('lockScroll');
            m.fadeIn(200, function() {
                m.center();
            });		
        });

    },

    modalInformacoes : function (title, message, img, classe) {

        var m = $(".modal#inforamcoes");

        // Reset
        m.find(".title").hide();
        m.find(".message, .title").html("");
    
        if (title) {
            m.find(".title").show();
            m.find(".title").html(title);
        }
        if (message) {
            m.find(".msg").show();
            m.find(".msg").html(message);
        }
        if (img) {
            m.find(".img").show();
            m.find(".img").attr('src', img);
        } else {
            m.find(".img").hide();
        }

        if (classe) {
            m.find(".conteudo").addClass(classe)
        }

       
    
        $("#mask").fadeIn(200, function() {
            //m.center();
            m.fadeIn(200, function() {
                //m.center();
            });		
        });
    
    },

    //Modal - O que eu valorizo (Temas)
    oQueEuValorizoTemas: function (_lista, _value, _ids) {

        var m = $(".modal#oqueeuvalorizo-temas");
        
        html = '';

        $(_lista).each(function (e) {

            html += '<li data-id="' + _ids[e] +'" data-value="'+_value[e]+'">'+_lista[e]+'</li>';

        });

        m.find('.lista-esolhida').html(html);

        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });

    },

    meuCandidatoIdeal : function () {

        var m = $(".modal#meu-candidato-ideal");

        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });

    },

    //Meus Candidatos

    //- Avaliar minhas opcoes
    alertaOQueEuValorizo : function () {

        var m = $(".modal#go-to-o-que-eu-valorizo");

        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });

    },

    //COMPARTILHAR
    compartilhar : function () {

        var m = $(".modal#modalCompartilhar");

        $("#mask").fadeIn(200, function() {
            m.center();
            m.fadeIn(200, function() {
                m.center();
            });		
        });

    },

    
};

