/*
|--------------------------------------------------------------------------
| Controller Ranking Partidos
|--------------------------------------------------------------------------
*/

APP.controller.RankingPartidos = {

    init : function () {

        this.setup();
        this.rankingPartidos();

    }, 

    setup : function () {

        //

    },

    rankingPartidos : function () { 
        
        this.getResultado();
        this.setClickFiltroNumber();
        this.setClickFiltroLetter();
        this.setScrollResultado();
        this.setClickRefazer();
        this.setClickPartido();

    },

    //GET
    getResultado : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/SeEuFosseUmPolitico/Resultado',
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.RankingPartidos.setInfosResultado(result);
            },
            error: function (result) {
            
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {
            tinysort('.lista-ranking .item ',{selector:'.full',data:'number', order:'desc'});
            APP.controller.RankingPartidos.setClickPartido();
        });

    },

    setInfosResultado : function (resultado) {
        
        $(resultado.partidos).each(function (e) {

            var css = APP.controller.RankingPartidos.setGradientColor(this.resultado);

            var html = `
                <div class="item">
                    <div class="name" data-link="${this.url}">
                        <span>${this.sigla}</span>
                        <p>${this.nome}</p>
                    </div>
                    <span class="full" data-number="${this.resultado}" style="color: #${css}">${this.resultado}%</span>
                </div>
            `;
    
            $('.lista-ranking').append(html);

        });

        //Texto Botao
        if (resultado.completo) {
            $('.refazer span').text('Refazer Resultado');
        }        

    },

    setGradientColor : function (number) {

        var color1 = '0000BF';
        var color2 = '06E3FF';
        var ratio = 0;

        if (number < 10) {
            ratio = parseFloat('0.0');
        } else {
            ratio = parseFloat('0.'+number);
        }

        var hex = function(x) {
            x = x.toString(16);
            return (x.length == 1) ? '0' + x : x;
        };

        var r = Math.ceil(parseInt(color1.substring(0,2), 16) * ratio + parseInt(color2.substring(0,2), 16) * (1-ratio));
        var g = Math.ceil(parseInt(color1.substring(2,4), 16) * ratio + parseInt(color2.substring(2,4), 16) * (1-ratio));
        var b = Math.ceil(parseInt(color1.substring(4,6), 16) * ratio + parseInt(color2.substring(4,6), 16) * (1-ratio));


        if (number != 100) {
            var cor = hex(r) + hex(g) + hex(b);
        }

        return cor;

    },

    //INTERACAO
    setClickFiltroNumber : function () {

        $('.filtros .number').on('click', function (event) {

            event.preventDefault();

            tinysort('.lista-ranking .item ',{selector:'.full',data:'number', order:'desc'});
            $('.filtros div').removeClass('active');
            $(this).addClass('active');
            

        });

    },

    setClickFiltroLetter : function () {

        $('.filtros .letter').on('click', function (event) {
            
            event.preventDefault();

            tinysort('.lista-ranking .item ',{selector:'.name p'});
            $('.filtros div').removeClass('active');
            $(this).addClass('active');

        });

    },

    setScrollResultado : function () {

        $(window).scroll(function(){
            if ($(window).scrollTop() > 300) {
                $('.actions').fadeOut();
            } else {
                $('.actions').fadeIn();
            };
            
        });

    },

    setClickRefazer : function () {

        $('.refazer').on('click', function () {

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/SeEuFosseUmPolitico/MaisVotacoes",
                beforeSend: function () {
                    
                },
                success: function (result) {
                    window.location.href = "/SeEuFosseUmPolitico";
                },
                error: function (result) {
                    //APP.component.Alert.customAlert('', "Erro");
                },
                complete: function (result) {
                    
                }
            });

        });
        
    },

    setClickPartido : function () {

        $('.lista-ranking .item').on('click', function(event) {

            event.preventDefault();

            var sigla = $(this).find('.name span').text();
            var nome = $(this).find('.name p').text();
            var imgName = APP.component.RemoveAcentos.init($(this).find('.name p').text().toLowerCase());
            var link = $(this).find('.name').data('link');

            APP.component.Alert.modalRankingPartidos(sigla, nome, imgName, link);

        });

    },

 
};
