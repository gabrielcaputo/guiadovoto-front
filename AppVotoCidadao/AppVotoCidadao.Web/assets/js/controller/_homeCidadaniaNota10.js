/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

APP.controller.CidadaniaNota10 = {

    init : function () {

        this.setup();
        this.cidadaniaNota10();

    }, 

    setup : function () {

        //

    },

    cidadaniaNota10 : function () { 

        this.getQuizzes();

    },

    getQuizzes : function () {
                
        var login = APP.component.Utils.getLogin();
        var url ='';

        if (login) {
            url = '/Quiz/ListaQuizMenu';
        } else {
            url = '/QuizDegustacao/GetQuiz';
        }

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: url,
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {

                if (login) {
                    APP.controller.CidadaniaNota10.setQuizzes(result);
                    APP.controller.Home.setClickEtapas();
                } else {
                    APP.controller.CidadaniaNota10.setQuizDegustacao(result);
                    APP.controller.CidadaniaNota10.setClickQuizDegustacao();
                    APP.controller.Home.setClickEtapas();
                }

                setTimeout(function() {
                    if (window.location.hash === "#quizzes") {
                        $('.cidadania .title').first().click()
                        $('.item .cover').first().click();
                    }                    
                }, 100);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });

    },

    setQuizzes : function (listaQuiz) {

        var quiz = listaQuiz.quizList;
        var quizRespondidos = 0;

        $(quiz).each(function(e){
            
            var html = '';
            var acertos = 0;
            var respondidas = 0;

            html += '<!-- Item -->';
            html += '<div class="item">';

            html +=     '<div class="cover">';
            html +=         '<img src="assets/img/home/cover-quiz-cap-0'+(e+1)+'.png" alt="Cover Capítulo 0'+(e+1)+'" />';
            html +=     '</div>';
            html +=     '<div class="info">';
            html +=         '<h3>'+this.assunto+'</h3>';
            html +=         '<div class="progression">';
            html +=             '<ul>';
            $(this.perguntas).each(function (i) {

                var classe = '';
                var pergunta = this;
                //Progresso
                if (pergunta.acertou === true) {
                    classe = 'right';
                    acertos ++;
                    respondidas++;
                } else if (pergunta.acertou === false) {
                    classe = 'wrong';
                    respondidas++;
                } else {
                    classe = '';
                }
                html += '<li class="'+classe+'"></li>';

            })
            html +=             '</ul>';
            html +=         '</div>';
            html +=         '<div class="status">';
            html +=             '<!-- Numeros -->';
            html +=             '<div class="numbers">';
            html +=                 '<p>';
            html +=                     'Respondido <span class="init">'+respondidas+'</span> <span>de</span> <span class="end">'+this.perguntas.length+'</span>';
            html +=                 '</p>';
            html +=             '</div>';
            html +=             '<!-- Porcentagem -->';
            html +=             '<div class="percentage">';
            html +=                 '<p>';
                                        var porcentagem = (100 * acertos) / this.perguntas.length;
            html +=                     'Acertos <span class="total">'+porcentagem.toFixed(0)+'</span> <span>%</span>';
            html +=                 '</p>';
            html +=             '</div>';
            html +=         '</div>';
            html +=         '<p class="desc">';
            html +=             ''+this.titulo+'';
            html +=         '</p>';
            html +=         '<a href="/Quiz?id='+this.idQuiz+'" class="btn comecar">';
                                var nameButton = (respondidas == 0) ? 'COMEÇAR' : (respondidas != this.perguntas.length) ? 'CONTINUAR' : 'VER RESULTADO';
            html +=             '<span>'+nameButton+'</span>';
            html +=         '</a>';
            html +=     '</div>';
            html += '</div>';

            //Quiz Respondido
            if (respondidas == this.perguntas.length) {
                quizRespondidos++
            }

            $('.quizzes').append(html);
        });

        //Progressao
        var porcentagemTestes = (100 * quizRespondidos) / quiz.length;
        $('.cidadania .etapa .info .progresso .bar .prog').css('width', porcentagemTestes+'%');
        $('.cidadania .etapa .info .progresso .porcentagem').text(porcentagemTestes + '%');
        
        //Status
        $('.cidadania .etapa .info .status .quiz-respondido .init').text(quizRespondidos);
        $('.cidadania .etapa .info .status .quiz-respondido .end').text(quiz.length);

    },

    setQuizDegustacao : function (quizDegustacao) {

        var html = '';

        html += '<!-- Item -->';
        html += '<div class="item">';
        html +=     '<div class="cover">';
        html +=         '<img src="assets/img/home/cover-quiz-cap-0.png" alt="Cover Capítulo Degustação" />';
        html +=     '</div>';
        html +=     '<div class="info">';
        html +=         '<h3>'+quizDegustacao.assunto+'</h3>';
        html +=         '<a href="/Quiz?id='+quizDegustacao.idQuiz+'" class="btn comecar active">';
        html +=             '<span>INICIAR TESTE</span>';
        html +=         '</a>';
        html +=     '</div>';
        html += '</div>';

        $('.quizzes').append(html);

        $('.etapas .cidadania').parent().addClass('active');
        $('.etapas .cidadania .title p').text('Como estão seus conhecimentos sobre as eleições?');
        $('.etapas .cidadania .etapa > .info').hide();
        $('.etapas .cidadania .etapa .sessoes').show();

        $('.sessoes .quiz .info h2').text('Descubra nessas 3 perguntas que preparamos para você.');
        $('.sessoes .quiz').addClass('degustacao');
        $('.sessoes .quiz .info .conteudo').hide();

    },

    setSlickQuizzes : function () {

        var configQuizzes = {
            dots: false,
            infinite: true,
            slidesToShow: 3,
            arrows: true,
            slidesToScroll: 1,
            initialSlide: 1,
            fade: false,
            centerMode: true,
            prevArrow: "<a class='arrow prev' href='javascript:void(0)'><i class='fas fa-arrow-left fa-lg'></i>TESTE ANTERIOR</a>",
            nextArrow: "<a class='arrow next' href='javascript:void(0)'>PRÓXIMO TESTE<i class='fas fa-arrow-right fa-lg'></i></a>",
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 1,
                        initialSlide: 0,
                        centerMode: true,
                        fade: false,
                        infinite: false,
                        dots: true,
                        centerPadding: '20px',
                        prevArrow: "<a class='arrow prev' href='javascript:void(0)'><i class='fas fa-angle-left fa-lg'></i>TESTE ANTERIOR</a>",
                        nextArrow: "<a class='arrow next' href='javascript:void(0)'>PRÓXIMO TESTE<i class='fas fa-angle-right fa-lg'></i></a>",
                    }
                },
            ]
        }
        APP.component.Slick.init('quizzes', configQuizzes);



    },

    //INTERACAO
    setClickQuiz : function () {
        $('.item .cover').off('click');
        $('.item .cover').on('click', function (event) {
            event.preventDefault();
            $('section').toggleClass('active');
            //Timeline
            //$('.timeline').slideToggle();
            $('.cidadania .title').toggle(200)
            $('.cidadania .etapa > .info').toggle(200)
            $('.cidadania .etapa .sessoes .quiz > .info').toggle(200)

            $('.quizzes').slick('setPosition');
            $('.back').prop('href', '/Home');

        });

    },

    setClickQuizDegustacao : function () {
        $('.degustacao .quizzes .item').on('click', function (event) {

            event.preventDefault();
            
            window.location.href = '/Quiz?id=9';

        });

    },

};