/*
|--------------------------------------------------------------------------
| Controller 
|--------------------------------------------------------------------------
*/
var ida = $('section#grau-de-afinidade-f3');
APP.controller.GrauDeAfinidadeF3 = {

    init: function () {
        this.changeCandidatoSelecionado();
        this.clickButtonVerMeuVoto();
        this.clicksCandidato();
        this.explicacaoCalculoAfinidade();
        this.buscaResultadoAfinidade();
    },

    changeCandidatoSelecionado : function () {
        ida.on('change', '.item input', function(event) {
            ida.find('button#verMeuVoto').removeAttr('disabled')
        });
    },

    buscarInfosCandidato: function (id, _afinidade, _selecionado, _cargo) {
        var erro = "";
        $.ajax({
            type: "GET",
            data: { id } ,
            dataType: 'json',
            url: "/MeusCandidatos/DetalhesCandidato/",
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.GrauDeAfinidadeF3.preencheDetalheCandidato(result.candidato, _afinidade, _selecionado, _cargo);
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });
    },

    preencheDetalheCandidato : function (result, afinidade, selecionado, _cargo) {

        if (_cargo != undefined) {
            var cargo = _cargo;
        } else {
            var cargo = $('div.selecionarCargo a.open').text();
        }

        var candidatoAfinidade = $('div#candidatoAfinidade');
        var classe = result.status.toLowerCase();
        
        candidatoAfinidade.html('');
        
        candidatoAfinidade.append(`
            <a href="#" class="close"></a>
            <div class="header">
                <div class="title">
                    <a href="#" class="voltar">
                        <i class="fas fa-arrow-left"></i>
                    </a>
                    Dados do Candidato
                </div>
                <div class="infos">
                    <div class="title">Candidato a ${cargo}</div>
                    <div class="nome">${result.nome.toLowerCase()}</div>
                </div>
                <div class="afinidade">
                    <div class="title">Afinidade</div>
                    <div class="porcentagem">${afinidade}%</div>
                </div>
            </div>
            <div class="photo ${selecionado ? "active" : ""}"><span style="background-image: url(${result.foto})"></span></div>
            <div class="item">
                <a href="#" class="info" data-title="" data-info="coligacoes" data-texto='Os dados disponibilizados na plataforma GUIA DO VOTO, incluindo os status das candidaturas para todos os cargos, são atualizados a cada <strong>48 horas</strong> a partir de informações oficiais do <strong>TSE</strong>.' data-img="">
                    <img src="/assets/img/meucandidatoideal/icon-info.svg">
                </a>
                <div class="nome">${result.nome.toLowerCase()}</div>
                <div class="status">
                    <div class="title">Status</div>
                    <div class="txt ${camelCase(classe)}">${result.status}</div>
                </div>
            </div>
            <div class="item">
                <div class="partido">
                    <div class="sigla">${result.sigla}</div>
                    <div class="txt">${result.partido}</div>
                </div>
                <div class="numero">${result.numero}</div>
            </div>
            ${(result.subCargos.length > 0 ?
                `<div class="item subcargos">
                    <div class="title">${cargo.toLowerCase().indexOf('senador') >= 0 ? "Suplente" : "Vice"}</div>
                    ${result.subCargos.map( l =>
                        `<div class="subcargo">
                            ${l.nome.toLowerCase()} (${l.sigla})
                        </div>`                    
                    ).join('')}
                </div>` : ''
            )}
            <div class="item">
                <div class="vice">
                    <div class="title">Coligação</div>
                    <div class="txt">
                        ${result.listaColigacao}
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="nomeCompleto">
                    <div class="title">Nome Completo</div>
                    <div class="txt">
                        ${result.nomeCompleto.toLowerCase()}
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="idade">
                    <div class="title">Idade</div>
                    <div class="num">${result.idade}</div>
                    <div class="txt">anos</div>
                </div>
                <div class="sexo">
                    <div class="title">Sexo</div>
                    <div class="icon">
                        <img src="/assets/img/meucandidatoideal/icon-${result.sexo == 'Masculino' ? 'mas' : 'fem'}.png" alt="">
                    </div>
                    <div class="txt">${result.sexo}</div>
                </div>
            </div>
            <div class="item">
                <div class="grau">
                    <div class="title">Grau/Instrução</div>
                    <div class="txt">${result.grau.toLowerCase()}</div>
                </div>
            </div>
            <div class="item">
                <div class="estadocivil">
                    <div class="title">Estado Civil</div>
                    <div class="txt">${result.estadoCivil}</div>
                </div>
                <div class="corRaca">
                    <div class="title">Cor/Raça</div>
                    <div class="txt">${result.cor}</div>
                </div>
            </div>
            <div class="item">
                <div class="ocupacao">
                    <div class="title">Ocupação</div>
                    <div class="txt">${result.ocupacao.toLowerCase()}</div>
                </div>
            </div>
            ${(result.valorMaximo != "-1" ? `
                <div class="item">
                    <div class="ocupacao">
                        <div class="title">Valor máximo da campanha</div>
                        <div class="txt">R$ ${result.valorMaximo}</div>
                    </div>
                </div>` : ``
            )}
            ${(result.sites.length > 0 ? `
                <div class="conhecerMais">
                    <p>Quer conhecer mais sobre este(a) candidato(a)? Acesse:</p>
                    ${result.sites.map( l =>
                        `<a href="${l.link}" target="_system">${l.nome}</a>`
                    ).join('')}
                    <small>Os dados apresentados em outras plataformas on-line não são de responsabilidade do Guia do Voto.</small>
                </div>` : ''
            )}

            ${selecionado != undefined ? 
                `<input ${selecionado ? "checked" : ""} id="candidato_${result.id}_in" data-id="${result.id}"  data-afinidade="${afinidade}" type="checkbox" />
                <label for="candidato_${result.id}_in"></label>
                <a href="#" class="verMeuVoto">Ver meu voto</a> 
                `
            : 
                ``
            }
          
        `)
    },

    buscaResultadoAfinidade : function () {
        $.ajax({
            async: false,
            url: '/GrauDeAfinidade/BuscarResultado/?cargo=' + cargoF3,
            type: 'GET',
        })
        .done(function(response) {
            APP.controller.GrauDeAfinidadeF3.preencheResultadoAfinidade(response);
        })
        .fail(function(response) {
            
        })
    },

    preencheResultadoAfinidade : function (_data) {

        
        if (_data.length < 3) {
            ida.find('.resultados .items').addClass('center')
        }

        ida.find('.resultados .items').append(`
            ${_data.map( a =>
                `<div class="item" data-id="${a.id}" data-afinidade="${a.afinidade}">
                    ${a.escolhido && !a.selecionado ?
                        ``
                        : `<input ${a.selecionado ? 'checked' : ''} name="candidato_${cargoF3}" id="candidato_${a.id}" type="radio">`                        
                    }
                    <div class="infos">
                        <div class="photo"><span style="background-image: url(${a.foto})"></span></div>
                        <div class="candidato">
                            <div class="nome">
                                ${a.nome.toLowerCase()} - 
                                <span class="numero">${a.numero}</span>
                            </div>
                            <div class="partido">
                                <span class="sigla">${a.sigla}</span>
                                (${a.partido})
                            </div>
                        </div>                  
                    </div>
                    ${a.escolhido && !a.selecionado ?
                        `<span class="escolhido">Candidato já escolhido</span>`
                        : `<label for="candidato_${a.id}"></label>`
                    }
                    <div class="afinidade">
                        <div class="title">Afinidade</div>
                        <div class="porcentagem">${a.afinidade}%</div>
                    </div>
                </div>`                    
            ).join('')}
        `)
        if (ida.find('.items input:checked').length > 0) {
            ida.find('button#verMeuVoto').attr('disabled', false);
        }
    },

    clickButtonVerMeuVoto : function () {
        ida.on('click', 'button#verMeuVoto', function(event) {
            APP.controller.MeusCandidatosF3.saveCargoEtapa(cargoF3, 4, '/MeuVoto/');
        });
        $('#candidatoAfinidade').on('click', '.verMeuVoto', function(event) {
            event.preventDefault();
            APP.controller.MeusCandidatosF3.saveCargoEtapa(cargoF3, 4, '/MeuVoto/');
        });
    },

    clicksCandidato : function () {
        $('body').on('click', '.resultados .item .infos', function(event) {
            $('#candidatoAfinidade').scrollTop(0);
            $('body').addClass('lockScroll');
            $('#candidatoAfinidade').addClass('active');
            ida.addClass('pushLeft');

            var idCandidato = $(this).parent().data('id');
            var afinidade = $(this).parent().data('afinidade');
            var selecionado = $(this).parent().find('input').is(':checked');

            APP.controller.GrauDeAfinidadeF3.buscarInfosCandidato(idCandidato, afinidade, selecionado);
        });

        $('body').on('click', '.resultados .item label', function(event) {
            if ($(this).parent().find('input').is(':checked')) {
                event.preventDefault();
                $(this).parent().find('input').prop('checked', false);
                ida.find('button#verMeuVoto').attr('disabled', true);
                APP.controller.GrauDeAfinidadeF3.removerVoto(cargoF3);
            } else {
                APP.controller.GrauDeAfinidadeF3.salvarVoto(cargoF3, $(this).parent().data('id'), $(this).parent().data('afinidade'));
            }
        });

        $('body').on('click', '#candidatoAfinidade .voltar, #candidatoAfinidade .close', function(event) {
            event.preventDefault();
            $('body').removeClass('lockScroll');
            $('#candidatoAfinidade').removeClass('active');
            ida.removeClass('pushLeft');
        });

        $('body').on('change', '#candidatoAfinidade input', function(event) {
            var id = $(this).data('id')
            var checked = $(this).is(':checked');

            if (checked) {
                ida.find('.items #candidato_'+id).prop('checked', checked);
                ida.find('button#verMeuVoto').removeAttr('disabled');
                APP.controller.GrauDeAfinidadeF3.salvarVoto(cargoF3, $(this).data('id'), $(this).data('afinidade'));
                $('#candidatoAfinidade div.photo').addClass('active');
            } else {
                ida.find('.items input').prop('checked', checked);
                ida.find('button#verMeuVoto').attr('disabled', true);
                APP.controller.GrauDeAfinidadeF3.removerVoto(cargoF3);
                $('#candidatoAfinidade div.photo').removeClass('active');
            }
        });

        // $('body').on('click', '.info-ciente', function(event) {
        //     event.preventDefault();
        //     $('#explicacaoCalculoAfinidade').addClass('active');
        // });
    },

    salvarVoto : function (_cargo, _id, _porcentagem) {
        var data = {
            'cargo': _cargo,
            'idCandidato': _id,
            'porcentagem': _porcentagem,
        }
        $.ajax({
            url: '/GrauDeAfinidade/SalvarVoto/',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                // $('body').addClass('lockScroll');
                APP.component.Loading.show();
            }
        })
        .done(function(response) {
        })
        .always(function() {
            APP.component.Loading.hide();
        })
        .fail(function(response) {
        })
    },

    removerVoto : function (_cargo) {
        var data = {
            'cargo': _cargo,
        }
        $.ajax({
            url: '/GrauDeAfinidade/RemoverVoto/',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                // $('body').addClass('lockScroll');
                APP.component.Loading.show();
            }
        })
        .done(function(response) {
        })
        .always(function() {
            APP.component.Loading.hide();
        })
        .fail(function(response) {
        })
    },

    explicacaoCalculoAfinidade : function () {
        // if (localStorage.getItem('LeuExplicacaoCalculoAfinidade') === null) {
            // $('#explicacaoCalculoAfinidade').addClass('active');            
        // }

        // $('body').on('click', '#explicacaoCalculoAfinidade .ciente', function(event) {
        //     event.preventDefault();
        //     $('#explicacaoCalculoAfinidade').removeClass('active');
        //     localStorage.setItem('LeuExplicacaoCalculoAfinidade', true);
        // });

        $('body').on('click', '#inforamcoes .termos', function(event) {
            event.preventDefault();
            $('.modal-termos').fadeIn();
        });
    },
};