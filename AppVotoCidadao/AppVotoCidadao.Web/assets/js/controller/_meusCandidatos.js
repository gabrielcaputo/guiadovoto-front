/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

var page = window.location.pathname.split('/')[2];
var cargo = window.location.pathname.split('/')[window.location.pathname.split('/').length - 1];

APP.controller.MeusCandidatos = {

    init: function () {
        this.setup();
        this.meusCandidatos();

    }, 

    setup : function () {

        //

    },

    meusCandidatos : function () {

        //Menu
        this.setInteracoesMeusCandidatos();

        //tutorial
        this.tutorial();
        this.setClickTrigguerTutorial();
        this.setClickTrigguerOndePesquisar();
        this.setClickBox2Links();
        this.setClickInfos();
        
        this.getCargoEtapa();
        //Controller Page
        this.setControllerInit();
        this.scrollableHeaders();

    },

    /*
    |--------------------------------------------------------------------------
    | Interações
    |--------------------------------------------------------------------------
    */
    setInteracoesMeusCandidatos : function () {

        this.getCargoEtapaList();

    },

    getCargoEtapa : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/MeusCandidatos/BuscarEtapa',
            beforeSend: function () {
            },
            success: function (result) {
                APP.controller.MeusCandidatos.setCargoEtapa(result);

            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
            }
        })

    },

    setCargoEtapa : function (result) {

        var etapa = 0;

        $(result).each(function (e) {
            var cargoC = properCase(this.candidato).replace(' ', '');
            cargo === cargoC ? etapa = this.etapa : '';
        });

        $('.steps li').each(function (e) {
            if ((e+1) < etapa) {
                $(this).addClass('finalizado');
            }
        });

    },

    saveCargoEtapa : function (_cargo, _etapa, _url) {

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: `/MeusCandidatos/ConcluirEtapa?cargo=${_cargo}&etapa=${_etapa}`,
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                
                if (_url) {
                    window.location.href = _url;                    
                }

            },
            error: function (result) {
                
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        })

    },

    /*
    |--------------------------------------------------------------------------
    | Selecionar Cargos
    |--------------------------------------------------------------------------
    */

    getCargoEtapaList : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/MeusCandidatos/BuscarEtapa',
            beforeSend: function () {
            },
            success: function (result) {
                APP.controller.MeusCandidatos.setCargoEtapa(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
            }
        }).promise().done(function () {
            APP.controller.MeusCandidatos.verificaCargoPage();
            APP.controller.MeusCandidatos.clicksSelecionarCargo();
        });

    },

    setCargoEtapa : function (result) {

        $(result).each(function (index) {

            var etapa = this.etapa;
            var cargoEtapa = properCase(this.candidato).replace(' ', '');
            
            APP.controller.MeusCandidatos.setRulesEtapa(etapa, cargoEtapa, index+1);

        });

    },

    setRulesEtapa : function (_etapa, _cargoEtapa, _index) {

        var selecionarCargo = $('.selecionarCargo .wrap ul');

        switch(_etapa) {
            case 1:
                selecionarCargo.find('li:nth-of-type('+_index+') a').attr('href', '/MeusCandidatos/Candidatos2018/'+_cargoEtapa);
                break;
            case 2:
                selecionarCargo.find('li:nth-of-type('+_index+') a').attr('href', '/MeusCandidatos/AvaliarSelecionados/'+_cargoEtapa);
                break;
            case 3:
                selecionarCargo.find('li:nth-of-type('+_index+') a').attr('href', '/MeusCandidatos/GrauDeAfinidade/'+_cargoEtapa);
                break;
            case 4:
            selecionarCargo.find('li:nth-of-type('+_index+') a').attr('href', '/MeusCandidatos/GrauDeAfinidade/'+_cargoEtapa);
                break;
            default:
                
        }

    },

    verificaCargoPage : function () {

        $('.selecionarCargo .wrap li a').each(function () {
            var cargoThis = $(this).text().replace(" ", "");
            // var url = '/MeusCandidatos/'+page+'/'+cargoThis;
            var urlMeuCandidato = '/MeuCandidatoIdeal/?cargo='+cargoThis;
            if (cargo == cargoThis) {
                $(this).addClass('close');
                $(this).closest('.selecionarCargo').find('.open').text($(this).text());
                $('.filtros').find('.caracteristicas').attr('href', urlMeuCandidato);
            }
            //$(this).attr('href', url);
        });

    },

    clicksSelecionarCargo : function () {
        var selecionarCargo = $('div.selecionarCargo');
        selecionarCargo.on('click', '.open', function(event) {
            event.preventDefault();
            $('body').addClass('lockScroll');
            selecionarCargo.addClass('active');
        });

        selecionarCargo.on('click', '.close', function(event) {
            event.preventDefault();
            $('body').removeClass('lockScroll');
            selecionarCargo.removeClass('active');
        });
    },


    scrollableHeaders : function () {
        $('body > header').addClass('scrollable');

        var lastScrollTop = 0;
        $(window).on('scroll', function() {
            st = $(this).scrollTop();

            // Selecionar Cargo
            var sC = $('.selecionarCargo');
            if (st > 120) {
                if (st > 60) {
                    sC.addClass('scrollable');
                }                
                sC.addClass('fixed');

                if(st < lastScrollTop) {
                    sC.addClass('scroll');
                }
                else if ((st - lastScrollTop) < 40) {
                    sC.removeClass('scroll');
                }
            } else if (st <= 60) {
                sC.removeClass('scrollable fixed scroll');
            }

            lastScrollTop = st;
        });
    },
    
    /*
    |--------------------------------------------------------------------------
    | TUTORIAL 
    |--------------------------------------------------------------------------
    */

    setClickBox2Links : function () {

        $('.box-tutorial a.ask').on('click', function (event) {

            event.preventDefault();

            if ($(this).closest('.box-tutorial').hasClass('active')) {
                $(this).closest('.box-tutorial').removeClass('active');
            } else {
                $(this).closest('.box-tutorial').addClass('active');
            }


        });

    },

    tutorial : function () {

        var leuTutorial = localStorage.getItem('LeuTutorial'+page);
        var login = APP.component.Utils.getLogin();

        APP.controller.MeusCandidatos.setSlickTutorial();
        APP.controller.MeusCandidatos.setClickTutorialComecar();

        // if (leuTutorial != 'true' && login && page != "GrauDeAfinidade") {
        //     $('[id^=tutorial-]').fadeIn();
        //     $('body').addClass('lockScroll');
        // }

    },

    setLeuTutorial : function () {

        var leuTutorial = localStorage.getItem('LeuTutorial'+page);
        if (leuTutorial == null) {
            localStorage.setItem('LeuTutorial'+page, true);
        }

    },

    setSlickTutorial : function () {

        $('.tutorial-steps').slick({
            arrows: false,
            dots: true,
            infinite: false,
            adaptiveHeight: false,
        });

    },

    setClickTutorialComecar : function () {

        $('[id^=tutorial] a.comecar').on('click', function (event) {

            event.preventDefault();

            APP.controller.MeusCandidatos.setLeuTutorial();
            $('[id^=tutorial-]').fadeOut();
            $('body').removeClass('lockScroll');

        });

    },

    setClickTrigguerTutorial : function () {
        
        var triggerTutorial = $('.triggerTutorial');
        triggerTutorial.on('click', function (event) {

            event.preventDefault();

            $('[id^=tutorial-]').fadeIn();
            $('.tutorial-steps').slick('setPosition');
            $('[id^=tutorial] .comecar').text('VOLTAR');
            $('body').addClass('lockScroll');

        });

    },

    setClickTrigguerOndePesquisar : function () {
        
    
        $('body').on('click', '.triggerOndePesquisar', function (event) {

            event.preventDefault();

            APP.component.Alert.generico('#ondePesquisar')

        });

    },

    setClickInfos : function () {
        $('body').on('click', '.info', function(event) {
            event.preventDefault();

            var text = $(this).data('texto');
            var img = $(this).data('img');
            var title = $(this).data('title');

            APP.component.Alert.modalInformacoes(title, text, img, 'voce-sabia')
        });
    },

    /*
    |--------------------------------------------------------------------------
    | Set Controllers Internas
    |--------------------------------------------------------------------------
    */
    setControllerInit : function () {

        switch (page) {
            case 'Candidatos2018':
                APP.controller.Candidatos2018.init();
                $('.steps li:nth-of-type(1)').addClass('active');
                break;
            case 'AvaliarSelecionados':
                APP.controller.AvaliarSelecionados.init();
                $('.steps li:nth-of-type(2)').addClass('active');
                break;
            case 'GrauDeAfinidade':
                APP.controller.GrauDeAfinidade.init();
                $('.steps li:nth-of-type(3)').addClass('active');
                break;
        }
        
    },

};