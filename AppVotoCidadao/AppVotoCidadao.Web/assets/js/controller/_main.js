/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

APP.controller.Main = {

    init : function () {

        this.setup();
        this.main();

    },

    setup : function () {

        //

    },

    //Models
    models : {

        //PerfilUsuarioModel: APP.model.PerfilUsuario,

    },

    //Global
    onUserLoaded : function() {},
    
    //Main
    main : function () {
        
        APP.component.CheckConnection.init();
        APP.component.Modal.init();
        APP.component.OneSignal.init();
        this.setScroll();


    },

    setScroll : function () {

		$(window).bind('scroll', function() {

            if ( $(".modal:visible:not(#customHTML)").length > 0 ) {
				$(".modal").center();
			}

        });
        

    },


};