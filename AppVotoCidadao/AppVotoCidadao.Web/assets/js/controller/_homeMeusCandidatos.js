﻿/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/
APP.controller.MeusCandidatosHome = {
    
    init : function () {
        
        this.setup();
        this.meuCandidatos();
        
        
    }, 
    
    setup : function () {
        
        //
        
    },
    
    meuCandidatos : function () { 
        
        this.getCargoEtapaList();
        this.getEstadosList();
        
    },
    
    /*
    |--------------------------------------------------------------------------
    | ESTADOS
    |--------------------------------------------------------------------------
    */
    getEstadosList : function (result) {
        
        $.ajax({
            type: "GET",
            url: "/SegundoTurno/BuscarEstados",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.MeusCandidatosHome.setEstadosList(result);
            },
            error: function (result) {
                // APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro", 'verify-question', 'OK');
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function (result) {
            APP.controller.MeusCandidatosHome.setClickEstados();
        });
        
    },
    
    setEstadosList : function (result) {
        
        $.each(result.estados, function (index, val) {
            
            if (val.selecionado && !val.segundoTurno) {
                $('.candidatos .mc-wrap:last-of-type').css('visibility', 'hidden');
                //$('.candidatos').slick("slickSetOption", "swipe", false);
            }
            
            $('div#estados-st ul').append(
                `<li data-id="${val.id}" value="${val.sigla}">
                <span class="sigla">${val.sigla}</span>
                <div class="informacoes">
                <span class="nome">${val.nome}</span>
                <span class="option-turno">
                ${val.segundoTurno ? `2º turno para presidente e governador` : `2º turno para presidente`}
                </span>
                </div>
                </li>
                `
                );
                
                $('div.estado select#estado, div.escolhe-estado select#estado').append(`<option data-id="${val.id}" ${val.selecionado ? `selected` : ``} value="${val.sigla}" data-segundo-turno="${val.segundoTurno}">${val.sigla} - ${val.nome}</option>`)
            });

            
            
        },
        
        setClickEstados : function () {
            
            $('.triggerEstado').on('click', function (event) {
                
                event.preventDefault();            
                
                $(this).toggleClass('disabled');
                
                $('#estados-st').addClass('active');
                $('section').addClass('blur');
                $('body').addClass('scroll-lock');
                $('body').css('pointer-events', 'none');
                
                $('#estados-st li').removeClass('active');            
                
                if ($('#estado').val() != '') {
                    $('#estados-st li[value=' + $('#estado').val() + ']').addClass('active');
                }
            });
            
            $('body').on('click', '#estados-st a.close', '#home', function (event) {
                event.preventDefault();
                
                $('#estados-st').removeClass('active');
                $('section').removeClass('blur');
                $('body').removeClass('scroll-lock');
                $('body').css('pointer-events', 'initial');
                
            });
            
            $('body #estados-st').on('click', 'li', function (event) {
                event.preventDefault();
                
                var estado = $(this).attr('value');
                $('[id=estado]').val(estado);
                $('#estados-st').removeClass('active');
                $('section').removeClass('blur');
                $('body').removeClass('scroll-lock');
                $('body').css('pointer-events', 'initial');
                $('.escolhe-estado').removeClass('blur');
                
                APP.controller.MeusCandidatosHome.saveEstado(parseInt($(this).attr('data-id')));
            });
            
        },
        
        saveEstado: function (_data) {
            
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '/SegundoTurno/SalvarEstado',
                data: { "estado": _data },
                beforeSend: function () {
                    APP.component.Loading.show();
                },
                success: function (result) {
                    if (!result.segundoTurno) {
                        $('.candidatos .mc-wrap:last-of-type').css('visibility', 'hidden');
                        $('.candidatos').slick("slickSetOption", "swipe", false);
                        $('.candidatos').slick("slickGoTo", 0);
                        
                    } else {
                        $('.candidatos .mc-wrap:last-of-type').css('visibility', 'visible');
                        $('.candidatos').slick("slickSetOption", "swipe", true);
                    }
                },
                error: function (result) {
                    //APP.component.Alert.customAlert(result.statusText, "Erro");
                },
                complete: function (result) {
                    APP.component.Loading.hide();
                }
            });
            
        },
        
        /*
        |--------------------------------------------------------------------------
        | ESTADO - primeiro acesso
        |--------------------------------------------------------------------------
        */
        getVerificaPrimeiroAcesso : function () {
            
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: '/SegundoTurno/EstadoSelecionado',
                beforeSend: function () {
                },
                success: function (result) {
                    if (result.estado == null) {
                        APP.controller.MeusCandidatosHome.verificaPrimeiroAcesso(result.estado);
                    }
                },
                error: function (result) {
                    //APP.component.Alert.customAlert(result.statusText, "Erro");
                },
                complete: function (result) {
                }
            }).promise().done(function () {
                APP.controller.MeusCandidatosHome.setClickEstadosPrimeiroAcesso();
            });
            
        },
        
        verificaPrimeiroAcesso : function (estado) {
            $('.escolhe-estado').addClass('active');
            $('body').addClass('scroll-lock');
        },
        
        setClickEstadosPrimeiroAcesso : function () {
            
            $('a.triggerEstado-home').on('click', function (event) {
                
                event.preventDefault();            
                
                $(this).toggleClass('disabled');
                $('#estados-st .close').css('display', 'none');
                $('#estados-st').addClass('active');
                $('#estados-st li').removeClass('active');
                $('.escolhe-estado').addClass('blur');
                
                if ($('#estado').val() != '') {
                    $('#estados-st li[value=' + $('#estado').val() + ']').addClass('active');
                }
            });
            
            $('body #estados-st').on('click', 'li', function (event) {
                event.preventDefault();
                $('#estados-st .close').css('display', 'block');
                $('body').removeClass('scroll-lock');
                $('.escolhe-estado').removeClass('blur');
                APP.controller.MeusCandidatosHome.saveEstado(parseInt($(this).attr('data-id')));
            });
            
            $('body .escolhe-estado').on('click', '.continuar', function (event) {
                event.preventDefault();
                $('.escolhe-estado').removeClass('active');
                $('.escolhe-estado').removeClass('blur');
                $('body').removeClass('scroll-lock');
                APP.controller.MeusCandidatosHome.saveEstado(parseInt($('#estado option:selected').data('id')));

            });
            
        },
        
        /*
        |--------------------------------------------------------------------------
        | CANDIDATOS
        |--------------------------------------------------------------------------
        */
        setSlickCandidatos : function () {
            
            
            if ($(window).width() > 960) {
                $('.candidatos').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                    APP.controller.MeusCandidatosHome.setArrowsDisabled(nextSlide);
                });
                $('.candidatos').on('init', function(slick){
                    APP.controller.MeusCandidatosHome.setArrowsDisabled(1);

                });
            }
            
            var configSlick = {
                dots: false,
                infinite: false,
                slidesToShow: 2,
                arrows: true,
                fade: false,
                centerMode: false,
                initialSlide: 0,
                touchMove: false,
                swipe: true,
                prevArrow: "<a class='arrow prev' href='javascript:void(0)'><i class='fas fa-arrow-left fa-lg'></i>TESTE ANTERIOR</a>",
                nextArrow: "<a class='arrow next' href='javascript:void(0)'>PRÓXIMO TESTE<i class='fas fa-arrow-right fa-lg'></i></a>",
                responsive: [
                    {
                        breakpoint: 960,
                        settings: {
                            slidesToShow: 2,
                            arrows: false,
                            centerMode: false,
                            fade: false,
                            infinite: false,
                            dots: false,
                            centerPadding: '0',
                            initialSlide: 0,
                            touchMove: true,
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 641,
                        settings: {
                            slidesToShow: 1,
                            arrows: false,
                            centerMode: true,
                            fade: false,
                            infinite: false,
                            dots: false,
                            centerPadding: '20px',
                            initialSlide: 0,
                            touchMove: true,
                            swipe: true
                        }
                    },
                ]
            }
            
            
            APP.component.Slick.init('candidatos', configSlick);

            // req.then(response => response.json())
            // .then((response => {
            //     response.estados.map(item => {
            //         if (!item.segundoTurno) {
            //             $('.candidatos').slick({
            //                 swipe: false
            //             });
            //         }
            //     });
            // }));
            
        },
        
        setArrowsDisabled : function (nextSlide) {
            
            if (nextSlide < 2) {
                $('.candidatos .arrow.prev').addClass('slick-disabled-custom');
                
            } else if (nextSlide > 3) {
                $('.candidatos .arrow.next').addClass('slick-disabled-custom');
            } else {
                $('.candidatos .arrow').removeClass('slick-disabled-custom');
            }
            
        },
        
        getCargoEtapaList : function () {
            
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: '/MeusCandidatos/BuscarEtapa',
                beforeSend: function () {
                },
                success: function (result) {
                    APP.controller.MeusCandidatosHome.setCargoEtapa(result);
                },
                error: function (result) {
                    //APP.component.Alert.customAlert(result.statusText, "Erro");
                },
                complete: function (result) {
                }
            })
            
        },
        
        setCargoEtapa : function (result) {
            
            $(result).each(function () {
                
                var etapa = this.etapa;
                var cargoEstapa = properCase(this.candidato).replace(' ', '');
                var lista = $('.candidatos').find('[data-cargo="'+cargoEstapa+'"]').closest('.item').find('.lista-etapas');
                
                APP.controller.MeusCandidatosHome.setRulesEtapa(etapa, lista);
                
            });
            
        },
        
        setRulesEtapa : function (_etapa, _lista) {
            
            switch(_etapa) {
                case 1:
                _lista.find('a:nth-of-type('+_etapa+')').addClass('inicio');
                _lista.find('a:nth-of-type('+_etapa+')').removeClass('blocked');
                break;
                case 2:
                _lista.find('a:nth-of-type('+_etapa+')').addClass('andamento');
                _lista.find('a:nth-of-type('+_etapa+')').removeClass('blocked');
                _lista.find('a:nth-of-type('+(_etapa-1)+')').addClass('finalizado');
                _lista.find('a:nth-of-type('+(_etapa-1)+')').removeClass('blocked');
                break;
                case 3:
                _lista.find('a:nth-of-type('+_etapa+')').addClass('andamento');
                _lista.find('a:nth-of-type('+(_etapa-1)+')').addClass('finalizado');
                _lista.find('a:nth-of-type('+(_etapa-2)+')').addClass('finalizado');
                _lista.find('a').removeClass('blocked');
                break;
                case 4:
                _lista.find('a').addClass('finalizado');
                _lista.find('a').removeClass('blocked');
                break;
                default:
                
            }
            
        },
        
    };