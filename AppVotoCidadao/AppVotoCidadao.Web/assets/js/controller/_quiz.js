/*
|--------------------------------------------------------------------------
| Controller Quiz
|--------------------------------------------------------------------------
*/

APP.controller.Quiz = {

    init : function () {

        this.setup();
        this.quiz();

    },

    setup : function () {

        //

    },

    //Quiz 
    quiz : function () {

        this.setShowAndHide();
        this.getQuiz();
        this.setClickResposta();
        this.setJumpQuestion();
        this.setResponderQuestion();
        this.setGoSaibaMais();

    },

    setShowAndHide : function () {

        $('body header').css('border', 'none');
        $('.back').prop('href', '/Home');

        
        if ($(window).width() > 960) {
            $('section').addClass('desk');
        }

        var login = APP.component.Utils.getLogin();

        if (!login) {
            $('.infos .progression').hide();
            $('.infos .status').hide();
            $('.infos .desc p').hide();
            $('.infos .desc small').hide();
            $('.btn-back').hide();
        }

    },

    //GET
    getQuiz : function () {

        var login = APP.component.Utils.getLogin();
        var url ='';

        if (login) {
            url = '/Quiz/GetQuiz';
        } else {
            if (APP.component.Utils.getUrlParameter('redirected')) {
                window.location = "/"
            };
            url = '/QuizDegustacao/GetQuiz';
        }

        var idQuiz = APP.component.Utils.getUrlParameter('id');

        $.ajax({
            type: "GET",
            data: {'idQuiz' : idQuiz},
            dataType: 'json',
            url: url,
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.Quiz.setInfosQuiz(result, login);
            },
            error: function (result) {
                
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        });

    },

    //Quiz
    setInfosQuiz : function (quiz, login) {

        //Background Capitulo
        $('section').attr('data-cap', quiz.perguntas[0].idCapitulo);
        $('.infos').addClass('cap-0'+quiz.perguntas[0].idCapitulo);

        var acertos = 0;
        var respondidas = 0;

        $(quiz.perguntas).each(function (e) {

            var html = '';
            var classe = '';
            var pergunta = this;
            var bt = 0;

            //Progresso
            if (pergunta.acertou === true) {
                classe = 'right';
                acertos ++;
                respondidas++;
            } else if (pergunta.acertou === false) {
                classe = 'wrong';
                respondidas++;
            } else {
                classe = '';
            }
            $('.progression ul').append('<li class="'+classe+'"></li>');

            if (!login) {
                
                bt = pergunta.respostas[0].idQuizRespostaCorreta;
            }

            //Pergunta
            html += '<div class="item">';
            html += '<!-- Pergunta -->';
            html += '<div class="question" data-id-question="'+pergunta.idQuizPergunta+'">';
            html += '<p>';
            html += ''+pergunta.dsPergunta+'';
            html += '</p>';
            html += '</div>';
            //Resposta
            html += '<!-- Resposta -->';
            html += '<div class="answer">';
            html += '<div class="list" data-bt="'+bt+'" >';
            $(pergunta.respostas).each(function (i) {
                html += '<input type="radio" id="answer-'+this.idQuizResposta+'" value="'+this.idQuizResposta+'" name="quetionario-'+e+'">';
                html += '<label for="answer-'+this.idQuizResposta+'" data-index="'+this.idQuizResposta+'"><span>';
                html += ''+this.dsResposta+'';
                html += '</span></label>';
                html += '<div class="explanation">';
                html += '<p>';
                html += ''+pergunta.dsExplicacao+'';
                html += '</p>';
                // html += '<a href="/Guia/Capitulo?cap='+pergunta.idCapitulo+'&sub='+pergunta.idReferencia+'&pag='+pergunta.pagina+'" class="saiba-mais">Saiba mais </a>';
                html += '</div>'
            });
            html += '</div>';
            html += '</div>';
            html += '</div>';

            $('.quiz').append(html);

        });

        if (login) {
            
            //Status
            $('.status .numbers .init').text(respondidas);
            $('.status .numbers .end').text(quiz.perguntas.length);
            var porcentagem = (100 * acertos) / quiz.perguntas.length;
            $('.percentage .total').text(porcentagem.toFixed(0));
            $('.desc .total').text((100 / quiz.perguntas.length ));
        }

        //Descrição
        $('.desc h2').text(quiz.assunto);
        $('.desc h2').addClass('cap-0'+quiz.perguntas[0].idCapitulo);
        $('.desc p').text(quiz.titulo);
        $('.desc .end').text(quiz.perguntas.length);

        //Slick
        APP.controller.Quiz.setSlickPerguntas(quiz.idPerguntaStop);

        //Page
        var page = $('.quiz').slick('slickCurrentSlide');
        $('.actions .pages span.init').text(page+1);
        $('.actions .pages span.end').text(quiz.perguntas.length);

        //Desabilita Botao
        if(respondidas == quiz.perguntas.length-1) {
            $('.jumpQuestion').addClass('disabled');
        }

        //Bind
        APP.controller.Quiz.setClickResposta();

        //Se quiz finalizado
        if (quiz.finalizado == true) {
            APP.controller.Quiz.setFinalQuiz(quiz.perguntas.length);
        }

    },

    setSlickPerguntas : function (_idPerguntaStop) {

        var configQuiz = {
            //dots: true,
            infinite: false,
            slidesToShow: 1,
            arrows: false,
            slidesToScroll: 1,
            //fade: true,
            adaptiveHeight: true,
            swipe: false,
            touchMove: false,
            draggable: false,
            initialSlide: _idPerguntaStop - 1,
        }
        APP.component.Slick.init('quiz', configQuiz);


    },

    setFinalQuiz : function (_tamQuiz) {
        
        var login = APP.component.Utils.getLogin();
        //Get
        var total = $('.infos .status .percentage .total').text();

        if (login) {
            APP.controller.Quiz.getResultadoQuiz(total, _tamQuiz);
        } else {
            APP.controller.Quiz.setInfoFinalQuizDegustacao(total, _tamQuiz);
        }
        
    },

    setInfoFinalQuiz : function (quizFinal, tamQuiz) {

        var capitulo = quizFinal.resultado.capitulo;
        var nota = quizFinal.resultado.nota;
        var frase = quizFinal.resultado.frase;
        var range = quizFinal.resultado.range;
        var proximoQuiz = quizFinal.resultado.proximoQuizId;

        //Verifica Range
        if (range < 5) {
            range = 1;
        }

        //URL de compartilhamento
        var title = 'Guia do Voto';
        var desc = 'Que tal testar seus conhecimentos também?';
        var imgRange = quizFinal.resultado.range;
        var url = 'guiadovoto.org.br'
        //var URLShare = encodeURI('http://guiadovoto.fullbarhomologa.com.br/Compartilhar?title=' + title + '&desc=' + desc + '&img=' + 'facebook-nota-cap-0'+capitulo+'-'+imgRange+'.png&url='+url+'');
        var URLShare = 'https://www.facebook.com/dialog/share?app_id=247591319322843&display=popup&href=' + encodeURIComponent('https://guiadovoto.org.br/Compartilhar?title=' + title + '&desc=' + desc + '&img=' + 'facebook-nota-cap-0'+capitulo+'-'+imgRange+'.png&url='+url+'');

        var progression = $('.progression').html();
        var status = $('.status').html();
        
        var finalQuiz = '';
        
        finalQuiz += '<div class="item final-quiz">';
        finalQuiz +=    '<div class="imagem cap-0'+capitulo+'">';
        finalQuiz +=        '<span>'+nota+'</span>';
        finalQuiz +=        '<img src="assets/img/quiz/bg-nota-cap-0'+capitulo+'-'+range+'.png" alt="Nota" />';
        finalQuiz +=    '</div>';
        finalQuiz +=    '<div class="infos-final">';
        finalQuiz +=        '<div class="progression final">';
        finalQuiz +=            ''+progression+'';
        finalQuiz +=        '</div>';
        finalQuiz +=        '<div class="status">';
        finalQuiz +=            ''+status+'';
        finalQuiz +=        '</div>';
        finalQuiz +=        '<p>'+frase+'</p>';
        finalQuiz +=    '</div>';
        finalQuiz +=    '<div class="actions-end">';
        finalQuiz +=        '<a href="#" onclick="window.open(\'' + URLShare + '\', \'_system\')" class="btn compartilhar st-custom-button">';
        finalQuiz +=            '<span>COMPARTILHAR RESULTADO</span>';
        finalQuiz +=        '</a>';
        finalQuiz +=        '<div class="btn-quiz">';
        finalQuiz +=            '<a href="#" class="btn quiz-final refazer-quiz">';
        finalQuiz +=                '<span>REFAZER</span>';
        finalQuiz +=            '</a>';
        if (proximoQuiz != null) {
            finalQuiz +=            '<a href="/Quiz?id='+proximoQuiz+'" class="btn quiz-final proximo-quiz">';
            finalQuiz +=                '<span>PRÓXIMO TESTE</span>';
            finalQuiz +=            '</a>';
        }
        finalQuiz +=        '</div>';
        finalQuiz += '</div>';

        //Disable
        $('.infos').addClass('disable');
        $('.actions').addClass('disable');

        //Slick
        $('.quiz').slick('slickAdd', finalQuiz);
        $('.quiz').slick('slickGoTo', tamQuiz+1, true);
        setTimeout(function(){ 
            $('.quiz').slick('setPosition'); 
        }, 600);

        var btnBack = '';
        btnBack += '<a href="/Home#quizzes" class="btn-back">';
        btnBack += '<img src="/assets/img/icons/icon-arrow-blue-desk.png">';
        btnBack += '<span>VOLTAR PARA OS TESTES</span>';
        btnBack += '</a>';

       $(btnBack).insertBefore($('.quiz'));

        //Bind
        APP.controller.Quiz.setRefazer();
        // APP.component.ShareThis.init();

    },

    setInfoFinalQuizDegustacao : function (quizFinal, tamQuiz) {

        var finalQuiz = '';

        finalQuiz += '<div class="item final-quiz deg">';
        finalQuiz +=    '<div class="imagem cap-00">';
        finalQuiz +=        '<img src="assets/img/quiz/bg-nota-cap-00.png" alt="Final Quiz Degustacao" />';
        finalQuiz +=    '</div>';
        finalQuiz +=    '<div class="infos-final cap-0">';
        finalQuiz +=        '<p>Que tal continuar praticando? Faça seu cadastro!</p>';
        finalQuiz +=    '</div>';
        finalQuiz +=    '<div class="actions-end">';
        finalQuiz +=        '<a href="/Cadastro" class="btn cadastrese">';
        finalQuiz +=            '<span>CADASTRE-SE</span>';
        finalQuiz +=        '</a>';
        finalQuiz += '</div>';

        //Disable
        $('.infos').addClass('disable');
        $('.actions').addClass('disable');

        //Slick
        $('.quiz').slick('slickAdd', finalQuiz);
        $('.quiz').slick('slickGoTo', tamQuiz+1);
        
        setTimeout(function(){ 
            $('.quiz').slick('setPosition'); 
        }, 600);

    },

    //INTERACOES
    setClickResposta : function () {

        $('.answer .list label').on('click', function () {

            $('.answer .list label').removeClass('active');
            $('.responder').removeClass('disabled');
            $(this).toggleClass('active');
            var lacuna = $(this).closest('.item').find('.question p span').length;
            
            if (lacuna > 0) {
                var texto = $(this).find('span').text();
                $(this).closest('.item').find('.question p span').addClass('txt-question');
                $(this).closest('.item').find('.question p span').text(texto);
            }

        });

    },

    setJumpQuestion : function () {



        $('.jumpQuestion').on('click', function (event) {

            event.preventDefault();

            //Progressão
            $('.progression ul li:nth-of-type('+($('.quiz').slick('slickCurrentSlide') + 1)+')').addClass('jump');

            //Move para o proximo slick null
            var slick = [];
            $('.progression ul li').each(function(e) {
                var has = $(this).is('.right, .wrong, .jump');
                has == false ? slick.push(e) : '';
            })

            if (slick != '') {
                $('.quiz').slick('slickGoTo', slick[0]);
            }

            if (slick != '') {
                if (slick.length > 1) {$('.jumpQuestion').removeClass('disabled');}
                $('.quiz').slick('slickGoTo', slick[0]);
            } else {
                //Verifica se algum foi pulado
                $('.progression ul li').each(function(e) {
                    var has = $(this).is('.jump:not(.right):not(.wrong)');
                    has == true ? slick.push(e) : '';
                })
                
                if (slick != '') {
                    $('.progression ul li').removeClass('jump');
                    if (slick.length > 1) {$('.jumpQuestion').removeClass('disabled');}
                    $('.quiz').slick('slickGoTo', slick[0]);
                }
            }

            //Page
            var page = $('.quiz').slick('slickCurrentSlide')+1;
            $('.actions .pages span.init').text(page);

            //Actions
            $('.actions .responder').addClass('disabled');

            $('html, body').animate({
                scrollTop: 0
            }, 500);

        });


    },

    setResponderQuestion : function () {

        $('.responder').on('click', function (event) {

            event.preventDefault();

            var login = APP.component.Utils.getLogin();

            var idPergunta = $('.slick-active .question').data('id-question');
            var idResposta = parseInt($('.slick-active .answer .list input:checked').val());

            if (idResposta != '' &&  !isNaN(idResposta) && login) {
                APP.controller.Quiz.sendAnswerQuiz(idPergunta, idResposta);
            } else  if (idResposta != '' &&  !isNaN(idResposta)) {
                APP.controller.Quiz.setAnswerQuizDegustacao();
            } else {
                APP.component.Alert.customAlert("Escolha uma opção", "Atenção");
            }

        });


    },

    setGoNextQuestion : function () {
        $('.continuar').off('click')
        $('.continuar').on('click', function (event) {
            
            event.preventDefault();

            var tamQuiz = $('.quiz .item').length;

            //Verifica se esta no ultimo slick para desabilitar o Pular
            if ($('.quiz').slick('slickCurrentSlide')+1 == tamQuiz) {
                $('.jumpQuestion').addClass('disabled');
            }

            //Move para o proximo slick null
            var slick = [];
            $('.progression ul li').each(function(e) {
                var has = $(this).is('.right, .wrong, .jump');
                has == false ? slick.push(e) : '';
            })

            if (slick != '') {
                if (slick.length > 1) {$('.jumpQuestion').removeClass('disabled');}
                $('.quiz').slick('slickGoTo', slick[0]);
            } else {
                //Verifica se algum foi pulado
                $('.progression ul li').each(function(e) {
                    var has = $(this).is('.jump:not(.right):not(.wrong)');
                    has == true ? slick.push(e) : '';
                })
                
                if (slick != '') {
                    $('.progression ul li').removeClass('jump');
                    if (slick.length > 1) {$('.jumpQuestion').removeClass('disabled');}
                    $('.quiz').slick('slickGoTo', slick[0]);
                } else {
                    //Set Final do Quiz
                    APP.controller.Quiz.setFinalQuiz(tamQuiz);
                }
            }

            //Page
            var page = $('.quiz').slick('slickCurrentSlide')+1;
            $('.actions .pages span.init').text(page);

            //Explicacao
            $('.quiz .explanation').removeClass('active');

            //Botoes
            $('.actions .responder').addClass('disabled');
            $('.actions .responder').removeClass('disable');
            $('.actions .continuar').addClass('disable');

            $('html, body').animate({
                scrollTop: 0
            }, 500);

        });

    },
    
    setAnswerQuiz : function (result) {

        //Resposta
        $('.slick-active .answer .list input').each(function () {
            var index = parseInt($(this).val());

            $(this).next('label').css('pointer-events', 'none');

            if (index === result.correta) {
                $(this).next('label').addClass('right');

                var lacuna = $(this).closest('.item').find('.question p span').length;
                if (lacuna > 0) {
                    var texto = $(this).next('label').find('span').text();
                    $(this).closest('.item').find('.question p span').addClass('txt-question');
                    $(this).closest('.item').find('.question p span').text(texto);
                }

            } else {
                $(this).next('label').addClass('wrong');
            }

        });

        //Progressao
        $('.progression ul li:nth-of-type('+($('.quiz').slick('slickCurrentSlide') + 1)+')').removeClass('jump');
        if (result.acertou) {
            $('.progression ul li:nth-of-type('+($('.quiz').slick('slickCurrentSlide') + 1)+')').addClass('right');
        } else {
            $('.progression ul li:nth-of-type('+($('.quiz').slick('slickCurrentSlide') + 1)+')').addClass('wrong');
        }

        //Status
        var acertos = 0;
        var respondido = 0;
        var total = 0;
        $('.progression ul li').each(function (e) {
           if ($(this).hasClass('right')) {
                acertos++;
                respondido++;
           } else if ($(this).hasClass('wrong')) {
                respondido++;
           }
           total++;
        });
        $('.status .numbers .init').text(respondido)
        $('.status .percentage .total').text(((100 * acertos) / total));

        //Botoes
        $('.actions .responder').addClass('disable');
        $('.actions .continuar').removeClass('disable');

        $('.slick-active .answer .list label.active').next().addClass('active');
        $('.quiz').slick('setPosition');

        //Desabilita Botao
        $('.jumpQuestion').addClass('disabled');
        
        $('html, body').animate({
            scrollTop: $('body').outerHeight()
        }, 500);
        
        APP.controller.Quiz.setGoNextQuestion();

    },

    setAnswerQuizDegustacao : function () {

        //Resposta
        $('.slick-active .answer .list input').each(function () {
            var index = parseInt($(this).val());
            var bt = parseInt($(this).closest('.list').data('bt'));

            $(this).next('label').css('pointer-events', 'none');

            if (index === bt) {
                $(this).next('label').addClass('right');

                var lacuna = $(this).closest('.item').find('.question p span').length;
                if (lacuna > 0) {
                    var texto = $(this).next('label').find('span').text();
                    $(this).closest('.item').find('.question p span').addClass('txt-question');
                    $(this).closest('.item').find('.question p span').text(texto);
                }
                
            } else {
                $(this).next('label').addClass('wrong');
            }

        });

        //Progressao
        var checked = parseInt($('.slick-active .answer .list input:checked').val());
        var correta = parseInt($('.slick-active').find('.list').data('bt'));
        $('.progression ul li:nth-of-type('+($('.quiz').slick('slickCurrentSlide') + 1)+')').removeClass('jump');

        if (checked == correta) {
            $('.progression ul li:nth-of-type('+($('.quiz').slick('slickCurrentSlide') + 1)+')').addClass('right');
        } else {
            $('.progression ul li:nth-of-type('+($('.quiz').slick('slickCurrentSlide') + 1)+')').addClass('wrong');
        }

        //Status
        var acertos = 0;
        var respondido = 0;
        var total = 0;
        $('.progression ul li').each(function (e) {
            if ($(this).hasClass('right')) {
                    acertos++;
                    respondido++;
            } else if ($(this).hasClass('wrong')) {
                    respondido++;
            }
            total++;
        });

        $('.status .numbers .init').text(respondido)
        $('.status .percentage .total').text(((100 * acertos) / total));

        //Botoes
        $('.actions .responder').addClass('disable');
        $('.actions .continuar').removeClass('disable');

        $('.slick-active .answer .list label.active').next().addClass('active');
        $('.quiz').slick('setPosition');

        //Desabilita Botao
        $('.jumpQuestion').addClass('disabled');

        
        $('html, body').animate({
            scrollTop: $('body').outerHeight()
        }, 500);
        
        APP.controller.Quiz.setGoNextQuestion();

    },

    setGoSaibaMais : function () {

        $('.saiba-mais').on('click', function (event) {

            event.preventDefault();

            var guia = $(this).data('guia');
            window.location.href = '/Guia?guia='+guia+'';


        });

    },

    setRefazer : function () {

        $('.refazer-quiz').on('click', function () {

            var idQuiz = parseInt(window.location.search.slice(4));

            $.ajax({
                type: "POST",
                dataType: 'json',
                url: "/Quiz/FinalizarQuiz/?idQuiz=" + idQuiz,
                beforeSend: function () {
                    
                },
                success: function (result) {
                    window.location.href = "/Quiz?id="+idQuiz+"";
                },
                error: function (result) {
                    //APP.component.Alert.customAlert('', "Erro");
                },
                complete: function (result) {
                    
                }
            });

        });

    },

    setClickPerguntaRespondida : function () {

        $('.verify-question').on('click', function(event) {

            event.preventDefault();
            $('.quiz').slick('slickNext');
            $('.modal, #mask').fadeOut();

        });

    },

    //Send
    sendAnswerQuiz : function (_idPergunta, _idResposta) {

        var resposta = {
            'idPergunta': _idPergunta,
            'idResposta': _idResposta,
        }
       
        $.ajax({
            type: "POST",
            data: resposta,
            dataType: 'json',
            url: "/Quiz/Salvar",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.Quiz.setAnswerQuiz(result);
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro", 'verify-question', 'OK');
                APP.controller.Quiz.setClickPerguntaRespondida();

            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        });

    },

    getResultadoQuiz : function (_total, _tamQuiz) {

        var tamQuiz = _tamQuiz;
        var idQuiz = parseInt(window.location.search.slice(4));

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: "/Quiz/Resultado/?idQuiz=" + idQuiz + "&total=" + _total,
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.Quiz.setInfoFinalQuiz(result, tamQuiz);
            },
            error: function (result) {
                //APP.component.Alert.customAlert('', "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });

    },

};