/*
|--------------------------------------------------------------------------
| Controller Guia
|--------------------------------------------------------------------------
*/

APP.controller.Capitulo = {

    init : function () {

        this.setup();
        this.capitulo();
 
    },

    setup : function () {

        //

    },

    //Guia 
    capitulo : function () {

        this.verifyMobile();

        //GET
        this.getChapter();       

    },

    //HIDE and SHOW
    verifyMobile : function () {

        if ($(window).width() < 960) {
            $('body').addClass('cap mobile');
        } else {
            $('body').addClass('cap desktop');
        }

    },

    //GET
    getChapter : function () {

        var idCapitulo = APP.component.Utils.getUrlParameter('cap');
        var idSubCapitulo = APP.component.Utils.getUrlParameter('sub');

        $.ajax({
            type: "GET",
            data: {'idCapitulo' : idCapitulo, 'idSubCapitulo': idSubCapitulo},
            dataType: 'json',
            url: '/Guia/GetCapitulo',
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.Capitulo.setInfosChapter(result);
                APP.controller.Capitulo.bind(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                // alert('eita')
                APP.component.Loading.hide()
            }
        });
        
    },

    //SET
    // - Progresso Inicial
    setInitProgresso : function () {

        var login = APP.component.Utils.getLogin();
        if ( $(window).width() < 960 ) {
            var slideInit = $('.capitulo').slick('slickCurrentSlide');
        }

        if (login) {
            if ( $(window).width() < 960 ) {
                APP.controller.Capitulo.sendProgressoLeitura(slideInit);
            } else {
                APP.controller.Capitulo.sendProgressoLeitura(0);
            }
        }

    },

    // - Slick Capitulo
    setSlickCapitulo : function (capitulo) {

        var inicioSlick = 0;
        var page = APP.component.Utils.getUrlParameter('pag');

        if (page != undefined) {
            inicioSlick = page-1;
        } else if (capitulo.paginaStop > 0) {

            inicioSlick = capitulo.paginaStop-1;
        }

        if (window.location.search.indexOf('first') > 0) {
            inicioSlick = 0;
        }
        if (window.location.search.indexOf('last') > 0) {
            inicioSlick = capitulo.conteudoLista.length;
        }


        var configCapitulo = {
            //dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            arrows: false,
            asNavFor: '.nav-capitulo',
            adaptiveHeight: true,
            swipe: true,
            initialSlide: inicioSlick,
        }
        APP.component.Slick.init('capitulo', configCapitulo);

    },

    // - Slick Navegação Capitulos
    setSlickNavCapitulo : function () {

        var inicioSlick = 0;
        var page = APP.component.Utils.getUrlParameter('pag');

        if (page != undefined) {
            inicioSlick = page-1;
        } else if (capitulo.paginaStop > 0) {
            inicioSlick = capitulo.paginaStop-1;
        }

        var configCapitulo = {
            //dots: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            asNavFor: '.capitulo',
            adaptiveHeight: true,
            swipe: true,
            initialSlide: inicioSlick,
        }
        APP.component.Slick.init('nav-capitulo', configCapitulo);

    },

    // - HTML on page
    setInfosChapter : function (capitulo) {

        this.setHtmlPageMenu(capitulo);
        this.setHtmlPage(capitulo);
        this.setPorcentagemLeitura(capitulo);

    },

    //HTML - Menu
    setHtmlPageMenu : function (capitulo) {
        var htmlPageMenu = '';

        htmlPageMenu += '';

        htmlPageMenu += '<div class="close">';
        htmlPageMenu +=     '<a href="#">';
        htmlPageMenu +=         '<i class="fas fa-times fa-lg"></i>';
        htmlPageMenu +=     '</a>';
        htmlPageMenu += '</div>';
        htmlPageMenu += '<div class="title">';
        htmlPageMenu +=     '<p data-num-cap="'+capitulo.conteudoLista[0].idReferencia+'">';
        htmlPageMenu +=         ''+capitulo.conteudoLista[0].titulo+'';
        htmlPageMenu +=     '</p>';
        htmlPageMenu += '</div>';
        htmlPageMenu += '<div class="letter">';
        htmlPageMenu +=     '<i class="fas fa-minus"></i>';
        htmlPageMenu +=     '<i class="fas fa-plus"></i>';
        htmlPageMenu +=     '<img src="/assets/img/icons/icon-a.png" alt="" />';
        htmlPageMenu += '</div>';
        htmlPageMenu += '<div class="list-guias">';
        htmlPageMenu +=     '<a href="/Guia">';
        htmlPageMenu +=         '<img src="/assets/img/icons/icon-lista-guias.png" alt="Icone Lista Guias" />';
        htmlPageMenu +=     '</a>';
        htmlPageMenu += '</div>';

        $('.menu-cap').append(htmlPageMenu);

    },

    //::::: HTML - Page :::::
    setHtmlPage : function (capitulo) {

        if (capitulo.subCapituloFirst && $(window).width() <= 960) {
            APP.controller.Capitulo.setHtmlPageCover(capitulo);
            if (capitulo.porcentagemLeitura == 0){
                $('section').addClass('active');
            }
        }
        if ($(window).width() > 960) {
            APP.controller.Capitulo.setHtmlPageCover(capitulo);
        }

        $(capitulo.conteudoLista).each(function (index) {
            if (index == 0) {
                APP.controller.Capitulo.setHtmlPageFirst(capitulo, this);
            } else if (index == capitulo.conteudoLista.length-1){
                APP.controller.Capitulo.setHtmlPageLast(capitulo, this);
            } else {
                APP.controller.Capitulo.setHtmlPageMiddle(this);
            }
        });

    },

    //HTML - Page Cover
    setHtmlPageCover : function (capitulo) {

        var htmlPageCover = '';

        htmlPageCover += '';

        htmlPageCover += '<div class="item">';
        htmlPageCover +=    '<div class="wrap">';
        htmlPageCover +=        '<div class="pages">';
        htmlPageCover +=            '<span class="init">0</span> / <span class="end">'+capitulo.conteudoLista.length+'</span>';
        htmlPageCover +=        '</div>';
        htmlPageCover +=        '<div class="cover-cap cap-0'+capitulo.idCapitulo+'">';
        htmlPageCover +=            '<div class="trigger"></div>';
        htmlPageCover +=            '<div class="number">';
        htmlPageCover +=                '<span>Capítulo</span>';
        htmlPageCover +=                '<span>'+capitulo.idCapitulo+'.0</span>';
        htmlPageCover +=            '</div>';
        htmlPageCover +=            '<div class="title">';
        htmlPageCover +=                '<p>'+capitulo.titulo+'</p>';
        htmlPageCover +=                '<p>'+capitulo.dsCapitulo+'</p>';
        htmlPageCover +=            '</div>';
        htmlPageCover +=            '<div class="enter">';
        htmlPageCover +=                '<a href="" class="button">';
        htmlPageCover +=                    '<i class="fas fa-chevron-right"></i>';
        htmlPageCover +=                '</a>';
        htmlPageCover +=            '</div>';
        htmlPageCover +=        '</div>';
        htmlPageCover +=    '</div>';
        htmlPageCover += '</div>';

        $('.capitulo').append(htmlPageCover);
        $('.nav-capitulo').append(htmlPageCover);

    },

    //HTML - Pages First
    setHtmlPageFirst : function (capitulo, _this) {

        var htmlPageFirst= '';
        
        htmlPageFirst += '';

        htmlPageFirst += '<div class="item">';
        htmlPageFirst +=    '<div class="wrap">';
        htmlPageFirst +=        '<div class="trigger-nav-caps">';
        htmlPageFirst +=            '<div>';
        htmlPageFirst +=            '</div>';
        htmlPageFirst +=            '<div>';
        htmlPageFirst +=                '<a  href="/Home"><img src="/assets/img/logo-guia-do-voto.png" alt="Guia do Voto" class="log-cap"></a>';
        htmlPageFirst +=            '</div>';
        htmlPageFirst +=            '<div>';
        htmlPageFirst +=                '<img src="/assets/img/icons/icon-engrenagem.png" class="engrenagem" />';
        htmlPageFirst +=            '</div>';
        htmlPageFirst +=        '</div>';
        htmlPageFirst +=       '<div class="page">';
        htmlPageFirst +=           '<div class="topo">';
        htmlPageFirst +=               '<span>'+capitulo.conteudoLista[0].idReferencia+'</span>';
        htmlPageFirst +=               '<p>';
        htmlPageFirst +=                   ''+_this.titulo+'';
        htmlPageFirst +=               '</p>';
        htmlPageFirst +=           '</div>';
        htmlPageFirst +=           '<div class="texto">';
        htmlPageFirst +=               '<p>';
        htmlPageFirst +=                   ''+_this.texto+'';
        htmlPageFirst +=               '</p>';
        htmlPageFirst +=           '</div>';
        htmlPageFirst +=       '</div>';
        htmlPageFirst +=       '<div class="actions">';
        if (capitulo.idCapitulo === 1 && capitulo.subCapituloFirst) {
            htmlPageFirst +=             '<a href="#" class="prev-cap">';
        } else {
            if (capitulo.subCapituloFirst) {
                htmlPageFirst +=             '<a href="/Guia/Capitulo?cap='+(capitulo.idCapitulo-1)+'&sub='+capitulo.prevIdReferencia+'&last" class="next-cap">';
                
            } else {
                htmlPageFirst +=             '<a href="/Guia/Capitulo?cap='+(capitulo.idCapitulo)+'&sub='+capitulo.prevIdReferencia+'&last" class="next-cap">';
            }
        }
        htmlPageFirst +=               '<i class="fas fa-chevron-left"></i>';
        htmlPageFirst +=           '</a>';
        htmlPageFirst +=           '<p><span class="numero-page">'+(_this.porcentagem * _this.pagina).toFixed(2)+'</span> %</p>';
        htmlPageFirst +=           '<a href="#" class="next-cap">';
        htmlPageFirst +=               '<i class="fas fa-chevron-right"></i>';
        htmlPageFirst +=           '</a>';
        htmlPageFirst +=       '</div>';
        htmlPageFirst +=   '</div>';
        htmlPageFirst += '</div>';

        $('.capitulo').append(htmlPageFirst);
        $('.nav-capitulo').append(htmlPageFirst);

    },

    //HTML - Pages Middle
    setHtmlPageMiddle : function (_this) {

        var htmlPageMiddle = '';

        htmlPageMiddle += '';

        htmlPageMiddle += '<div class="item">';
        htmlPageMiddle +=   '<div class="wrap">';
        htmlPageMiddle +=       '<div class="trigger-nav-caps">';
        htmlPageMiddle +=           '<div>';
        htmlPageMiddle +=           '</div>';
        htmlPageMiddle +=           '<div>';
        htmlPageMiddle +=               '<a  href="/Home"><img src="/assets/img/logo-guia-do-voto.png" alt="Guia do Voto" class="log-cap"></a>';
        htmlPageMiddle +=           '</div>';
        htmlPageMiddle +=           '<div>';
        htmlPageMiddle +=               '<img src="/assets/img/icons/icon-engrenagem.png" class="engrenagem"/>';
        htmlPageMiddle +=           '</div>';
        htmlPageMiddle +=       '</div>';
        htmlPageMiddle +=       '<div class="page">';
        htmlPageMiddle +=           '<div class="texto">';
        htmlPageMiddle +=               '<p>';
        htmlPageMiddle +=                   ''+_this.texto+'';
        htmlPageMiddle +=               '</p>';
        htmlPageMiddle +=           '</div>';
        htmlPageMiddle +=       '</div>';
        htmlPageMiddle +=       '<div class="actions">';
        htmlPageMiddle +=           '<a href="#" class="prev-cap">';
        htmlPageMiddle +=               '<i class="fas fa-chevron-left"></i>';
        htmlPageMiddle +=           '</a>';
        htmlPageMiddle +=           '<p><span class="numero-page">'+(_this.porcentagem * _this.pagina).toFixed(2)+'</span> %</p>';
        htmlPageMiddle +=           '<a href="#" class="next-cap">';
        htmlPageMiddle +=               '<i class="fas fa-chevron-right"></i>';
        htmlPageMiddle +=           '</a>';
        htmlPageMiddle +=       '</div>';
        htmlPageMiddle +=   '</div>';
        htmlPageMiddle += '</div>';

        $('.capitulo').append(htmlPageMiddle);
        $('.nav-capitulo').append(htmlPageMiddle);

    },

    //HTML - Page Last
    setHtmlPageLast : function (capitulo, _this) {

        var htmlPageLast = '';

        htmlPageLast += '';
        
        htmlPageLast += '<div class="item">';
        htmlPageLast +=     '<div class="wrap final-page">';
        htmlPageLast +=         '<div class="trigger-nav-caps">';
        htmlPageLast +=             '<div>';
        htmlPageLast +=             '</div>';
        htmlPageLast +=             '<div>';
        htmlPageLast +=                 '<a  href="/Home"><img src="/assets/img/logo-guia-do-voto.png" alt="Guia do Voto" class="log-cap"></a>';
        htmlPageLast +=             '</div>';
        htmlPageLast +=             '<div>';
        htmlPageLast +=                 '<img src="/assets/img/icons/icon-engrenagem.png" class="engrenagem" />';
        htmlPageLast +=             '</div>';
        htmlPageLast +=         '</div>';
        htmlPageLast +=         '<div class="page">';
        htmlPageLast +=             '<div class="texto">';
        htmlPageLast +=                 '<p>';
        htmlPageLast +=                     ''+_this.texto+'';
        htmlPageLast +=                 '</p>';
        htmlPageLast +=             '</div>';
        htmlPageLast +=             '<div class="final">';
        htmlPageLast +=                 '<p>';
        htmlPageLast +=                     'Teste seus conhecimentos sobre o Guia do Voto';
        htmlPageLast +=                 '</p>';
        htmlPageLast +=                 '<a href="/Quiz?id='+capitulo.idQuiz+'" class="btn iniciar-quiz">';
        htmlPageLast +=                     '<span>COMEÇAR</span>';
        htmlPageLast +=                 '</a>';
        htmlPageLast +=             '</div>';
        htmlPageLast +=         '</div>';
        htmlPageLast +=         '<div class="actions">';

        if (capitulo.idCapitulo === 1 && capitulo.subCapituloFirst && $(window).width() > 960) {
            htmlPageLast +=             '<a href="#" class="">'; 
        } else {
            if ($(window).width() < 960) {
                htmlPageLast +=             '<a href="#" class="prev-cap">';            
            } else {
                if (capitulo.subCapituloFirst) {
                    htmlPageLast +=             '<a href="/Guia/Capitulo?cap='+(capitulo.idCapitulo-1)+'&sub='+capitulo.prevIdReferencia+'&last" class="next-cap">';
                    
                } else {
                    htmlPageLast +=             '<a href="/Guia/Capitulo?cap='+(capitulo.idCapitulo)+'&sub='+capitulo.prevIdReferencia+'&last" class="next-cap">';
                }
            }
            htmlPageLast +=                 '<i class="fas fa-chevron-left"></i>';
        }
        
        htmlPageLast +=             '</a>';
        htmlPageLast +=             '<p><span class="numero-page">'+Math.round((_this.porcentagem * _this.pagina).toFixed(2) / 10) * 10+'</span> %</p>';
        
        if (capitulo.idCapitulo === 6 && capitulo.subCapituloLast) {
            htmlPageLast +=             '<a href="#" class="">'; 
        } else {
            if (capitulo.subCapituloLast) {
                htmlPageLast +=             '<a href="/Guia/Capitulo?cap='+(capitulo.idCapitulo+1)+'&sub='+(capitulo.idCapitulo+1)+'&first" class="next-cap">';
            } else {
                htmlPageLast +=             '<a href="/Guia/Capitulo?cap='+(capitulo.idCapitulo)+'&sub='+(capitulo.nextIdReferencia)+'&first" class="next-cap">';
            }
            htmlPageLast +=                 '<i class="fas fa-chevron-right"></i>';
        }

        htmlPageLast +=             '</a>';
        htmlPageLast +=         '</div>';
        htmlPageLast +=     '</div>';
        htmlPageLast += '</div>';

        $('.capitulo').append(htmlPageLast);
        $('.nav-capitulo').append(htmlPageLast);
        
    },

    //Porcentagem
    setPorcentagemLeitura : function (capitulo) {

        $('.progresso .bar').css('width', ''+capitulo.porcentagemLeitura+'%');

    },

    setResizeReload : function () {

        var o = $(window).width()

        $(window).on('resize', function() {
            var c = $(window).width();

            if (o > 960 && c < o) {
                window.location.reload();
            }

            if (o < 960 && c > o) {
                window.location.reload();
            }

        });

    },

    //INTERAÇÃO
    setClickCapa : function () {

        $('.capitulo .item .wrap .cover-cap .trigger, .capitulo .item .wrap .page').on('click', function () {

            if ($(window).width() < 960) {
                $(this).closest('#capitulo').stop().toggleClass('active');
                $('.capitulo').slick('setPosition');
                $('.nav-capitulo').slick('setPosition');

                setTimeout(function() {
                    $('.capitulo').slick('setPosition');
                }, 500);
            }

        });

        $('body').on('click', '.capitulo .item .wrap .page .texto a',function(event) {
            $(this).closest('#capitulo').stop().removeClass('active');
        });

    },

    setClickEnter : function () {

        $('.enter a').on('click', function (event) {
            event.preventDefault();
            $('.capitulo').slick('slickNext');
        });

    },

    setClickEngrenagem : function () {

        $('.trigger-nav-caps .engrenagem, .close').on('click', function () {
            if ($(window).width() < 960) {
                $(this).closest('#capitulo').stop().toggleClass('active');
                $('.capitulo').slick('setPosition');
                $('.nav-capitulo').slick('setPosition');
            }

            setTimeout(function() {
                $('.capitulo').slick('setPosition');
            }, 500);

        });

    },

    setNextSlide : function () {
      
        $('.actions .next-cap').on('click', function (event) {

            if ($(this).attr('href') === "#") {
                event.preventDefault();
                $('.capitulo').slick('slickNext');
            }

        });
        
    },

    setPrevSlide : function () {
      
        $('.actions .prev-cap').on('click', function (event) {

            if ($(this).attr('href') === "#") {
                event.preventDefault();
                $('.capitulo').slick('slickPrev');
            }

        });
        
    },

    setClickNavCap : function () {

        $('.nav-capitulo .item').on('click', function (event) {

            event.preventDefault();

            var index = $(this).data('slick-index');
            $('.capitulo').slick('slickGoTo', index)

        });

    },

    setClickImagemTexto : function () {

        $('.img-texto').on('click', function (event) {

            var valueHref = $(this).attr('href');
            
            if (valueHref == '#') {
                event.preventDefault();
                var html = '<img src="/assets/img/guia/img-texto/'+$(this).data('url-img')+'" alt="'+$(this).data('img-name')+'" />';
                APP.component.Alert.customHTML($(this).data('img-name'), '', html);
                $('section').addClass('blur');
                $('#mask').css('background', 'rgba(226, 226, 226, 0.7)');
            }

        });

    },

    setClickProgressoLeitura : function () {

        $('.capitulo').on('afterChange', function(event, slick, currentSlide, nextSlide){
            
            var login = APP.component.Utils.getLogin();

            if (login) {
                APP.controller.Capitulo.sendProgressoLeitura(currentSlide);
            }
        
        });

    },

    setScrollProgressoLeitura : function () {
        var timeout;
        var last = 0;
        $('.capitulo').on('scroll', function() {

            $('.capitulo > .item').each(function(index, el) {
                if ($(this).offset().top > 0 && $(this).offset().top < $(window).height() && index > 0) {
                    last = index - 1;
                }
            });
            clearTimeout(timeout);
            timeout = setTimeout(function(){
                APP.controller.Capitulo.sendProgressoLeituraDesktop(last);
            }, 1000);

        });
    },

    setEdgeSlick : function (capitulo) {
        $('.capitulo').on('edge', function(event, slick, direction){

            if (direction == 'right') {

                if (capitulo.idCapitulo === 1 && capitulo.subCapituloFirst) {

                } else {
                    if (capitulo.subCapituloFirst) {
                        window.location.href = '/Guia/Capitulo?cap='+(capitulo.idCapitulo-1)+'&sub='+capitulo.prevIdReferencia+'&last';
                    } else {
                        window.location.href = '/Guia/Capitulo?cap='+(capitulo.idCapitulo)+'&sub='+capitulo.prevIdReferencia+'&last';
                    }
                }

            } else {

                if (capitulo.idCapitulo === 6 && capitulo.subCapituloLast) {

                } else {
                    if (capitulo.subCapituloLast) {
                        window.location.href = '/Guia/Capitulo?cap='+(capitulo.idCapitulo+1)+'&sub='+(capitulo.idCapitulo+1)+'&first';
                    } else {
                        window.location.href = '/Guia/Capitulo?cap='+(capitulo.idCapitulo)+'&sub='+capitulo.nextIdReferencia+'&first';
                    }
                }

            }
        });

    },

    //BIND
    bind : function (capitulo) {

        //SET
        if ($(window).width() < 960) {
            APP.controller.Capitulo.setSlickCapitulo(capitulo);
            APP.controller.Capitulo.setSlickNavCapitulo(capitulo);  
            APP.controller.Capitulo.setClickProgressoLeitura();            
        } else {
            APP.controller.Capitulo.setScrollProgressoLeitura();
        }

        //INTERACAO
        APP.controller.Capitulo.setInitProgresso();
        APP.controller.Capitulo.setClickCapa();
        APP.controller.Capitulo.setClickEnter();
        APP.controller.Capitulo.setClickEngrenagem();
        APP.controller.Capitulo.setNextSlide();
        APP.controller.Capitulo.setPrevSlide();
        APP.controller.Capitulo.setClickNavCap();
        APP.controller.Capitulo.setClickImagemTexto();
        APP.controller.Capitulo.setResizeReload();
        APP.controller.Capitulo.setEdgeSlick(capitulo);
        APP.component.FontSize.init();
        

    },

    //SEND
    sendProgressoLeitura : function (pagina) {

        var idReferencia = APP.component.Utils.getUrlParameter('sub');

        var progressoLeitura = {
            'idReferencia': idReferencia,
            'pagina': pagina + 1,
        }

        $.ajax({
            type: "POST",
            data: progressoLeitura,
            dataType: 'json',
            url: "/Guia/Salvar",
            beforeSend: function () {
                
            },
            success: function (result) {
                //
            },
            error: function (result) {
                //
            },
            complete: function (result) {
                
            }
        });

    },

    sendProgressoLeituraDesktop : function (pagina) {

        var idReferencia = APP.component.Utils.getUrlParameter('sub');

        var progressoLeitura = {
            'idReferencia': idReferencia,
            'pagina': pagina + 1,
        }

        $.ajax({
            type: "POST",
            data: progressoLeitura,
            dataType: 'json',
            url: "/Guia/SalvarDesktop",
            beforeSend: function () {
                
            },
            success: function (result) {
                //
            },
            error: function (result) {
                //
            },
            complete: function (result) {
                
            }
        });

    },


}