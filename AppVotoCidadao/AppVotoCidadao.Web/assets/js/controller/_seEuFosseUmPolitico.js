/*
|--------------------------------------------------------------------------
| Controller Se eu fosse um politico
|--------------------------------------------------------------------------
*/

APP.controller.SeEuFosseUmPolitico = {

    init : function () {

        this.setup();

        if ($('section#ranking-partidos').length > 0) {
            APP.controller.RankingPartidos.init();
        } else {
            this.seeufosseumpolitico();
        }

    }, 

    setup : function () {

        if (isMobile.apple.device ) {
            $('body').addClass('ios');

            if (typeof localStorage.platform !== "undefined") {
                $('body').addClass('ios-app');
            }
        }

    },

    seeufosseumpolitico : function () { 

        var login = APP.component.Utils.getLogin();

        if (login) {
            this.getQuiz();
        } else {
            this.getQuizDegustacao();
        }

        this.tutorial();


    },

    //QUIZ
    getQuiz : function () {

        var slickInitial = 0;

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/SeEuFosseUmPolitico/BuscarPerguntas',
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {

                //Verifica se esta finalizado
                if (result.finalizado == true) {
                    window.location.href = '/SeEuFosseUmPolitico/RankingPartidos';
                } else {
                    if (result.idStop < 10) {
                        slickInitial = result.idStop;
                    } else {
                        slickInitial = result.idStop - 10;
                    }
                    APP.controller.SeEuFosseUmPolitico.setInfosQuiz(result);
                    APP.controller.SeEuFosseUmPolitico.setInfosRespostas(result);
                }
            },
            error: function (result) {
                
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {
            //Slick
            APP.controller.SeEuFosseUmPolitico.setSlickQuiz(slickInitial);
            APP.controller.SeEuFosseUmPolitico.setSlickRespostas(slickInitial);
            //Bind
            APP.controller.SeEuFosseUmPolitico.bind();
        });

    },

    setInfosQuiz : function (quiz) {

        var quizStop = {};
        var index = 1;
        var qtdPerguntas = 10;

        //Verifica qual Lista deve apresentar
        if (quiz.idStop < 10) {
            quizStop = quiz.perguntasIniciais;
        } else {
            quizStop = quiz.perguntasFinais;
            index = 11;
            qtdPerguntas = 20;
        }

        $(quizStop).each(function (e) {

            var html = `
                <div class="item">
                    <div class="wrap">
                        <!-- Question -->
                        <div class="question" data-question="${this.id}">
                            <p class="number">
                                Questão <span class="init">${index}</span> / <span class="end">${qtdPerguntas}</span>
                            </p>
                            <p class="text-question">
                                ${this.pergunta}
                            </p>
                        </div>
                        <!-- Resumo -->
                        <div class="resumo-ementa">
                            <span>Resumo</span>
                            <p>
                            ${this.resumo}
                            </p>
                            <div class="stats">
                                <p>
                                    Projeto <span class="">${this.projeto}</span>
                                </p>
                                <p>
                                    ID Votação <span class="">${this.idVotacao}</span>
                                </p>
                                <a href="${this.link}" target="_blank">Fonte Oficial</a>
                            </div>
                            <span>Ementa</span>
                            <p>
                                ${this.ementa}
                            </p>
                        </div>
                    </div>
                </div>
            `;

            index ++;
    
            $('.quiz').append(html);

        })

    },

    setInfosRespostas : function (quiz) {

        var quizStop = {};
        var index = 1;
        var qtdPerguntas = 10;

        //Verifica qual Lista deve apresentar
        if (quiz.idStop < 10) {
            quizStop = quiz.perguntasIniciais;
        } else {
            quizStop = quiz.perguntasFinais;
            index = 11;
            qtdPerguntas = 20;
        }


        $.each(quizStop, function(i, val) {
            

            var html = `
            <div class="item">
                <div class="wrap">
                    <!-- Question -->
                    <div class="question" data-question="${val.id}">
                        <p class="number">
                            Questão <span class="init">${index}</span> / <span class="end">${qtdPerguntas}</span>
                        </p>
                        <p class="text-question">
                            ${val.pergunta}
                        </p>
                    </div>
                    <!-- Lista -->
                    <div class="list">
                        <input type="radio" name="answer" id="answer-${i+1}-sim" value="S" >
                        <label for="answer-${i+1}-sim"><span>Sim</span></label>
                        <input type="radio" name="answer" id="answer-${i+1}-nao" value="N">
                        <label for="answer-${i+1}-nao"><span>Não</span></label>
                        <input type="radio" name="answer" id="answer-${i+1}-abstencao" value="A">
                        <label for="answer-${i+1}-abstencao"><span>Abstenção</span></label>
                    </div>
                    <!-- Partidos -->
                    <div class="partidos">
                        <div class="title active">
                            <span>Veja como os partidos votaram neste tema de acordo com dados da Câmara dos Deputados</span>
                        </div>
                        <div class="lista-partidos active">
                        <div class="help-box">
                            <p>
                                Fonte: Banco de Dados do Legislativo do CEBRAP/NECI construído com base em informações oficiais. Os resultados não incluem os partidos 
                                ${val.partidosExcluidos.map((e, index) =>
                                    (index + 1 == val.partidosExcluidos.length ?
                                        ` e <a target="_blank" href="http://${e.link}">${e.nome}</a>`
                                        : ` <a target="_blank" href="http://${e.link}">${e.nome}</a>${index + 1 === val.partidosExcluidos.length - 1 ? `` : `,`}`
                                    )
                                ).join('')}, por não estarem representados na Câmara quando foi realizada a votação da matéria.
                            </p>
                        </div>
                            <div class="legenda">
                                
                                <span class="tag">Sim</span>
                                <span class="tag">Não</span>
                                <span class="tag">Abstenção</span>
                            </div>
                            <div class="lista">
                            ${val.partidos.map( e =>
                                `<div class="item" data-sim="${e.sim}" data-nao="${e.nao}" data-abstencao="${e.abstentacao}">
                                    <span>${e.sigla}</span>
                                    <div class="progression">
                                        <ul>
                                        ${
                                            e.sim != 0 ? `<li class="sim" style="width: ${e.sim < 15 ? 15 : e.sim}%" data-number="${e.sim}"><span>${e.sim} %</span></li>` : ``
                                        }
                                        ${
                                            e.nao != 0 ? `<li class="nao" style="width: ${e.nao < 15 ? 15 : e.nao}%" data-number="${e.nao}"><span>${e.nao} %</span></li>` : ``
                                        }
                                        ${
                                            e.abstentacao != 0 ? `<li class="abstencao" style="width: ${e.abstentacao < 15 ? 15 : e.abstentacao}%" data-number="${e.abstentacao}"><span>${e.abstentacao} %</span></li>` : ``
                                        }
                                        </ul>
                                    </div>
                                </div>`
                            ).join('')}
                            </div>
                        </div>
                    </div>
                    <!-- Botao -->
                    <div class="next-question">
                        <a href="#" class="next">
                            <span>Próxima Pergunta</span>
                            <img src="/assets/img/icons/icon-arrow-blue.png">
                        </a>
                    </div>
                </div>
            </div>
            `;

            index ++;
            
            $('.respostas').append(html);
        
        });

    },

    //Quiz Degustacao
    getQuizDegustacao : function () {

        var slickInitial = 0;

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/SeEuFosseUmPolitico/BuscarPerguntas',
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.SeEuFosseUmPolitico.setInfosQuizDegustacao(result);
                APP.controller.SeEuFosseUmPolitico.setInfosRespostasDegustacao(result);
                APP.controller.SeEuFosseUmPolitico.setInfosFinalQuizDegustacao(result);
            },
            error: function (result) {
                
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {
            //Slick
            APP.controller.SeEuFosseUmPolitico.setSlickQuiz(slickInitial);
            APP.controller.SeEuFosseUmPolitico.setSlickRespostas(slickInitial);
            //Bind
            APP.controller.SeEuFosseUmPolitico.bind();
        });

    },

    setInfosQuizDegustacao : function (quiz) {

        $(quiz.perguntasIniciais).each(function (e) {

            var html = `
                <div class="item">
                    <div class="wrap">
                        <!-- Question -->
                        <div class="question" data-question="${this.id}">
                            <p class="number">
                                Questão <span class="init">${e+1}</span> / <span class="end">${quiz.perguntasIniciais.length}</span>
                            </p>
                            <p class="text-question">
                                ${this.pergunta}
                            </p>
                        </div>
                        <!-- Resumo -->
                        <div class="resumo-ementa">
                            <span>Resumo</span>
                            <p>
                            ${this.resumo}
                            </p>
                            <div class="stats">
                                <p>
                                    Projeto <span class="">${this.projeto}</span>
                                </p>
                                <p>
                                    ID Votação <span class="">${this.idVotacao}</span>
                                </p>
                                <a href="${this.link}" target="_blank">Fonte Oficial</a>
                            </div>
                            <span>Ementa</span>
                            <p>
                                ${this.ementa}
                            </p>
                        </div>
                    </div>
                </div>
            `;
    
            $('.quiz').append(html);

        })

    },

    setInfosRespostasDegustacao : function (quiz) {
        
        $.each(quiz.perguntasIniciais, function(i, val) {

            var html = `
            <div class="item">
                <div class="wrap">
                    <!-- Question -->
                    <div class="question" data-question="${val.id}">
                        <p class="number">
                            Questão <span class="init">${i+1}</span> / <span class="end">${quiz.perguntasIniciais.length}</span>
                        </p>
                        <p class="text-question">
                            ${val.pergunta}
                        </p>
                    </div>
                    <!-- Lista -->
                    <div class="list">
                        <input type="radio" name="answer" id="answer-${i+1}-sim" value="S" >
                        <label for="answer-${i+1}-sim"><span>Sim</span></label>
                        <input type="radio" name="answer" id="answer-${i+1}-nao" value="N">
                        <label for="answer-${i+1}-nao"><span>Não</span></label>
                        <input type="radio" name="answer" id="answer-${i+1}-abstencao" value="A">
                        <label for="answer-${i+1}-abstencao"><span>Abstenção</span></label>
                    </div>
                    <!-- Partidos -->
                    <div class="partidos">
                        <div class="title active">
                            <span>Veja como os partidos votaram neste tema de acordo com dados da Câmara dos Deputados</span>
                        </div>
                        <div class="lista-partidos active">
                        <div class="help-box">
                            <p>
                                Fonte: Banco de Dados do Legislativo do CEBRAP/NECI construído com base em informações oficiais.
                            </p>
                        </div>
                          
                            <div class="legenda">
                                
                                <span class="tag">Sim</span>
                                <span class="tag">Não</span>
                                <span class="tag">Abstenção</span>
                            </div>
                            <div class="lista">
                            ${val.partidos.map( e =>
                                `<div class="item" data-sim="${e.sim}" data-nao="${e.nao}" data-abstencao="${e.abstentacao}">
                                    <span>${e.sigla}</span>
                                    <div class="progression">
                                        <ul>
                                        ${
                                            e.sim != 0 ? `<li class="sim" style="width: ${e.sim < 15 ? 15 : e.sim}%" data-number="${e.sim}"><span>${e.sim} %</span></li>` : ``
                                        }
                                        ${
                                            e.nao != 0 ? `<li class="nao" style="width: ${e.nao < 15 ? 15 : e.nao}%" data-number="${e.nao}"><span>${e.nao} %</span></li>` : ``
                                        }
                                        ${
                                            e.abstentacao != 0 ? `<li class="abstencao" style="width: ${e.abstentacao < 15 ? 15 : e.abstentacao}%" data-number="${e.abstentacao}"><span>${e.abstentacao} %</span></li>` : ``
                                        }
                                        </ul>
                                    </div>
                                </div>`
                            ).join('')}
                            </div>
                        </div>
                    </div>
                    <!-- Botao -->
                    <div class="next-question">
                        <a href="#" class="next">
                            <span>Próxima Pergunta</span>
                            <img src="/assets/img/icons/icon-arrow-blue.png">
                        </a>
                    </div>
                </div>
            </div>
            `;
            
            $('.respostas').append(html);
        
        });

    },

    setInfosFinalQuizDegustacao : function () {

        var html = `
            <div class="mp-final-quiz-degustacao">
                <div class="wrap">
                    <div class="imagem-final">
                        <img src="assets/img/seeufosseumpolitico/bg-final-degustacao.png">
                    </div>
                    <div class="texto">
                        <p>
                            Quer acessar outras votações
                            e descobrir quais partidos têm
                            opiniões próximas às suas?
                        </p>
                        <a href="/Cadastro" class="btn">
                            <span>CADASTRE-SE</span>
                        </a>
                    </div>
                </div>
            </div>
        `;

        $('.container').append(html);


    },

    //Slick
    setSlickQuiz : function (_slickInitial) {

        var configQuiz = {
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            draggable: false,
            swipe: false,
            touchMove: false,
            fade: false,
            asNavFor: '.respostas',
            adaptiveHeight: true,
            initialSlide: _slickInitial,
        }
        APP.component.Slick.init('quiz', configQuiz);

    },

    setSlickRespostas : function (_slickInitial) {

        var configRespostas = {
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            draggable: false,
            swipe: false,
            touchMove: false,
            fade: true,
            initialSlide: _slickInitial,
            //adaptiveHeight: true,
        }
        APP.component.Slick.init('respostas', configRespostas);

    },

    setSlickTutorial : function () {

        $('.tutorial-steps').slick({
            arrows: false,
            dots: true,
            infinite: false,
            adaptiveHeight: false,
        });

    },

    bind : function () {

        APP.controller.SeEuFosseUmPolitico.setClickResposta();
        APP.controller.SeEuFosseUmPolitico.setClickProximaPergunta();
        APP.controller.SeEuFosseUmPolitico.setClickInformacoes();

    },

    //Tutorial
    tutorial : function () {

        var leuTutorial = localStorage.getItem('LeuTutorialSeEuFosseUmPolitico');
        var login = APP.component.Utils.getLogin();

        if (leuTutorial != 'true' && login) {
            $('#tutorial').fadeIn();
            // APP.controller.SeEuFosseUmPolitico.setSlickTutorial();
            APP.controller.SeEuFosseUmPolitico.setClickTutorialComecar();
        }

    },

    setLeuTutorial : function () {

        var leuTutorial = localStorage.getItem('LeuTutorialSeEuFosseUmPolitico');
        if (leuTutorial == null) {
            localStorage.setItem('LeuTutorialSeEuFosseUmPolitico', true);
        }

    },

    //INTERACOES
    setClickResposta : function () {

        $('.escolha .list input').on('change', function () {

            var current = $('.quiz').slick('slickCurrentSlide');
            var idQuestion = $(this).closest('.container').find('.respostas .slick-active .wrap .question').data('question');
            var resposta = $(this).closest('.list').find('input:checked').val();
            
            $(this).closest('.escolha').fadeOut();
            $(this).closest('.container').find('.respostas').addClass('active');
            $(this).closest('.container').find('.respostas .slick-active .wrap .list input[type=radio][value='+resposta+']').prop('checked', true);

            $('.quiz').slick('setPosition');
            $('body').css('overflow', 'hidden');
            $('section').addClass('changed');

            var escolha = $(this).closest('.container').find('.respostas .slick-active .wrap .list input[type=radio]:checked').attr('id').split('-');
            
            //Ordena Lista
            tinysort('.respostas .slick-active .wrap .partidos .lista-partidos .lista .item',{
                data: escolha[2],
                order: 'desc',
                emptyEnd: true,
            });
            
            APP.controller.SeEuFosseUmPolitico.sendRespostaQuiz(idQuestion, resposta);

        })

    },
    
    setClickProximaPergunta : function () {

        $('.next-question').off('click');
        $('.next-question').on('click', function () {

            //Resets
            $('.wrap').removeClass('active');
            $('body').css('overflow', 'auto');
            $('section').removeClass('changed');
            $(this).closest('.container').find('.respostas').removeClass('active');
            $('input').prop('checked', false);
            $('body').find('.escolha').fadeIn();

            APP.controller.SeEuFosseUmPolitico.setNextQuestion();

        })
    
    },

    setNextQuestion : function () {

        var login = APP.component.Utils.getLogin();


        var count = $('.quiz').slick('getSlick').slideCount;
        var current = $('.quiz').slick('slickCurrentSlide')+1;

        //Verifica em qual pergunta esta
        if (current == count) {
            if (login) {
                APP.component.LocalStorage.setPorcentagemMinhasPrioridades('seeufosseumpolitico');
                window.location.href = '/SeEuFosseUmPolitico/RankingPartidos';
            } else {
                $('.mp-final-quiz-degustacao').fadeIn();
            }
        } else if (current == (count - 1)) {
            $('.quiz').slick('slickNext');
            $('.respostas .slick-active').find('.next-question span').text('Visualizar Resultado');
        } else {
            $('.quiz').slick('slickNext');
        }

    },

    setClickInformacoes : function () {
        $('a.help').on('click', function (event, data) {

            event.preventDefault();
            
            var data = $(this).data('help');

            var pergunta = `
                <strong class="blue">"Abstenção"</strong>
                é quando um parlamentar não quer se posicionar sobre a questão, mas deseja registrar sua presença para permitir o quórum exigido na votação.
                <br><br>
                <strong class="blue">"Obstrução"</strong>
                quando um parlamentar se recusa a votar uma questão por determinação de seu partido ou bloco, e tenta impedir que a votação aconteça utilizando recursos como pedir seu adiamento ou se retirar do Plenário para que o quórum seja insuficiente.
                <br><br>
                <span class="ps"><strong>Nas questões da seção ‘Se eu fosse um político’, eventuais posicionamentos de obstrução estão sendo considerados como votos "Não", tendo em vista que em nenhum caso atingiu número de votos necessários para obstruir a votação da matéria.</strong></span>
            `;    

            if (data == 'pergunta') {
                APP.component.Alert.modalInformacoes(' ', pergunta, '/assets/img/seeufosseumpolitico/bg-informacoes.png', 'se-eu-fosse');
            } else if (data == 'resposta'){
                APP.component.Alert.modalInformacoes(' ', pergunta, '/assets/img/seeufosseumpolitico/bg-informacoes.png', 'se-eu-fosse');
            }

        })
    },

    setClickTutorialComecar : function () {

        $('#tutorial a.comecar').on('click', function (event) {

            event.preventDefault();

            APP.controller.SeEuFosseUmPolitico.setLeuTutorial();
            $('#tutorial').fadeOut();

        });

    },

    //Send
    sendRespostaQuiz : function (_idQuestion, _resposta) {

        var data = {
            'id': _idQuestion, 
            'resposta': _resposta
        }

        $.ajax({
            type: "POST",
            data: data,
            dataType: 'json',
            url: "/SeEuFosseUmPolitico/Salvar",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                
            },
            error: function (result) {

            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        });

    },

};
