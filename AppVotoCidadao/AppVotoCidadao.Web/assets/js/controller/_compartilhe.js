/*
|--------------------------------------------------------------------------
| Controller Compartilhe
|--------------------------------------------------------------------------
*/

APP.controller.Compartilhe = {

    init : function () {

        this.setup();
        this.compartilhe();

    }, 

    setup : function () {

        //

    },

    compartilhe : function () {

        this.setShowAndHide();
        this.setClickBtn();
        APP.component.ShareThis.init();
        this.setClickCompartilhar();

    },

    setShowAndHide: function () {

        $('body header').hide();
        $('body').addClass("bg-body");

    },

    setClickBtn : function () {

        $('.lista-compartilhamento > a[data-share]').on('click', function (event) {
            event.preventDefault();

            var url = $(this).attr('data-share')

            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                setTimeout(function() {
                    window.open(url, '_system');
                }, 500);
            }

        });

    },

    setClickCompartilhar : function () {

        $('.compartilhar').on('click', function (event) {

            event.preventDefault();

            $('.lista-compartilhamento > div.active').each(function () {

                var network = $(this).find('span').text().toLowerCase();

            })

        });
    }

};
