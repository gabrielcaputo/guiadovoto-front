/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

APP.controller.Teste = {

    init : function () {

        this.setup();
        this.compartilhar();

    }, 

    setup : function () {

        //

    },

    compartilhar : function () {

        this.setClickCompartilhar();

    },

    setClickCompartilhar : function () {

        $("#compartilhar").on('click', function (event) {

            event.preventDefault();

            var element = $("#html-content-holder")[0];
            var config = {
                width: 600,
                height: 315
            }

            html2canvas(element,config).then(function(canvas) {
                $("#previewImage").append(canvas);

                var imgageData = canvas.toDataURL("image/png");
                $('.img-render').attr('src',imgageData)

                APP.controller.Teste.saveImgCompartilhar(imgageData);
        

            });

        });

    },

    saveImgCompartilhar : function (img) {

        var erro = "";

        var formdata = new FormData();
        formdata.append("base64image", img);
        
        $.ajax({
            type: "POST",
            data: formdata,
            dataType: 'json',
            processData: false,
            contentType: false,
            url: "/Compartilhar/SalvarImagem",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.Teste.setImgCompartilhar(result);
            },
            error: function (result) {
                APP.component.Loading.hide();
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        });

    },

    setImgCompartilhar : function (result) {

        var title = 'Guia do Voto';
        var desc = 'Que tal testar seus conhecimentos também?';
        var img = result.img;
        //var url = 'guiadovoto.org.br';
        var url = 'https://995bb74d.ngrok.io/';
        //var URLShare = encodeURI('http://guiadovoto.fullbarhomologa.com.br/Compartilhar?title=' + title + '&desc=' + desc + '&img=' + 'facebook-nota-cap-0'+capitulo+'-'+imgRange+'.png&url='+url+'');
        var URLShare = 'https://www.facebook.com/dialog/share?app_id=247591319322843&display=popup&href=' + encodeURIComponent('https://995bb74d.ngrok.io/Compartilhar?title=' + title + '&desc=' + desc + '&img=' + 'compartilhar-'+img+'.png&url='+url+'');
        window.open(URLShare, '_system');

    },

};