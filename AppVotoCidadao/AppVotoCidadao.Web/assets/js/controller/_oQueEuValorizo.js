/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

APP.controller.OQueEuValorizo  = {

    init: function () {

        this.setup(); 
        this.oQueEuValorizo(); 

    },

    setup : function () {

        //

    },

    /*
    |--------------------------------------------------------------------------
    | Default
    |--------------------------------------------------------------------------
    */
    oQueEuValorizo : function () {

        this.getStepStop();

    },

    /*
    |--------------------------------------------------------------------------
    | STEPs
    |--------------------------------------------------------------------------
    */

    getStepStop : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/OQueEuValorizo/GetStep',
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.OQueEuValorizo.verificaStepQueParou(result.step);
            },
            error: function (result) {
                
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function(result) {
            //Slick
            APP.controller.OQueEuValorizo.setSlickSteps(result.step);
            APP.controller.OQueEuValorizo.setNextSlickSteps(result.step);
        });

    },
    
    verificaStepQueParou : function (stepStop) {

        switch(stepStop) {
            case 0:
                APP.controller.OQueEuValorizo.setTutorial();
                break;
            case 1:
                APP.controller.OQueEuValorizo.getListaTemas();
                break;
            case 2:
                APP.controller.OQueEuValorizo.getListaPerguntas();
                break;
            case 3:
                APP.controller.OQueEuValorizo.getResultPrioridades();
                break
        }

    },

    setSlickSteps : function (initialSlick) {

        var configSteps = {
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            draggable: false,
            swipe: false,
            touchMove: false,
            fade: false,
            adaptiveHeight: true,
            initialSlide: initialSlick,
        }
        APP.component.Slick.init('carousel-steps', configSteps);

    },

    setNextSlickSteps : function (stepStop) {

        var step = 0;

        if (stepStop != undefined) {
            step = stepStop;
        } else {
            step = $('.carousel-steps').slick('slickCurrentSlide')+1;
        }

        switch(step) {
            case 1:
                APP.controller.OQueEuValorizo.setRulesSlickStep00(step);
                break;
            case 2:
                APP.controller.OQueEuValorizo.setRulesSlickStep01(step);
                break;
            case 3:
                APP.controller.OQueEuValorizo.setRulesSlickStep02(step);
                break;
        }
        APP.controller.OQueEuValorizo.setShowAndHide();


    },

    setShowAndHide : function () {

        var count = $('.carousel-steps').slick('getSlick').slideCount;
        var current = $('.carousel-steps').slick('slickCurrentSlide');
        $('.carousel-steps').slick('setPosition');

        switch(current) {
            case 0:
                $('a.comecar').addClass('locked');
                $('a.ok').removeClass('locked');
                $('a.confirmar').removeClass('locked');
                $('.actions-final').removeClass('locked');
                break;
            case 1:
                $('a.comecar').removeClass('locked');
                $('a.ok').addClass('locked');
                $('a.confirmar').removeClass('locked');
                $('.actions-final').removeClass('locked');
                break;
            case 2:
                $('a.comecar').removeClass('locked');
                $('a.ok').removeClass('locked');
                $('a.confirmar').addClass('locked');
                $('.actions-final').removeClass('locked');
                break;
            case 3:
                $('a.comecar').removeClass('locked');
                $('a.ok').removeClass('locked');
                $('a.confirmar').removeClass('locked');
                
                $('.actions-final').addClass('locked');
                $('a.refazer').addClass('locked');
                $('a.refazer').addClass('active');
                $('a.go-se-eu-fosse-um-politico').addClass('locked');
                $('a.go-se-eu-fosse-um-politico').addClass('active');
                break
        }

    },

    /*
    |--------------------------------------------------------------------------
    | TUTORIAL
    |--------------------------------------------------------------------------
    */

    setTutorial : function () {

        this.setSlickTutorial();
        this.verificaTutorial();
        this.setInfoTutorial();
        this.setClickComecar();

    },

    setSlickTutorial : function () {

        var configSteps = {
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            adaptiveHeight: false,
            //initialSlide: 1,
        }
        APP.component.Slick.init('lista-tutorial', configSteps);

    },

    verificaTutorial : function () {
    
        var leuTutorial = localStorage.getItem('LeuTutorialOQueEuValorizo');
        if (leuTutorial != 'true') {

            //APP.controller.SeEuFosseUmPolitico.setClickTutorialComecar();
        }
        
    },

    setInfoTutorial : function () {

        $('.steps').addClass('disable');
        $('a.comecar').addClass('active');

    },

    setClickComecar : function () {

        $('a.comecar').on('click', function (event) {

            event.preventDefault();

            APP.controller.OQueEuValorizo.sendTemasStep00();

        });

    },

    setRulesSlickStep00 : function (step) {

        $('.carousel-steps').slick('slickGoTo', step);
        var current = $('.carousel-steps').slick('slickCurrentSlide')+1;
        $('.steps').removeClass('disable');

    },

    /*
    |--------------------------------------------------------------------------
    | STEP 01
    |--------------------------------------------------------------------------
    */
    getListaTemas : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/OQueEuValorizo/ListaTemas',
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.OQueEuValorizo.setListaTemas(result.temas);
            },
            error: function (result) {
                
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {
            //Bind
            APP.controller.OQueEuValorizo.bindStep01();
            setTimeout(function(){ 
                $('.carousel-steps').slick('setPosition');
            }, 500);
        });

    },

    setListaTemas : function (temas) {
        
        $(temas).each(function (e) {
            var html = `
                <li>
                    <input data-id="${this.id}" id="tema-${this.id}" type="checkbox" value="${this.tema}">
                    <label for="tema-${this.id}">${this.tema}</label>
                </li>
            `;
    
            $('#step-01 .lista-temas').append(html);
        });

    },

    setClickListaTema : function () {

        $('.lista-temas li input').on('change', function (event) {

            var lista = $('.lista-temas').find('input:checked').length;
             if (lista == 5) {
                $(this).closest('li').toggleClass('active');
                $('a.ok').addClass('full');
                $('a.ok span.num').html(lista);

                $('.lista-temas li').each(function () {
                    var checked = $(this).find('input').is(':checked');
                    if (checked) {
                        $(this).addClass('checked');
                    } else {
                        $(this).addClass('blocked');
                    }
                })
    
            } else if (lista > 0) {
                $('.lista-temas li').removeClass('checked');
                $('.lista-temas li').removeClass('blocked');
                $(this).closest('li').toggleClass('active');
                $('a.ok').addClass('active');
                $('a.ok').removeClass('full');
                $('a.ok span.num').html(lista);

            } else {
                $(this).closest('li').toggleClass('active');
                $('a.ok').removeClass('active');
                $('a.ok span.num').html(lista);
            }


        });

    },

    setClickOkListaTema : function () {

        $('a.ok').on('click', function (event) {

            event.preventDefault();

            var lista = [];
            var values = [];
            var ids = [];

            $('.lista-temas').find('input:checked').each(function () {

                lista.push($(this).val());
                values.push(APP.component.RemoveAcentos.init($(this).val().toLowerCase()));
                ids.push($(this).data('id'));

            });

            APP.component.Alert.oQueEuValorizoTemas(lista, values, ids);

        });

    },

    setClickAvancarTemas : function () {

        $('#oqueeuvalorizo-temas .avancar').on('click', function (event) {

            event.preventDefault();
            var ids = [];

            $('.lista-esolhida li').each(function () {
                ids.push(parseInt($(this).data('id')));
            })

            APP.controller.OQueEuValorizo.sendTemasStep01(ids);

        });

    },

    setClickRever : function () {

        $('.rever').on('click', function (event) {

            event.preventDefault();
            if ($('.modal:visible').length > 1) {
                $('.modal#customAlert, #mask').fadeOut();                
            } else {
                $('.modal, #mask').fadeOut();
            }

        });

    },

    setRulesSlickStep01 : function (step) {

        $('.carousel-steps').slick('slickGoTo', step);
        var current = $('.carousel-steps').slick('slickCurrentSlide');
        $('.modal, #mask').fadeOut();
        $('ul.steps li').removeClass('active');
        $('ul.steps li:nth-of-type('+current+')').addClass('active');
        
    },

    bindStep01 : function () {

        APP.controller.OQueEuValorizo.setClickListaTema();
        APP.controller.OQueEuValorizo.setClickOkListaTema();
        APP.controller.OQueEuValorizo.setClickAvancarTemas();
        APP.controller.OQueEuValorizo.setClickRever();
        APP.controller.OQueEuValorizo.setShowAndHide();

    },

    /*
    |--------------------------------------------------------------------------
    | STEP 02
    |--------------------------------------------------------------------------
    */

    getListaPerguntas : function () {

        var iniciaPergunta = 0;

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: "/OQueEuValorizo/BuscarPerguntas",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                iniciaPergunta = result.perguntaStop;
                APP.controller.OQueEuValorizo.setOrdenaTemas(result.perguntas);
            },
            error: function (result) {

            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {
            APP.controller.OQueEuValorizo.bindStep02(iniciaPergunta);
            setTimeout(function(){ 
                $('.carousel-steps').slick('setPosition');
            }, 500);
        });

    },

    setOrdenaTemas : function (perguntas) {

        $.each(perguntas, function(i, val) {
            var html = `
                <div class="item">
                    <ul class="pergunta" data-pergunta="${val.id}">
                        ${val.respostas.map( e =>
                            `<li>
                                <input id="p_${val.id}_o_${e.id}" type="radio" name="p_${val.id}" value="${e.value}">
                                <label for="p_${val.id}_o_${e.id}" id="${e.id}">${e.tema}</label>
                            </li>`
                        ).join('')}
                    </ul>
                </div>
            `;

            $('#step-02 .ordena-temas').append(html);

        });        

    },

    setClickOrdenaTema : function () {

        $('.ordena-temas .pergunta li input').on('change', function (event) {

            $(this).closest('.pergunta').find('li').removeClass('active');
            $(this).closest('li').toggleClass('active');
            $('.confirmar').addClass('active');

        });

    },

    setSlickOrdenaTemas : function (iniciaPergunta) {

        var inicioSlick = 0;

        iniciaPergunta != undefined ? inicioSlick = iniciaPergunta : '';

        var configSteps = {
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            draggable: false,
            swipe: false,
            touchMove: false,
            fade: false,
            //adaptiveHeight: true,
            initialSlide: inicioSlick,
        }
        APP.component.Slick.init('ordena-temas', configSteps);

    },

    setClickConfirmar : function () {
        $('.confirmar').on('click', function (event) {

            event.preventDefault();

            var current = $('.ordena-temas').slick('slickCurrentSlide') + 1;
            var id = $('.ordena-temas .item:nth-of-type(' + current + ') .pergunta li input:checked ~ label').attr('id');
            APP.controller.OQueEuValorizo.sendTemasStep02(id);

        });
    },

    setNextSlickOrdemStep02 : function () {

        $('.ordena-temas').slick('slickNext');
        $('.confirmar').removeClass('active');
        
        var count = $('.ordena-temas').slick('getSlick').slideCount;
        var current = $('.ordena-temas').slick('slickCurrentSlide')+1;

        if (count == current) {
            $('.confirmar').text('Ver Resultado');
        }


    },

    setRulesSlickStep02 : function (step) {

        $('.carousel-steps').slick('slickGoTo', step);
        var current = $('.carousel-steps').slick('slickCurrentSlide');
        $('ul.steps li').removeClass('active');
        $('ul.steps li:nth-of-type('+current+')').addClass('active');

    },

    bindStep02 : function (iniciaPergunta) {

        APP.controller.OQueEuValorizo.setClickOrdenaTema();
        APP.controller.OQueEuValorizo.setClickConfirmar();
        APP.controller.OQueEuValorizo.setSlickOrdenaTemas(iniciaPergunta);
        APP.controller.OQueEuValorizo.setShowAndHide();

    },

    /*
    |--------------------------------------------------------------------------
    | STEP 03
    |--------------------------------------------------------------------------
    */
    getResultPrioridades : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/OQueEuValorizo/ListaPrioridades',
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.OQueEuValorizo.setResultPrioridades(result);
                APP.controller.OQueEuValorizo.verificaDeOndeVeio(result);
            },
            error: function (result) {
                
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {
            APP.controller.OQueEuValorizo.bindStep03();
            APP.component.LocalStorage.setPorcentagemMinhasPrioridades('oqueeuvalorizo');
            setTimeout(function () {
                $('.carousel-steps').slick('setPosition');
            }, 500);
        });
    },

    setResultPrioridades: function (result) {

        $(result.resultado).each(function (e) {

            var text = APP.controller.OQueEuValorizo.setChangePorcentToText(this.porcentagem);
            var css = APP.controller.OQueEuValorizo.setGradientColor(this.porcentagem);
            
            var html = `
                <li>
                    <span class="num" style="background-color: #${css}">${e+1}</span>
                    <span class="tema">${this.tema}</span>
                    <span class="porcentagem" style="color: #${css}">${text}</span>
                </li>
            `;
    
            $('#step-03 .resultado-temas').append(html);

        });

    },

    setChangePorcentToText : function (porcent) {

        var texto = '';
        switch(porcent) {
            case 100:
                texto = 'Muito alta';
                break;
            case 75:
                texto = 'Alta';
                break;
            case 50:
                texto = 'Média';
                break;
            case 25:
                texto = 'Baixa';
                break;
            case 0:
                texto = 'Muito baixa';
                break;
        }

        return texto;

    },
    
    verificaDeOndeVeio : function () {

        var came = APP.component.Utils.getUrlParameter('came');
        var cargo = APP.component.Utils.getUrlParameter('cargo');

        if (came != "undefined" && came == "AvaliarSelecionados") {
            $('.actions-final').find('.go-se-eu-fosse-um-politico').attr('href', '/MeusCandidatos/'+came+'/'+cargo);
            $('.actions-final').find('.go-se-eu-fosse-um-politico span:nth-of-type(2)').text('AVALIAR MINHAS OPÇÕES');
        }

    },

    setGradientColor : function (number) {

        var color1 = '0000BF';
        var color2 = '06E3FF';
        var ratio = 0;

        if (number < 10) {
            ratio = parseFloat('0.0');
        } else {
            ratio = parseFloat('0.'+number);
        }

        var hex = function(x) {
            x = x.toString(16);
            return (x.length == 1) ? '0' + x : x;
        };

        var r = Math.ceil(parseInt(color1.substring(0,2), 16) * ratio + parseInt(color2.substring(0,2), 16) * (1-ratio));
        var g = Math.ceil(parseInt(color1.substring(2,4), 16) * ratio + parseInt(color2.substring(2,4), 16) * (1-ratio));
        var b = Math.ceil(parseInt(color1.substring(4,6), 16) * ratio + parseInt(color2.substring(4,6), 16) * (1-ratio));


        if (number != 100) {
            var cor = hex(r) + hex(g) + hex(b);
        }

        return cor;

    },

    setClickRefazerTemas : function () {

        $('a.refazer').on('click', function (event) {

            event.preventDefault();

            var title = 'Atenção';
            var msg = 'Ao redefinir os temas mais importantes para você, os resultados de algumas atividades serão apagados (exceto os candidatos já enviados para a seção <strong>"Meu Voto"</strong>).';

            var htmlForm = `
                <a href="#" class="btn continuarRefazerTemas">
                    <span>CONTINUAR</span>
                </a>
                <a href="#" class="btn close">
                    <span>CANCELAR</span>
                </a>
            `;

            APP.component.Alert.customGuiaDoVoto(title, msg, htmlForm, false, false, '/assets/img/avaliar-selecionados/bg-alerta-alterartemas.png');

            $('body').off('click', '.continuarRefazerTemas');
            $('body').on('click', '.continuarRefazerTemas', function(event) {
                event.preventDefault();
                APP.controller.OQueEuValorizo.sendRefazerTemas();
            });

        });

    },

    bindStep03 : function () {

        APP.controller.OQueEuValorizo.setClickRefazerTemas();
        APP.controller.OQueEuValorizo.setShowAndHide();
        APP.component.Compartilhar.init();

    },

    /*
    |--------------------------------------------------------------------------
    | SEND
    |--------------------------------------------------------------------------
    */

    sendTemasStep00 : function () {

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/OQueEuValorizo/Iniciar",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.OQueEuValorizo.setNextSlickSteps(1);
                APP.controller.OQueEuValorizo.getListaTemas();
            },
            error: function (result) {

            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {

        });

    },

    sendTemasStep01: function (temas) {

        $.ajax({
            type: "POST",
            data: { temas } ,
            dataType: 'json',
            url: "/OQueEuValorizo/SalvarStep01",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.OQueEuValorizo.setNextSlickSteps(2);
                APP.controller.OQueEuValorizo.setOrdenaTemas(result.perguntas);
            },
            error: function (result) {

            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function() {
            APP.controller.OQueEuValorizo.bindStep02();
            setTimeout(function(){ 
                $('.carousel-steps').slick('setPosition');
            }, 500);
        });

    },

    sendTemasStep02 : function (resposta) {

        $.ajax({
            type: "POST",
            data: { resposta },
            dataType: 'json',
            url: "/OQueEuValorizo/SalvarStep02",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {

                var count = $('.ordena-temas').slick('getSlick').slideCount;
                var current = $('.ordena-temas').slick('slickCurrentSlide')+1;

                if (count == current) {
                    APP.controller.OQueEuValorizo.setNextSlickSteps(3);
                    APP.controller.OQueEuValorizo.getResultPrioridades();
                } else {
                    APP.controller.OQueEuValorizo.setNextSlickOrdemStep02();
                }
            },
            error: function (result) {

            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        });
    },

    sendRefazerTemas : function (_url) {

        var url ='/OQueEuValorizo';
        
        if (_url != '' && _url != undefined) {
            url = _url;
        }

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/OQueEuValorizo/Refazer",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.component.LocalStorage.removePorcentagemMinhasPrioridades('oqueeuvalorizo');
            },
            error: function (result) {

            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function(result) {
            setTimeout(function(){ 
                window.location.href = url;
            }, 10);
        });
    }
};

