/*
|--------------------------------------------------------------------------
| Controller Inicio
|--------------------------------------------------------------------------
*/

APP.controller.Inicio = {

    init: function () {

        this.setup();
        this.inicio();

    }, 

    setup : function () {

        //

    },

    inicio : function () {

        this.setShowAndHide();

    },

    setShowAndHide : function () {

        $('body header').hide();

    },

};