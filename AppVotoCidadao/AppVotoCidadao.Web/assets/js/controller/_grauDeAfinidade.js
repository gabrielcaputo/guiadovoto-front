/*
|--------------------------------------------------------------------------
| Controller 
|--------------------------------------------------------------------------
*/
var sectionAfinidade = $('section#grau-de-afinidade');
APP.controller.GrauDeAfinidade = {

    init: function () {
        this.changeCandidatoSelecionado();
        this.clickButtonVerMeuVoto();
        this.clicksCandidato();
        this.explicacaoCalculoAfinidade();
        this.buscaResultadoAfinidade();
        this.buscaResultadoGovernador();
    },

    changeCandidatoSelecionado : function () {
        sectionAfinidade.on('change', '.item input', function(event) {
            sectionAfinidade.find('button#verMeuVoto').removeAttr('disabled')
        });
    },

    buscaResultadoGovernador : function () {

        const req = fetch('/SegundoTurno/BuscarCandidatos?cargo=governador');

        req.then((response) => {
            return response.json();
        }).then((response) =>{
        
            if (response.erro) {
                $(".open").attr('disabled', true);
                $(".open").addClass('inactive');
            }
        });

    },

    buscarInfosCandidato: function (id, _afinidade, _selecionado, _cargo) {
        var erro = "";
        $.ajax({
            type: "GET",
            data: { id } ,
            dataType: 'json',
            url: "/MeusCandidatos/DetalhesCandidato/",
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.Candidatos2018.preencheDetalheCandidato(result.candidato, _afinidade, _selecionado, _cargo);
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });
    },

    preencheDetalheCandidato : function (result, afinidade) {
        
        const candidato = result.candidato;

               
        if (candidato.cargo != undefined) {
            var cargo = candidato.cargo;
        } else {
            var cargo = $('div.selecionarCargo a.open').text();
        }

        var candidatoAfinidade = $('div#candidatoAfinidade');
        var classe = candidato.status.toLowerCase();
        
        candidatoAfinidade.html('');
        
        candidatoAfinidade.append(`
            <a href="#" class="close"></a>
            <div class="header">
                <div class="title">
                    <a href="#" class="voltar">
                        <i class="fas fa-arrow-left"></i>
                    </a>
                    Dados do Candidato
                </div>
                <div class="infos">
                    <div class="title">Candidato a ${cargo}</div>
                    <div class="nome">${candidato.nome.toLowerCase()}</div>
                </div>
                <div class="afinidade">
                <div class="title">Afinidade</div>
                    <div class="${afinidade == 'naoavaliado' ? 'nao-avaliado' : 'porcentagem'}">${afinidade == 'naoavaliado' ? 'CANDIDATO NÃO AVALIADO' : afinidade}</div>
                </div>
            </div>
            <div class="photo ${candidato.selecionado && window.location.href.indexOf('/MeuVoto') == -1 ? "active" : ""}"><span style="background-image: url(${candidato.foto})"></span></div>
            <div class="item">
                <a href="#" class="info" data-title="" data-info="coligacoes" data-texto='Os dados disponibilizados na plataforma GUIA DO VOTO, incluindo os status das candidaturas para todos os cargos, são atualizados a cada <strong>48 horas</strong> a partir de informações oficiais do <strong>TSE</strong>.' data-img="">
                    <img src="/assets/img/meucandidatoideal/icon-info.svg">
                </a>
                <div class="nome">${candidato.nome.toLowerCase()}</div>
                <div class="status">
                    <div class="title">Status</div>
                    <div class="txt ${camelCase(classe)}">${candidato.status}</div>
                </div>
            </div>
            <div class="item">
                <div class="partido">
                    <div class="sigla">${candidato.sigla}</div>
                    <div class="txt">${candidato.partido}</div>
                </div>
                <div class="numero">${candidato.numero}</div>
            </div>
            ${(candidato.subCargos.length > 0 ?
                `<div class="item subcargos">
                    <div class="title">${cargo.toLowerCase().indexOf('senador') >= 0 ? "Suplente" : "Vice"}</div>
                    ${candidato.subCargos.map( l =>
                        `<div class="subcargo">
                            ${l.nome.toLowerCase()} (${l.sigla})
                        </div>`                    
                    ).join('')}
                </div>` : ''
            )}
            <div class="item">
                <div class="vice">
                    <div class="title">Coligação</div>
                    <div class="txt">
                        ${candidato.listaColigacao}
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="nomeCompleto">
                    <div class="title">Nome Completo</div>
                    <div class="txt">
                        ${candidato.nomeCompleto.toLowerCase()}
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="idade">
                    <div class="title">Idade</div>
                    <div class="num">${candidato.idade}</div>
                    <div class="txt">anos</div>
                </div>
                <div class="sexo">
                    <div class="title">Sexo</div>
                    <div class="icon">
                        <img src="/assets/img/meucandidatoideal/icon-${candidato.sexo == 'Masculino' ? 'mas' : 'fem'}.png" alt="">
                    </div>
                    <div class="txt">${candidato.sexo}</div>
                </div>
            </div>
            <div class="item">
                <div class="grau">
                    <div class="title">Grau/Instrução</div>
                    <div class="txt">${candidato.grau.toLowerCase()}</div>
                </div>
            </div>
            <div class="item">
                <div class="estadocivil">
                    <div class="title">Estado Civil</div>
                    <div class="txt">${candidato.estadoCivil}</div>
                </div>
                <div class="corRaca">
                    <div class="title">Cor/Raça</div>
                    <div class="txt">${candidato.cor}</div>
                </div>
            </div>
            <div class="item">
                <div class="ocupacao">
                    <div class="title">Ocupação</div>
                    <div class="txt">${candidato.ocupacao.toLowerCase()}</div>
                </div>
            </div>
            ${(candidato.valorMaximo != "-1" ? `
                <div class="item">
                    <div class="ocupacao">
                        <div class="title">Valor máximo da campanha</div>
                        <div class="txt">R$ ${candidato.valorMaximo}</div>
                    </div>
                </div>` : ``
            )}
            ${(candidato.sites.length > 0 ? `
                <div class="conhecerMais">
                    <p>Quer conhecer mais sobre este(a) candidato(a)? Acesse:</p>
                    ${candidato.sites.map( l =>
                        `<a href="${l.link}" target="_system">${l.nome}</a>`
                    ).join('')}
                    <small>Os dados apresentados em outras plataformas on-line na~o sa~o de responsabilidade do Guia do Voto.</small>
                </div>` : ''
            )}

            ${candidato.selecionado != undefined ? 
                `<input ${candidato.selecionado ? "checked" : ""} id="candidato_${candidato.id}_in" data-id="${candidato.id}"  data-afinidade="${afinidade}" type="checkbox" />
                 
                    ${window.location.href.indexOf('/MeuVoto') != -1 || window.location.href.indexOf('/MeusCandidatos/AvaliarSelecionados') !== -1  ?  '' : `<label for="candidato_${candidato.id}_in"></label>`}
                    <a href="#" class="verMeuVoto">Ver meu voto</a>
                `
            : 
                ``
            }
          
        `)

        

    },

    buscaResultadoAfinidade : function (result) {
        $.ajax({
            async: false,
            url: '/SegundoTurno/BuscarCandidatos/?cargo=' + cargo,
            type: 'GET',
        })
        .done(function(response) {
            APP.controller.GrauDeAfinidade.preencheResultadoAfinidade(response);
        })
        .fail(function(response) {
            
        })
    },

    preencheResultadoAfinidade : function (_data) {

        if (_data.candidatos.length < 3) {
            sectionAfinidade.find('.resultados .items').addClass('center');
        }       
        sectionAfinidade.find('.resultados .items').append(`          

            ${_data.candidatos.map( a =>
                
                `<div class="item" data-id="${a.id}" data-afinidade="${a.avaliado ? a.afinidade : 'naoavaliado'}">
                    ${a.escolhido && !a.selecionado ?
                        ``
                        : `<input ${a.selecionado ? 'checked' : ''} name="candidato_${cargo}" id="candidato_${a.id}" type="radio">`                        
                    }
                    <div class="infos">
                        <div class="photo"><span style="background-image: url(${a.foto})"></span></div>
                        <div class="candidato">
                            <div class="nome">
                                ${a.nome.toLowerCase()} - 
                                <span class="numero">${a.numero}</span>
                            </div>
                            <div class="partido">
                                <span class="sigla">${a.sigla}</span>
                                (${a.partido})
                            </div>
                        </div>                  
                    </div>
                    ${a.escolhido && !a.selecionado ?
                        `<span class="escolhido">Candidato já escolhido</span>`
                        : `<label for="candidato_${a.id}"></label>`
                    }

                    <div class="afinidade">
                        <div class="title">Afinidade</div>
                        ${!a.avaliado ? `<span class="nao-avaliado">Candidato não avaliado</span>` :`<span class="avaliado">${a.afinidade + '%'}</span>`}
                    </div>
                </div>`                    
            ).join('')}
        `)
        if (sectionAfinidade.find('.items input:checked').length > 0) {
            sectionAfinidade.find('button#verMeuVoto').attr('disabled', false);
        }
    },

    clickButtonVerMeuVoto : function () {
        sectionAfinidade.on('click', 'button#verMeuVoto', function(event) {
            APP.controller.MeusCandidatos.saveCargoEtapa(cargo, 4, '/MeuVoto/');
        });
        $('#candidatoAfinidade').on('click', '.verMeuVoto', function(event) {
            event.preventDefault();
            APP.controller.MeusCandidatos.saveCargoEtapa(cargo, 4, '/MeuVoto/');
        });
    },

    clicksCandidato : function () {
        
        $('body').on('click', '.resultados .item .infos', function(event) {
            $('#candidatoAfinidade').scrollTop(0);
            $('body').addClass('lockScroll');
            $('#candidatoAfinidade').addClass('active');
            sectionAfinidade.addClass('pushLeft');

            var idCandidato = $(this).parent().data('id');
            var afinidade = $(this).parent().data('afinidade');
            var selecionado = $(this).parent().find('input').is(':checked');

            APP.controller.GrauDeAfinidade.buscarInfosCandidato(idCandidato, afinidade, selecionado);
            ;            
        });

        $('body').on('click', '.resultados .item label', function(event) {
            if ($(this).parent().find('input').is(':checked')) {
                event.preventDefault();
                $(this).parent().find('input').prop('checked', false);
                sectionAfinidade.find('button#verMeuVoto').attr('disabled', true);
                APP.controller.GrauDeAfinidade.removerVoto(cargo);
            } else {
                APP.controller.GrauDeAfinidade.salvarVoto(cargo, $(this).parent().data('id'), $(this).parent().data('afinidade'));
            }
        });

        $('body').on('click', '#candidatoAfinidade .voltar, #candidatoAfinidade .close', function(event) {
            event.preventDefault();
            $('body').removeClass('lockScroll');
            $('#candidatoAfinidade').removeClass('active');
            sectionAfinidade.removeClass('pushLeft');
        });

        $('body').on('change', '#candidatoAfinidade input', function(event) {
            var id = $(this).data('id')
            var checked = $(this).is(':checked');

            if (checked) {
                sectionAfinidade.find('.items #candidato_'+id).prop('checked', checked);
                sectionAfinidade.find('button#verMeuVoto').removeAttr('disabled');
                APP.controller.GrauDeAfinidade.salvarVoto(cargo, $(this).data('id'), $(this).data('afinidade'));
                $('#candidatoAfinidade div.photo').addClass('active');
            } else {
                sectionAfinidade.find('.items input').prop('checked', checked);
                sectionAfinidade.find('button#verMeuVoto').attr('disabled', true);
                APP.controller.GrauDeAfinidade.removerVoto(cargo);
                $('#candidatoAfinidade div.photo').removeClass('active');
            }
        });

        // $('body').on('click', '.info-ciente', function(event) {
        //     event.preventDefault();
        //     $('#explicacaoCalculoAfinidade').addClass('active');
        // });

    },

    salvarVoto : function (_cargo, _id, _porcentagem) {
        var data = {
            'cargo': _cargo,
            'idCandidato' : _id,
            'porcentagem' : _porcentagem
        }
        $.ajax({
            url: '/SegundoTurno/SalvarVoto/',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                // $('body').addClass('lockScroll');
                APP.component.Loading.show();
            }
        })
        .done(function(result) {
            
        })
        .always(function() {
            APP.component.Loading.hide();
        })
        .fail(function(response) {
        })
    },

    removerVoto : function (_cargo) {
        var data = {
            'cargo': _cargo,
        }
        $.ajax({
            url: '/SegundoTurno/RemoverVoto/',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                // $('body').addClass('lockScroll');
                APP.component.Loading.show();
            }
        })
        .done(function(response) {
        })
        .always(function() {
            APP.component.Loading.hide();
        })
        .fail(function(response) {
        })
    },

    explicacaoCalculoAfinidade : function () {
        // if (localStorage.getItem('LeuExplicacaoCalculoAfinidade') === null) {
            // $('#explicacaoCalculoAfinidade').addClass('active');            
        // }

        // $('body').on('click', '#explicacaoCalculoAfinidade .ciente', function(event) {
        //     event.preventDefault();
        //     $('#explicacaoCalculoAfinidade').removeClass('active');
        //     localStorage.setItem('LeuExplicacaoCalculoAfinidade', true);
        // });

        $('body').on('click', '#inforamcoes .termos', function(event) {
            event.preventDefault();
            $('.modal-termos').fadeIn();
        });
    },
};