/*
|--------------------------------------------------------------------------
| Controller Sobre
|--------------------------------------------------------------------------
*/

APP.controller.Sobre = {

    init : function () {

        this.setup();
        this.sobre();

    }, 

    setup : function () {

        //

    },

    sobre : function () { 

        this.setSlickSobre();

    },

    setSlickSobre: function () {
        
        var configPersonagens = {
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            arrows: true,
            slidesToScroll: 1,
            fade: true,
            //centerMode: true,
            //prevArrow: "<a class='arrow prev' href='javascript:void(0)'><i class='fas fa-angle-left fa-lg'></i>TESTE ANTERIOR</a>",
            //nextArrow: "<a class='arrow next' href='javascript:void(0)'>PR�XIMO TESTE<i class='fas fa-angle-right fa-lg'></i></a>",
        }
        APP.component.Slick.init('personagens', configPersonagens);
    }

};
