/*
|--------------------------------------------------------------------------
| Controller 
|--------------------------------------------------------------------------
*/

var c2018 = $('section#candidatos-2018-f3');
var quantidadeResultadoPagina = 20;
var filtros = {
    eleito: '',
    coligacoes: {}
};
var coligacoesSearch = [];

APP.controller.Candidatos2018F3 = {

    init: function () {

        //Busca
        this.buscarEvents();

        this.scrollResultados();
        this.clicksBusca();
        this.clicksMaisFiltros();
        this.clicksCandidato();
        this.clicksCandidatoSelecionado();
        this.clickScrollTop();
        this.setClickAvaliar();

        this.clicksLimiteCandidatos();

        this.buscarCandidatos(cargoF3, 0);
        this.buscarColigacoes();
        this.scrollableHeaders();
    },

    verificaCargoPreenchido : function (cargoF3) {

        var preenchido;

        $.ajax({
            async: false,
            url: '/MeusCandidatos/ParametroPreenchido/?cargo=' + cargoF3,
            type: 'GET',
        })
        .done(function(response) {
            preenchido = response.preenchido;
        })
        .fail(function(response) {
            return false;
        })

        return preenchido;
    },

    buscarCandidatos: function (cargoF3, index, search) {

        if (index == undefined) {
            var index = Math.ceil($('.resultados .item').length / quantidadeResultadoPagina);
        }

        if (search == undefined) {
            search = '';
        }

        //Get Filtros
        filtros = {};
        var f = $('#maisFiltros');
        var arrayColigacoes = [];
        coligacoesSearch = [];
        f.find('.coligacoes input[type=checkbox]:checked').each(function(index, el) {
            arrayColigacoes.push($(this).next().text());
            coligacoesSearch.push($(this).attr('id'));
        });
        filtros.eleito = f.find('[name=candidatoEleito]:checked').attr('id');
        filtros.coligacoes = arrayColigacoes;


        //Chamada
        var data = {
            'cargo': cargoF3,
            'quantidade': quantidadeResultadoPagina,
            'index': index,
            'search': search,
            'filtros': filtros
        }
        var preenchido = APP.controller.Candidatos2018F3.verificaCargoPreenchido(cargoF3);

        if (c2018.find('.items .loading-resultados').length === 0) {
            $.ajax({
                url: '/Candidatos2018/BuscarCandidatos/',
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    // $('body').addClass('lockScroll');
                    c2018.find('.items').append('<div class="loading-resultados"></div>');
                    APP.component.Loading.show();
                }
            })
            .done(function(response) {
                if (index === 0) {
                    APP.controller.Candidatos2018F3.limiteCandidatos(cargoF3, response.total, preenchido)
                }
                APP.controller.Candidatos2018F3.preencheResultadosBusca(response, index);
                APP.controller.Candidatos2018F3.preencheSelecionados(response.candidatosSelecionados);
            })
            .always(function() {
                if (!$('#tutorial-candidatos-2018').is(':visible')){
                    $('body').removeClass('lockScroll');
                    c2018.find('.items .loading-resultados').remove();
                }
                APP.component.Loading.hide();
            })
            .fail(function(response) {
            })
        }
    },

    salvarCandidato : function (_cargo, _id, _selecionado) {
        var data = {
            'cargo': _cargo,
            'id': _id,
            'selecionado': _selecionado,
        }
        $.ajax({
            url: '/Candidatos2018/SalvarCandidato/',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                // $('body').addClass('lockScroll');
                APP.component.Loading.show();
            }
        })
        .done(function(response) {
        })
        .always(function() {
            APP.component.Loading.hide();
        })
        .fail(function(response) {
        }) 
    },

    buscarColigacoes : function () {

        var erro = "";
       
        $.ajax({
            type: "GET",
            data: { cargoF3 },
            url: "/Candidatos2018/BuscarColigacoes",
            beforeSend: function () {
                
            },
            success: function (result) {
                APP.controller.Candidatos2018F3.setInfoColigacoes(result.coligacoes);
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                
            }
        });

    },

    setInfoColigacoes: function (result) {
        var coligacoes = $('div#maisFiltros .coligacoes');
        var texto = 'A partir de 2020 as coligações partidárias em eleições proporcionais não serão permitidas.';
        var img = 'bg-info-coligacoes.png';
        var title = 'Você sabia?';

        coligacoes.append(`
            <h1>
                Coligações partidárias
                <a href="#" class="info" data-title="${title}" data-info="coligacoes" data-texto='${texto}' data-img="/assets/img/meuscandidatos/informacoes/${img}">
                    <img src="/assets/img/meucandidatoideal/icon-info.svg">
                </a>
            </h1>
            <p>Selecione as coligações que contenham partidos nos quais você votaria. Apenas os candidatos das coligações selecionadas farão parte da sua lista de opções.</p>

            ${result.map( c =>
                `
                    <input id="${slugify(c)}" type="checkbox">
                    <label for="${slugify(c)}">${c}</label>
                `
            ).join('')}
         `)
    },

    limiteCandidatos : function (cargoF3, total, preenchido) {

        var maxCandidatos = 100;

        $('#limiteCandidatos .wrap').html('');

        var preencheuFase2 = typeof localStorage.porcentagemMP != "undefined" && typeof JSON.parse(localStorage.porcentagemMP)[user] == "object" && JSON.parse(localStorage.porcentagemMP)[user].length === 3;

        if (!preencheuFase2) {
            var title = `Vamos facilitar sua jornada?`;
            var text = `É importante concluir todas as etapas da área <strong>“Minhas prioridades”</strong> antes de prosseguir. É lá que você começa a fazer as escolhas que te levam a um voto consciente.`
        } else if (preencheuFase2 && total > maxCandidatos) {
            var title = `Ops! Ainda são muitos candidatos.`;
            var text = `Para facilitar sua navegação, que tal ajustar mais ainda as características definidas em <strong>Meu candidato ideal</strong> ou aplicar novos filtros?<br> Ao prosseguir com essa quantidade de candidatos, a navegação pode ser mais demorada.`
        } else if (preencheuFase2 && total === 0 && !$('.search').hasClass('active')) {
            var title = `Desculpe, não há candidatos com o perfil desejado.`;
            var text = `Ajuste as opções definidas em <strong>“Meu candidato ideal”</strong> ou o número de partidos de sua preferência para encontrar candidatos.`;
        }

        $('#limiteCandidatos .wrap').html(`
            <div class="encontrados">
                <span class="num">${total}</span> candidatos encontrados
            </div>
            <h1>${title}</h1>
            <p>
                ${text}
            </p>
            ${preencheuFase2 ?
                `<a href="/MeuCandidatoIdeal/?cargo=${cargoF3}" class="btn preferencias">Ajustar características</a>` :
                `<a href="/Home?MinhasPrioridades" class="btn preferencias">Ir para Minhas Prioridades</a>`
            }
            ${preencheuFase2 && total > maxCandidatos ?
                `<a href="#" class="btn maisFiltros">Aplicar mais filtros</a>` : ``
            }
            <a href="#" class="btn continue">Continuar</a>

        `);

        if (!preencheuFase2 ||
            preencheuFase2 && total > maxCandidatos ||
            preencheuFase2 && total === 0 && !$('.search').hasClass('active'))
        {
            $('#limiteCandidatos').addClass('active');
        }       
    },

    preencheResultadosBusca : function (items, index) {

        

        var r = $('section#candidatos-2018-f3').find('.resultados');
        var total = items.total;

        if (total === 0) {
            $('div.comResultado').hide()
            $('div.semResultado').show()
        } else {
            $('div.comResultado').show()
            $('div.semResultado').hide()
            if (index === 0) {
                $('html').scrollTop(0);
                r.find('div.ultimaAtualizacao').html(`
                    Última atualização: <strong>${APP.component.Date.formatDate(items.dataAtualizacao)}</strong>
                `)
                r.find('div.total').html(`
                    Total de <strong>${total}</strong> candidato${(total > 1 ? 's' : '')}
                `)
                r.find('div.items').html('');
            }
            r.find('div.items').append(`${items.candidatos.map( c =>
                `<div class="item" data-candidato="${c.id}">
                    <input ${c.selecionado ? 'checked' : ''} id="candidato_${c.id}" type="checkbox">
                    <label for="candidato_${c.id}"></label>
                    <div class="photo"><span style="background-image: url(${c.foto})"></span></div>
                    <div class="infos">
                        <div class="nome-numero">
                            <div class="nome">${c.nome.toLowerCase()}</div>
                            <div class="numero">${c.numero}</div>
                        </div>
                        <div class="partido">
                            <div class="sigla">${c.sigla}</div>
                            (${c.partido})
                        </div>
                    </div>
                </div>`
            ).join('')}`)
        }
    },

    preencheSelecionados : function (selecionados) {
        $('.candidatosSelecionados div.items').html('');
        if (selecionados != null) {
            $.each(selecionados, function(index, val) {
                $('.candidatosSelecionados div.items').append(`<a href="#" data-candidato="${val.id}" class="item">${val.nome.toLowerCase()}</a>`)
            });
        }
        APP.controller.Candidatos2018F3.checkMaxCandidatosSelecionados();
    },

    scrollResultados : function () {
        $(window).scroll(function(event) {

            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100 &&
                $('.resultados .item').length < parseInt($('.total strong').html())) {

                    if ($('.search').hasClass('active')) {
                        APP.controller.Candidatos2018F3.buscarCandidatos(cargoF3, undefined, $('.busca-event').val());
                    } else {
                        APP.controller.Candidatos2018F3.buscarCandidatos(cargoF3);
                    }
            }

            if ($(window).scrollTop() > 0) {
                c2018.find('a.scrollTop').addClass('active');
            } else {
                c2018.find('a.scrollTop').removeClass('active');
            }
        });
    },

    scrollableHeaders : function () {
        $('body > header').addClass('scrollable');

        var lastScrollTop = 0;
        $(window).on('scroll', function() {
            st = $(this).scrollTop();

            // Candidatos Selecionados
            var cS = $('.candidatosSelecionados');
            var cSWrapTop = $('.candidatosSelecionadosWrap').offset().top
            var cSWrapHeight = $('.candidatosSelecionadosWrap').outerHeight();

            if (st > cSWrapTop + cSWrapHeight) {
                if (st > $('.candidatosSelecionadosWrap').offset().top - 62) {
                    cS.addClass('scrollable');
                    cS.css('transform', 'translateY(-'+cSWrapHeight+'px)');
                } else {
                    cS.css('transform', 'translateY(-'+60+'px)');
                    cS.removeClass('scrollable');
                }
                cS.addClass('fixed');
                cS.css('top', (- cSWrapHeight) + 'px');

                if(st < lastScrollTop) {
                    cS.addClass('scroll');
                }
                else if ((st - lastScrollTop) < 40) {
                    cS.removeClass('scroll');
                }
            } else if (st <= $('.candidatosSelecionadosWrap').offset().top - 62) {
                cS.css('top', 60 + 'px');
                cS.css('transform', 'translateY(-'+60+'px)');
                cS.removeClass('scrollable fixed scroll');
            }

            lastScrollTop = st;
        });
    },

    // BUSCA
    clicksBusca : function () {
        var search = c2018.find('div.filtros div.search');
        search.on('click', '.open', function(event) {
            event.preventDefault();
            search.addClass('active');
            
        });

        search.on('click', '.close', function(event) {
            event.preventDefault();
            search.removeClass('active');
            search.find('input').val('');

            APP.controller.Candidatos2018F3.buscarCandidatos(cargoF3, 0);
        });
    },

    buscarEvents : function () {
        $(".busca-event").bindDelay('keyup', function() {
            APP.controller.Candidatos2018F3.buscarCandidatos(cargoF3, 0, $('.busca-event').val());
        }, 1500);

        $(".busca-event").on('keyup', function(event) {
            if (event.keyCode === 13) {
                APP.controller.Candidatos2018F3.buscarCandidatos(cargoF3, 0, $('.busca-event').val());
            }
        });
    },

    clicksMaisFiltros : function () {
        var maisFiltros = $('div#maisFiltros');

        // Abre os filtros
        c2018.find('div.filtros').on('click', '.maisFiltros', function(event) {
            event.preventDefault();
            maisFiltros.find('.content').scrollTop(0);
            maisFiltros.addClass('active');
            $('body').addClass('lockScroll');
            APP.controller.Candidatos2018F3.verificaFiltrosGravadosObj();
        });

        $('body').on('click', '#limiteCandidatos .maisFiltros', function(event) {
            event.preventDefault();
            maisFiltros.find('.content').scrollTop(0);
            maisFiltros.addClass('active');
            $('body').addClass('lockScroll');
            APP.controller.Candidatos2018F3.verificaFiltrosGravadosObj();
        });

        maisFiltros.on('click', 'label', function(event) {
            
            if ($(this).prev().is(':checked')) {
                event.preventDefault();
                $(this).prev().prop('checked', false);
            }
        });

        // Fecha os filtros
        maisFiltros.on('click', '.close', function(event) {
            event.preventDefault();
            maisFiltros.removeClass('active');
            $('body').removeClass('lockScroll');
        });

        // Limpa os filtros
        maisFiltros.on('click', '.limpar', function(event) {
            event.preventDefault();
            maisFiltros.find('input').prop('checked', false);
        });

        // Salva os filtros
        maisFiltros.on('click', '.salvar', function(event) {
            event.preventDefault();
            maisFiltros.removeClass('active');
            $('html').scrollTop(0);
            APP.controller.Candidatos2018.buscarCandidatos(cargoF3, 0);
            $('#limiteCandidatos').removeClass('active');
        });
    },

    clickScrollTop : function () {
        $('body').on('click', 'a.scrollTop', function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, 500);
        });

    },

    verificaFiltrosGravadosObj : function () {

        var f = $('#maisFiltros');

        f.find('#'+filtros.eleito).prop('checked', true);

        $.each(coligacoesSearch, function(index, val) {
            f.find('#'+this).prop('checked', true);
        });

    },


    //Click CANDIDATO
    clicksCandidato : function () {
        $('body').on('click', '.resultados .item .infos, .resultados .item .numero, .resultados .item .photo', function(event) {
            $('#candidato').scrollTop(0);
            $('body').addClass('lockScroll');
            $('#candidato').addClass('active');
            c2018.addClass('pushLeft');

            var idCandidato = $(this).parent().data('candidato');
            APP.controller.Candidatos2018F3.buscarInfosCandidato(idCandidato);

        });

        $('body').on('click', '.resultados .item input', function(event) {
            var total = 5;
            var selecionados = $('.candidatosSelecionados .items a').length;

            if (selecionados >= total && $(this).is(':checked')) {
                $(this).prop('checked', false);
            }

            var id = $(this).parent().data('candidato');
            var nome = $(this).parent().find('.nome').text()
            APP.controller.Candidatos2018F3.salvarCandidato(cargoF3, id, $(this).is(':checked'));
            if ($(this).is(':checked')) {
                $('.candidatosSelecionados div.items').append(`<a href="#" data-candidato="${id}" class="item">${nome}</a>`);
            } else {
                $('.candidatosSelecionados div.items').find('a[data-candidato="'+id+'"]').remove();
            }
            APP.controller.Candidatos2018F3.checkMaxCandidatosSelecionados();
        });

        $('body').on('click', '#candidato .voltar, #candidato .close', function(event) {
            event.preventDefault();
            $('body').removeClass('lockScroll');
            $('#candidato').removeClass('active');
            c2018.removeClass('pushLeft');
        });

        $('body').on('change', '#candidato input', function(event) {
            var id = $(this).data('id')
            var nome = $(this).parent().find('.nome').text();
            if ($(this).is(':checked')) {
                $('#candidato_'+ id).prop('checked', true);
                $('.candidatosSelecionados div.items').append(`<a href="#" data-candidato="${id}" class="item">${nome}</a>`);
                $('#candidato_'+ id).prop('checked', true);
                $('#candidato .photo').addClass('active');
            } else {
                $('#candidato_'+ id).prop('checked', false);
                $('.candidatosSelecionados div.items').find('a[data-candidato="'+id+'"]').remove();                
                $('#candidato .photo').removeClass('active');
            }
            APP.controller.Candidatos2018F3.salvarCandidato(cargoF3, id, $(this).is(':checked'));
            APP.controller.Candidatos2018F3.checkMaxCandidatosSelecionados();
        });  
    },

    clicksCandidatoSelecionado : function () {
        $('body').on('click', '.candidatosSelecionados a.triggerCandidatosSelecionados', function(event) {
            event.preventDefault();
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $(this).next().css("display", "flex");             
            } else {
                $(this).next().hide();
            }
            $('.candidatosSelecionadosWrap').height($('.candidatosSelecionados').outerHeight())
        });

        $('body').on('click', '.candidatosSelecionados div.items a', function(event) {
            event.preventDefault();
            c2018.find('#candidato_' + $(this).data('candidato')).prop('checked', false);
            APP.controller.Candidatos2018F3.salvarCandidato(cargoF3, $(this).data('candidato'), false);
            $(this).remove();
            APP.controller.Candidatos2018F3.checkMaxCandidatosSelecionados();
        });
    },

    checkMaxCandidatosSelecionados : function () {
        var total = 5;
        var selecionados = $('.candidatosSelecionados .items a').length;
        var cS = $('.candidatosSelecionados');
        var cSW = $('.candidatosSelecionadosWrap');
        var bA = $('button#avaliar');
        var cI = $('#candidato');

        if (selecionados === 0) {
            cS.hide();
            cSW.height(0)
            bA.attr('disabled', true);
            bA.removeClass('full');
            cI.removeClass('full');
            c2018.find('.resultados .items').removeClass('full')
            APP.controller.MeusCandidatosF3.saveCargoEtapa(cargoF3, 1);
        } else if (selecionados > 0 && selecionados < total) {
            cS.show();
            cSW.height(cS.outerHeight())
            bA.removeAttr('disabled');
            bA.find('span.counter').html(selecionados)
            bA.removeClass('full');
            cI.removeClass('full');
            c2018.find('.resultados .items').removeClass('full')
        } else if (selecionados >= total) {
            cS.show();
            cSW.height(cS.outerHeight())
            bA.removeAttr('disabled');
            bA.find('span.counter').html(selecionados)
            bA.addClass('full');
            cI.addClass('full');
            c2018.find('.resultados .items').addClass('full')
        }

    },

    buscarInfosCandidato: function (id) {

        var erro = "";
        $.ajax({
            type: "GET",
            data: { id },
            dataType: 'json',
            url: "/MeusCandidatos/DetalhesCandidato/",
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.Candidatos2018F3.preencheResultadoCandidato(result.candidato);
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });

    },

    preencheResultadoCandidato : function (result) {

        var candidato = $('div#candidato');
        var classe = result.status.toLowerCase();
        
        candidato.html('');
        
        candidato.append(`
            <a href="#" class="close"></a>
            <div class="header">
                <div class="title">
                    <a href="#" class="voltar">
                        <i class="fas fa-arrow-left"></i>
                    </a>
                    Dados do Candidato
                </div>
            </div>
            <div class="photo ${result.selecionado ? "active" : ""}"><span style="background-image: url(${result.foto})"></span></div>
            <div class="item">
                <a href="#" class="info" data-title="" data-info="coligacoes" data-texto='Os dados disponibilizados na plataforma GUIA DO VOTO, incluindo os status das candidaturas para todos os cargos, são atualizados a cada <strong>48 horas</strong> a partir de informações oficiais do <strong>TSE</strong>.' data-img="">
                    <img src="/assets/img/meucandidatoideal/icon-info.svg">
                </a>
                <div class="nome">${result.nome.toLowerCase()}</div>
                <div class="status">
                    <div class="title">Status</div>
                    <div class="txt ${slugify(classe)}">${result.status}</div>
                </div>
            </div>
            <div class="item">
                <div class="partido">
                    <div class="sigla">${result.sigla}</div>
                    <div class="txt">${result.partido}</div>
                </div>
                <div class="numero">${result.numero}</div>
            </div>
            ${(result.subCargos.length > 0 ?
                `<div class="item subcargos">
                    <div class="title">${cargoF3.toLowerCase().indexOf('senador') >= 0 ? "Suplente" : "Vice"}</div>
                    ${result.subCargos.map( l =>
                        `<div class="subcargo">
                            ${l.nome.toLowerCase()} (${l.sigla})
                        </div>`                    
                    ).join('')}
                </div>` : ''
            )}
            <div class="item">
                <div class="vice">
                    <div class="title">Coligação</div>
                    <div class="txt">
                        ${result.listaColigacao}
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="nomeCompleto">
                    <div class="title">Nome Completo</div>
                    <div class="txt">
                        ${result.nomeCompleto.toLowerCase()}
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="idade">
                    <div class="title">Idade</div>
                    <div class="num">${result.idade}</div>
                    <div class="txt">anos</div>
                </div>
                <div class="sexo">
                    <div class="title">Sexo</div>
                    <div class="icon">
                        <img src="/assets/img/meucandidatoideal/icon-${result.sexo == 'Masculino' ? 'mas' : 'fem'}.png" alt="">
                    </div>
                    <div class="txt">${result.sexo}</div>
                </div>
            </div>
            <div class="item">
                <div class="grau">
                    <div class="title">Grau/Instrução</div>
                    <div class="txt">${result.grau.toLowerCase()}</div>
                </div>
            </div>
            <div class="item">
                <div class="estadocivil">
                    <div class="title">Estado Civil</div>
                    <div class="txt">${result.estadoCivil}</div>
                </div>
                <div class="corRaca">
                    <div class="title">Cor/Raça</div>
                    <div class="txt">${result.cor}</div>
                </div>
            </div>
            <div class="item">
                <div class="ocupacao">
                    <div class="title">Ocupação</div>
                    <div class="txt">${result.ocupacao.toLowerCase()}</div>
                </div>
            </div>
            ${(result.valorMaximo != "-1" ? `
                <div class="item">
                    <div class="ocupacao">
                        <div class="title">Valor máximo da campanha</div>
                        <div class="txt">R$ ${result.valorMaximo}</div>
                    </div>
                </div>` : ``
            )}
            ${(result.sites.length > 0 ? `
                <div class="conhecerMais">
                    <p>Quer conhecer mais sobre este(a) candidato(a)? Acesse:</p>
                    ${result.sites.map( l =>
                        `<a href="${l.link}" target="_system">${l.nome}</a>`
                    ).join('')}
                    <small>Os dados apresentados em outras plataformas on-line não são de responsabilidade do Guia do Voto.</small>
                </div>` : ''
            )}
            <input ${result.selecionado ? "checked" : ""} id="candidato_${result.id}_in" data-id="${result.id}" type="checkbox" />
            <label for="candidato_${result.id}_in"></label>
        `)
    },

    clicksLimiteCandidatos : function () {
        $('body').on('click', '#limiteCandidatos .continue', function(event) {
            event.preventDefault();
            $('#limiteCandidatos').removeClass('active');
        });
    },

    setClickAvaliar : function () {
        $('body').on('click', '#avaliar', function () {

            APP.controller.MeusCandidatosF3.saveCargoEtapa(cargoF3, 2, '/MeusCandidatos/AvaliarSelecionados/'+cargoF3+'');

        });
    }

};