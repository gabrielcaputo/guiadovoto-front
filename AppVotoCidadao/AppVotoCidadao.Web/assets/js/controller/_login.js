/*
|--------------------------------------------------------------------------
| Controller Login
|--------------------------------------------------------------------------
*/

APP.controller.Login = {

    init: function () {

        this.setup();
        this.login();

    }, 

    setup : function () {

        document.getElementById("form-login").reset();

    },

    login : function () {
        APP.component.Loading.show();
        
        APP.component.Facebook.init();
        // APP.component.Facebook.connect();

        this.setShowAndHide();
        this.setFacebookLogin();
        this.setEsqueciSenha();        
        this.sendFormLogin();

    },

    setShowAndHide : function () {

        $('body header').hide();
        $('.back').prop('href', '/Inicio');

    },

    //Interações
    setFacebookLogin : function () {

        $('a.facebook').on('click', function (event) {

            event.preventDefault();

            //APP.component.Facebook.login();
            // window.location.href = 'https://www.facebook.com/v3.0/dialog/oauth?client_id=858598314350118&redirect_uri=https://13f56272.ngrok.io/Login&scope=email'; // HOMOLOG 
            //window.location.href = 'https://www.facebook.com/v3.0/dialog/oauth?client_id=858598314350118&redirect_uri=https://4fd95b97.ngrok.io/Login&scope=email'; // HOMOLOG 
            window.location.href = 'https://www.facebook.com/v3.0/dialog/oauth?client_id=247591319322843&redirect_uri=https://guiadovoto.org.br/Login&scope=email'; // PROD 

        });
        

    },

    //Send Login
    sendFormLogin: function (form) {

        $('.send-login').on('click', function (event) {

            event.preventDefault();

            var formLogin = APP.controller.Login.getFormLogin();
            var validate = APP.controller.Login.setValidateLogin(formLogin);

            if (validate) {
                
                APP.controller.Login.saveFormLogin(formLogin);
            }

        });

    },

    getFormLogin : function () {

        var form = $('#form-login');
        
        var login = {

            email: $(form).find('#email').val(),
            senha: $(form).find('#senha').val(),
            platform: APP.component.LocalStorage.getLocalStorage('platform'),
            playerId: APP.component.LocalStorage.getLocalStorage('playerID'),

        }

        return login;
    },

    setValidateLogin : function (formLogin) {

        //Email
        if(formLogin.email == '') {
            
            return false;
        }

        //Senha
        if(formLogin.senha == '') {

            return false;
        }
        
        return true;

    },

    saveFormLogin : function (_login) {

        var erro = "";
       
        $.ajax({
            type: "POST",
            data: _login,
            dataType: 'json',
            url: "/Login/Post",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                window.location.href = "/Home";
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        });

    },

    //Esqueci Minha Senha
    setEsqueciSenha: function () {

        $('.esqueci-minha-senha').on('click', function () {

            var title = 'Esqueceu sua senha?';
            var msg = '<strong>Não tem problema:</strong> digite seu e-mail no campo abaixo que enviaremos para a sua caixa de entrada.';

            var htmlForm = '';
            htmlForm += '<form id="esqueciSenha">';
            htmlForm += '<div class="field">';
            htmlForm += '<input type="email" id="email-es" required>';
            htmlForm += '<span class="bar"></span>';
            htmlForm += '<label class="label" for="email-es">E-mail*</label>';
            htmlForm += '</div>';
            htmlForm += '<input type="submit" value="ENVIAR" class="btn">';
            htmlForm += '</form>';

            APP.component.Alert.customGuiaDoVoto(title, msg, htmlForm);
            APP.controller.Login.sendFormEsqueciSenha();
        });

    },

    sendFormEsqueciSenha: function (form) {

        $('#esqueciSenha .btn').on('click', function (event) {

            event.preventDefault();

            var formEsqueciSenha = APP.controller.Login.getFormEsqueciSenha();
            var validate = APP.controller.Login.setValidateEsqueciSenha(formEsqueciSenha);

            if (validate) {
                
                APP.controller.Login.saveEsqueciSenha(formEsqueciSenha);

            }

        });

    },

    getFormEsqueciSenha : function () {

        var form = $('#esqueciSenha');
        
        var esqueciSenha = {

            email: $(form).find('#email-es').val(),

        }

        return esqueciSenha;
    },
    
    setValidateEsqueciSenha : function (formEsqueciSenha) {

        //Email
        if(formEsqueciSenha.email == '') {
            APP.component.Alert.customAlert('Preencha um email para recuperar a sua senha.', 'Atenção')
            
            return false;
        }
        
        return true;

    },

    saveEsqueciSenha : function (_esqueciSenha) {

        var erro = "";

        $.ajax({
            type: "POST",
            data: _esqueciSenha,
            dataType: 'json',
            url: "/Login/RecuperarSenha",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                var title = 'Esqueceu sua senha?';
                var msg = '<strong>E-mail enviado com sucesso!</strong><br><br>Um e-mail com sua senha foi enviado para <span style="color:#0C46E6;">'+result.email+'</span>';
                APP.component.Alert.customGuiaDoVoto(title, msg, '', 'OK', 'close');
            },
            error: function (result) {
                var title = 'OPA, ALGUMA COISA DEU ERRADO';
                var msg = '<strong>Usuário não localizado</strong><br><br>Verifique se você digitou o e-mail cadastrado no Guia do Voto.';
                APP.component.Alert.customGuiaDoVoto(title, msg, '', 'VOLTAR', 'esqueci-minha-senha');
                APP.controller.Login.setEsqueciSenha();
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        });

    },

};