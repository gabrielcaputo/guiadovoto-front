/*
|--------------------------------------------------------------------------
| Controller Cadastro
|--------------------------------------------------------------------------
*/

APP.controller.Cadastro  = {

    init: function () {

        this.setup(); 
        this.cadastro(); 

    },

    setup : function () {

        this.formularioCadastro = $('form#cadastro');
        //document.getElementById("cadastro").reset();

    },

    //Cadastrar
    cadastro : function () {

        APP.component.Facebook.init();
        //APP.component.Facebook.connect();
        //APP.component.DragAndDrop.init();
        //APP.component.Captcha.init();

        this.setShowAndHide();
        this.verificaEmailUrl();
        this.verificaEstadoUrl();
        this.setFacebookLogin();
        this.setListEstados();
        this.getListEstado();
        this.setEstado();
        this.setAjuda();
        this.setTermosCondicoes();
        this.sendFormCadastro();

    },

    setShowAndHide : function () {

        $('body header').hide();
        $('.back').prop('href', '/Inicio');

    },

    verificaEmailUrl : function () {
        var value = APP.component.Utils.getUrlParameter('email');
        if (value) {
            $('#cadastro #email').val(value);
        }
    },

    verificaEstadoUrl: function () {
        var value = APP.component.Utils.getUrlParameter('estado');
        if (value) {
            var temEstado = false;
            $('#estado option').each(function () {
                var estado = $(this).val();
                if (estado == value.toUpperCase()) {
                    temEstado = true;
                    $('#estado').val(estado)
                }
            })
        }
    },

    //Interações
    setFacebookLogin : function () {

        $('a.facebook').on('click', function (event) {
            event.preventDefault();

            //APP.component.Facebook.login();
            // window.location.href = 'https://www.facebook.com/v3.0/dialog/oauth?client_id=858598314350118&redirect_uri=https://13f56272.ngrok.io/Cadastro&scope=email'; // HOMOLOG 
            // window.location.href = 'https://www.facebook.com/v3.0/dialog/oauth?client_id=858598314350118&redirect_uri=https://4fd95b97.ngrok.io/Cadastro&scope=email'; // HOMOLOG 
            window.location.href = 'https://www.facebook.com/v3.0/dialog/oauth?client_id=247591319322843&redirect_uri=https://guiadovoto.org.br/Cadastro&scope=email'; // PROD 

        });
        

    },

    setListEstados : function () {

        $('.triggerEstado').on('click',function (event) {

            event.preventDefault();

            $(this).toggleClass('disabled');

            $('#estados').toggleClass('active');
            $('section').toggleClass('blur');

            $('#estados li').removeClass('active');

            if ($('#estado').val() != '') {
                $('#estados li[value=' + $('#estado').val() + ']').addClass('active');
            } 


        });

    },

    getListEstado : function () {

        $('#estados li').on('click',function (event) {

            event.preventDefault();
            
            $('#estados li').removeClass('active');
            $(this).toggleClass('active');
            
            var estado = $(this).attr('value');

            $('#estado').val(estado);
            $('.set-estado').trigger( "click" );

        });

    },

    setEstado : function () {

        $('.set-estado').on('click',function (event) {

            event.preventDefault();
            
            $('.triggerEstado').trigger( "click" );

        });

    },

    setTermosCondicoes : function () {

        $('.termos a').on('click', function(event) {

            event.preventDefault();
            
            var title = 'TERMOS E POLÍTCAS';
            //var html = '<iframe src="/assets/templates/termos.html"></iframe>';

            APP.component.Alert.customTermos(title, '');

        });

    },

    //Esqueci Minha Senha
    setAjuda: function () {

        $('#ajuda').on('click', function (event) {

            event.preventDefault();
            var m = $(".modal#ajuda");
            $("#mask").fadeIn(200, function() {
                m.center();
                m.fadeIn(200, function() {
                    m.center();
                });		
            });

        });

    },

    //Send Cadastro
    sendFormCadastro : function (form) {

        $('.send-cadastro').on('click', function (event) {

            event.preventDefault();

            var formCadastro = APP.controller.Cadastro.getFormCadastro();
            var validate = APP.controller.Cadastro.setValidateCadastro(formCadastro);

            formCadastro.Id_JS = APP.component.Utils.getUrlParameter('id');

            if (validate) {
                APP.controller.Cadastro.saveFormCadastro(formCadastro);
            }

        });

    },

    getFormCadastro : function () {

        var form = $('form#cadastro');
        
        var cadastro = {

            email: $(form).find('#email').val(),
            senha: $(form).find('#senha').val(),
            confirmarSenha: $(form).find('#confirma-senha').val(),
            estado: $(form).find('#estado :selected').val(),
            termos: $(form).find('#termos:checked').val(),
            // captcha: grecaptcha.getResponse(),
            platform: APP.component.LocalStorage.getLocalStorage('platform'),
            playerId: APP.component.LocalStorage.getLocalStorage('playerID'),
            emailFB: APP.component.LocalStorage.getLocalStorage('email-fb'),
            idFacebook: APP.component.LocalStorage.getLocalStorage('id-fb'),

        }

        return cadastro;
    },

    setValidateCadastro : function (formCadastro) {

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        $('form#cadastro').find('.error').removeClass('error');
        $('form#cadastro').find('.right').removeClass('right');
        
        //Email
        if(formCadastro.email == '' || validateEmail(formCadastro.email) === false) {
            $('form#cadastro #email').closest('.field').addClass('error');
        } else {
            $('form#cadastro #email').closest('.field').addClass('right');
        }

        //Senha
        if(formCadastro.senha == '') {
            $('form#cadastro #senha').closest('.field').addClass('error');
        } else {
            $('form#cadastro #senha').closest('.field').addClass('right');
        }  

        //Confirmar Senha
        if(formCadastro.confirmarSenha == '' || formCadastro.confirmarSenha != formCadastro.senha) {
            $('form#cadastro #confirma-senha').closest('.field').addClass('error');
        } else {
            $('form#cadastro #confirma-senha').closest('.field').addClass('right');
        }

        //Estado
        if(formCadastro.estado == '') {
            // APP.component.Alert.customAlert('Por favor selecione uma opção no campo Estado.', 'Atenção');
            $('form#cadastro #estado').closest('.field').addClass('error');
        } else {
            $('form#cadastro #estado').closest('.field').addClass('right');    
        }

        if ($('form#cadastro').find('.error').length > 0) {
            var totalError = $('.error').length;
           
            var error = "";
            $('.error').each(function(index, el) {
                if (totalError === 1) {
                    error = $(this).data('field');
                }
                if (totalError > 1) {
                    
                    if (totalError != index + 1) {
                        if (index + 1 < totalError - 1) {
                            error = error + $(this).data('field') + ", "
                        } else {
                            error = error + $(this).data('field') + " e "
                        }
                    } else {
                        error = error + $(this).data('field') + "."
                    }
                }
            });
           
            APP.component.Alert.customAlert('Preencha corretamente os seguintes campos: <br>' + error, 'Atenção');
            return false;
        }

        //Termos
        if(formCadastro.termos != "true") {
            APP.component.Alert.customAlert('Para continuar é necessário aceitar os Termos e Condições de Uso.', 'Atenção');
            return false;
        }

        // // Captcha
        // if (!APP.component.Captcha.validate()) {
        //     var captcha = $('#captchaInput').val();

        //     if (captcha != '') {
        //         APP.component.Alert.customAlert('Para continuar é necessário digitar o código de verificação válido.', 'Atenção');
        //     } else {
        //         APP.component.Alert.customAlert('Para continuar é necessário digitar o código de verificação.', 'Atenção');
        //     }

        //     return false;
        // }        
        
        return true;

    },

    saveFormCadastro : function (_cadastro) {

        APP.component.Loading.show()
        var erro = "";
       
        $.ajax({
            type: "POST",
            data: _cadastro,
            dataType: 'json',
            url: "/Cadastro/Salvar",
            beforeSend: function () {
                
            },
            success: function (result) {
                window.location.href = "/Home";
            },
            error: function (result) {
                APP.component.Loading.hide()
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                
            }
        });

    }

};

