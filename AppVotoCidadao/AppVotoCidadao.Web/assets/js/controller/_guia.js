/*
|--------------------------------------------------------------------------
| Controller Guia
|--------------------------------------------------------------------------
*/

APP.controller.Guia = {

    init : function () {

        this.setup();

        if ($('section#capitulo').length > 0) {
            APP.controller.Capitulo.init();
        } else {
            this.guia();
        }

    },

    setup : function () {

        //

    },

    //Guia 
    guia : function () {

        this.setHideAndShow();
        this.getChapters();
        this.setClickEnviarGuia();
        
    },

    setHideAndShow :function () {

        var login = APP.component.Utils.getLogin();

        if (!login) {
            $('.envio-email').hide();
        }
        $('.back').prop('href', '/Home');


    },

    verificaIOS : function () {

        var iOS = parseFloat(
            ('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0,''])[1]).replace('undefined', '3_2').replace('_', '.').replace('_', '')
        ) || false;

        if (iOS > 11.3) {
            //APP.component.Alert.customAlert(iOS);
            $('.chapters').find('.display-sub-chapters').hide();
            $('.sub-chapters').show();
        }
        

    },

    //GET
    getChapters : function () {

        $.ajax({
            type: "GET",
            datatype : "application/json",
            contentType: "text/plain",
            url: '/Guia/GetGuia',
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.Guia.setInfosChapters(result);
                //APP.controller.Guia.verificaIOS();
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });
        
    },

    //SET
    setInfosChapters : function (_chapters) {

        $(_chapters).each(function (i) {

            APP.controller.Guia.setHtmlChapters(this, i)

        });


    },

    setHtmlChapters : function (chapter, index) {

        var html = '';

        html += '<div class="chapter">';
        html +=     '<!-- Capa -->';
        html +=     '<div class="cover">';
        html +=         '<!-- Progressao -->';
        html +=         '<div class="progression">';
        html +=             '<ul>';

        //Progresso
        $(chapter.subCapitulos).each(function () {
            var classCompleted = '';
            if (this.porcentagem != '100') {
                classCompleted = 'not-comp';
            }
            html +=             '<li><div class="bar '+classCompleted+' " style="width: '+this.porcentagem+'%;"></div></li>';
        });
        
        html +=             '</ul>';
        html +=         '</div>';
        html +=         '<!-- Informaçõpes -->';
        html +=         '<div class="info">';
        html +=             '<img src="/assets/img/guia/book_guia_cap0'+chapter.idCapitulo+'.png" alt=""/>';
        html +=             '<div class="title">';
        html +=                 '<span>Capítulo</span>';
        html +=                 '<p data-num-cap="'+chapter.idCapitulo+'.0">';
        html +=                     ''+chapter.titulo+'';
        html +=                 '</p>';
        html +=             '</div>';
        html +=             '<a href="#" class="display-sub-chapters"><i class="fas fa-chevron-up"></i></a>';
        html +=         '</div>';
        html +=     '</div>';
        html +=     '<!-- Sub Capitulos -->';
        html +=     '<div class="sub-chapters">';
        html +=         '<ul>';

        //Sub Capitulos
        $(chapter.subCapitulos).each(function () {

            var classe = "";
            if (this.porcentagem === 100) {
                classe = "read";
            } else if (this.idReferencia === chapter.idUltimoSubCapitulo) {
                classe = "active";
            } else {
                classe = "";
            }

            html +=         '<li class="'+classe+'"><a href="/Guia/Capitulo?cap='+chapter.idCapitulo+'&sub='+this.idReferencia+'"><span data-num-subcap="'+this.idReferencia+'">'+this.subTitulo+'</span></a></li>';
        });
        html +=         '</ul>';
        html +=     '</div>';
        html += '</div>';

        $('.chapters').append(html);
        APP.controller.Guia.setVerMaisCapitulo();
    },

    //INTERAÇÕES
    setVerMaisCapitulo : function () {

        $('.cover').unbind('click');
        $('.cover').on('click', function (event) {

            event.preventDefault();
            $('.display-sub-chapters').removeClass('active');
            $('.sub-chapters').stop().slideUp();
            
            $(this).find('.display-sub-chapters').toggleClass('active');
            $(this).closest('.chapter').find('.sub-chapters').stop().slideToggle();

        });

    },

    setClickEnviarGuia : function () {

        $('.envio-email').on('click', function () {

            APP.controller.Guia.sendEmailGuia();

        });

    },

    sendEmailGuia : function () {

        $.ajax({
            type: "POST",
            url: '/Guia/EnviarEmail',
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.component.Alert.customAlert('O Guia do Voto (PDF) foi enviado para o e-mail cadastrado.', 'PRONTO!');
            },
            error: function (result) {
                APP.component.Alert.customAlert('Erro ao enviar o e-mail.', 'ATENÇÃO');
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });

    },

};