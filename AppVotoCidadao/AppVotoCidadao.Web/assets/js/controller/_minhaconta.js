/*
|--------------------------------------------------------------------------
| Controller Minha Conta
|--------------------------------------------------------------------------
*/

APP.controller.MinhaConta  = {

    init: function () {

        this.setup(); 
        this.minhaConta(); 

    },

    setup : function () {



    },

    //Minha Conta
    minhaConta : function () {
        
        APP.component.Facebook.init();
        this.getInfoMinhaConta();
        this.setOlho();
        this.setClickBell();
        this.setListEstados();
        this.getListEstado();
        this.setEstado();        
        this.sendFormMinhaConta();
    },


    //Interações
    setListEstados : function () {

        $('.triggerEstado').on('click',function (event) {

            event.preventDefault();

            $(this).toggleClass('disabled');

            $('#estados').toggleClass('active');
            $('section').toggleClass('blur');

            $('#estados li').removeClass('active');

            if ($('#estado').val() != '') {
                $('#estados li[value=' + $('#estado').val() + ']').addClass('active');
            } 


        });

    },

    getListEstado : function () {

        $('#estados li').on('click',function (event) {

            event.preventDefault();
            
            $('#estados li').removeClass('active');
            $(this).toggleClass('active');
            
            var estado = $(this).attr('value');

            $('#estado').val(estado);
            $('.set-estado').trigger( "click" );

        });

    },

    setEstado : function () {

        $('.set-estado').on('click',function (event) {

            event.preventDefault();
            
            $('.triggerEstado').trigger( "click" );

        });

    },

    setOlho: function () {

        $('.olho').on('click', function () {
            $(this).toggleClass('active');

            if ($(".olho").hasClass('active')) {
                $("#senha").attr("type", "text");                
            } else {
                $("#senha").attr("type", "password");
            }

            
        })
    },

    setClickBell : function () {

        $('.bell').on('click', function (event) {
            event.preventDefault();

            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $(this).find('input').prop('value',true);
            } else {
                $(this).find('input').prop('value',false);
            }
            

        });

    },

    //GET
    getInfoMinhaConta : function () {

        var playerId = localStorage.getItem(playerId);

        $.ajax({
            type: "GET",
            data: {'playerId' : playerId},
            dataType: 'json',
            url: '/MinhaConta/BuscaUsuario',
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.MinhaConta.setInfoMinhaConta(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });

    },

    setInfoMinhaConta : function (_minhaConta) {

        $('#email').val(_minhaConta.email);
        $('#senha').val(_minhaConta.senha);
        $('#estado').val(_minhaConta.estado),
        $('#notificacao').val(_minhaConta.notificar);

        if (_minhaConta.notificar == true) {
            $('.bell').addClass('active');
        }

        if ($('#estado').val() != '') {
            $('#estados li[value=' + $('#estado').val() + ']').addClass('active');
        } 

    },
        
    //Send Minha Conta
    sendFormMinhaConta : function (form) {

        $('.send-minha-conta').on('click', function (event) {

            event.preventDefault();

            var formMinhaConta = APP.controller.MinhaConta.getFormformMinhaConta();
            var validate = APP.controller.MinhaConta.setValidateformMinhaConta(formMinhaConta);

            if (validate) {
                
                APP.controller.MinhaConta.saveFormMinhaConta(formMinhaConta);
            }

        });

    },

    getFormformMinhaConta : function () {

        var form = $('form#minhaconta-f');

        var minhaConta = {

            email: $(form).find('#email').val(),
            senha: $(form).find('#senha').val(),            
            estado: $(form).find('#estado :selected').val(),
            notificar: $(form).find('#notificacao').val() == "true" ? true : false,    
        }

        return minhaConta;
    },

    setValidateformMinhaConta : function (formMinhaConta) {

        //Email
        if(formMinhaConta.email == '') {
            
            return false;
        }

        //Senha
        if(formMinhaConta.senha == '') {

            return false;
        }
        

        //Estado
        if(formMinhaConta.estado == '') {

            return false;
        }
        
        return true;

    },

    saveFormMinhaConta : function (_minhaConta) {

        var erro = "";
       
        $.ajax({
            type: "PUT",
            data: _minhaConta,
            dataType: 'json',
            url: "/MinhaConta/AtualizaUsuario",
            beforeSend: function () {
                
            },
            success: function (result) {
                window.location.href = "/Home";
            },
            error: function (result) {
                APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro");
            },
            complete: function (result) {
                
            }
        });

    }

};

