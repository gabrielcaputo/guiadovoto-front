﻿/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

APP.controller.Home = {

    init : function () {

        this.setup();
        this.home();

    },

    setup : function () {

        if ($( window ).width() > 960) {
            //$('section').toggleClass('desk');
        }

    },

    //Home 
    home : function () {

        this.setShowAndHide();
        this.setScrollTopHome();

        //Dias
        this.setProgressDate();
        this.setContagem();

        this.setCidadaniaNota10();
        this.setMinhasPrioridades();
        this.setMeusCandidatos();

        this.getCandidatosEscolhidos();

    },

    setShowAndHide : function () {

        var login = APP.component.Utils.getLogin();
        if (login) {
            $('.back img').hide();
        }

        $('.back').prop('href', '/Inicio');

        //Fase 01 - Depois retirar
        $('.etapas .wrap .etapa .info .status .quiz-respondido p').text('');

        var fase = $('meta[name=fase]').attr('content');

        $('section#home div.wrap:nth-of-type('+fase+')').addClass('ultimo-acesso');

        if (fase == 3) {
            $('.meus-candidatos .etapa .sessoes').css('display', 'block');
            $('div.wrap:nth-of-type('+fase+')').addClass('active');
            APP.controller.MeusCandidatosHome.setSlickCandidatos();
        }

        APP.controller.Home.setHeightUltimoAcesso();

    },

    setScrollTopHome : function () {

        var altura = $('.ultimo-acesso').offset().top;
        $('html, body').animate({
            scrollTop: altura, 
        }, 1000, function () {

        });

    },

    setHeightUltimoAcesso : function () {

        var header = $('header').outerHeight();
        var timeline = $('.timeline').outerHeight();
        var etapas = $('.etapas').outerHeight();
        var ultimoAcesso = $('.ultimo-acesso').outerHeight();
        var calc = (etapas + timeline + header) - (ultimoAcesso - 20);
        
        $('.ultimo-acesso').css('min-height', 'calc(100vh - ' +calc+'px)');

    },

    //Date
    setProgressDate : function () {

        var fullDate = new Date();
        var dia = fullDate.getDate();

        if (dia < 7) {
            var porcentagemMes = 0;
        } else if (dia > 7 && dia < 28) {
            var porcentagemMes = (dia - 6) * 4.5455;
        } else {
            var porcentagemMes = 100;
        }

        Math.ceil(porcentagemMes);

        if (dia >= 28) {
            $('.progresso .bar li').find('span').addClass('active');
            $('.progresso .bar li').find('span').addClass('last');
            $('.progresso .bar li').find('span').css('width', ''+porcentagemMes+'%');
        } else {
            $('.progresso .bar li').find('span').addClass('active');
            $('.progresso .bar li').find('span').css('width', ''+porcentagemMes+'%');
        }

    },

    setContagem: function () {

        var target_date = new Date("october 28, 2018").getTime();
        //var dias, horas, minutos, segundos;
        var dias;
        var regressiva = document.getElementById("regressiva");

        var current_date = new Date().getTime();
        var segundos_f = (target_date - current_date) / 1000;

        dias = parseInt(segundos_f / 86400);
        segundos_f = segundos_f % 86400;

        document.getElementById('dia').innerHTML =  dias+1;

    },

    //Timeline
    setTimeline : function () {

        if ($('.sessoes:visible').length > 0) {
            $('.timeline').slideUp();

        } else {
            $('.timeline').slideDown();
            

            if (window.navigator.userAgent.indexOf('Edge') == -1) {
                $('.wrap').removeClass('decrease');
            }
        }

    },

    //Interações
    setClickEtapas : function () {        

        $('.etapas .title, .etapas .etapa > .info').off('click');
        $('.etapas .title, .etapas .etapa > .info').on('click', function (event) {

            var wrap = $(this).closest('.box').attr('class').split(' ');

            $('.container .etapas .wrap').addClass('decrease');
            $('.container .etapas .wrap .'+wrap[1]+'').closest('.wrap').removeClass('decrease');

            //$('.container .etapas .wrap').removeClass('ultimo-acesso');

            var sessoes = $(this).closest('.box').find('.sessoes').is(':visible');

            if ($(this).closest('.box').hasClass('cidadania')) {
                //Cidadania Nota 10
                APP.controller.Home.setRulesCidadania(sessoes, this);
            } else if ($(this).closest('.box').hasClass('prioridades')) {
                //Minhas Prioridades
                APP.controller.Home.setRulesPrioridades(sessoes, this);
            } else if ($(this).closest('.box').hasClass('meus-candidatos')) {
                //Meus Candidatos
                APP.controller.Home.setRulesMeusCandidatos(sessoes, this);
                APP.controller.MeusCandidatosHome.getVerificaPrimeiroAcesso();
            } 

        });

        if (window.location.search === "?MinhasPrioridades") {
            $('.prioridades .title').click();
        };

    },

    setRulesCidadania : function (sessoes, _this) {

        $(_this).closest('.wrap').siblings('.wrap').find('.sessoes').slideUp(400 , function() {
            if ($('.minhas-prioridades').hasClass('slick-slider')) {
                $('.minhas-prioridades').slick('unslick'); 
            }
            if ($('.candidatos').hasClass('slick-slider')) {
                $('.candidatos').slick('unslick'); 
            }

            $('.prioridades').closest('.wrap').removeClass('active');
            $('.meus-candidatos').closest('.wrap').removeClass('active');
        });

        if (sessoes) {
            $(_this).closest('.wrap').find('.etapa').find('.sessoes').slideUp(200, function() {
                APP.controller.Home.setTimeline();
                $('.cidadania').closest('.wrap').removeClass('active');
                if (APP.component.Utils.getLogin()) {
                    $('.quizzes').slick('unslick');   
                }
            });
        } else  {
            if (APP.component.Utils.getLogin()) {
                APP.controller.CidadaniaNota10.setSlickQuizzes();
            }
            if ($(window).width() < 960 && APP.component.Utils.getLogin()) {
                APP.controller.CidadaniaNota10.setClickQuiz();
            }
            $(_this).closest('.wrap').find('.etapa').find('.sessoes').slideDown(200, function() {
                $(this).closest('.wrap').addClass('active');
                APP.controller.Home.setTimeline();
            });
        }
    },

    setRulesPrioridades : function (sessoes, _this) {

        $(_this).closest('.wrap').siblings('.wrap').find('.sessoes').slideUp(400 , function() {
            if (APP.component.Utils.getLogin() && $('.quizzes').hasClass('slick-slider')) {
                $('.quizzes').slick('unslick');
            }
            if ($('.candidatos').hasClass('slick-slider')) {
                $('.candidatos').slick('unslick'); 
            }
            $('.cidadania').closest('.wrap').removeClass('active');
            $('.meus-candidatos').closest('.wrap').removeClass('active');
        });
        if (sessoes) {
            $(_this).closest('.wrap').find('.etapa').find('.sessoes').slideUp(200, function() {
                APP.controller.Home.setTimeline();
                $('.minhas-prioridades').slick('unslick');
                $(this).closest('.wrap').removeClass('active');
            });
        } else  {
            APP.controller.MinhasPrioridades.setSlickPrioridades();
            $(_this).closest('.wrap').find('.etapa').find('.sessoes').slideDown(200, function() {
                $(this).closest('.wrap').addClass('active');
                APP.controller.Home.setTimeline();
            });
        }

        var leuDadosNaoRegistrados = localStorage.getItem('LeuDadosNaoRegistrados');
        if (leuDadosNaoRegistrados == null) {

            APP.component.Alert.dadosNaoRegistrados();
        }

    },

    setRulesMeusCandidatos : function (sessoes, _this) {

        $(_this).closest('.wrap').siblings('.wrap').find('.sessoes').slideUp(400 , function() {
            if (APP.component.Utils.getLogin() && $('.quizzes').hasClass('slick-slider')) {
                $('.quizzes').slick('unslick');
            }
            if ($('.minhas-prioridades').hasClass('slick-slider')) {
                $('.minhas-prioridades').slick('unslick'); 
            }
            //Modal para Login
            if (!APP.component.Utils.getLogin() && (!$('.meus-candidatos').closest('.wrap').hasClass('active'))) {

                // APP.component.Alert.customAlert("Ops!", "A área <strong>Meus Candidatos</strong> é exclusiva para usuários cadastrados.");
                var title = 'Atenção';
                var msg = 'A área <strong>Meus Candidatos</strong> é exclusiva para usuário cadastrados.';

                var htmlForm = `
                    <a href="/Login" class="btn login">
                        <span>Entre com Login e Senha</span>
                    </a>
                    <a href="/Cadastro" class="btn cadastro">
                        <span>Cadastre-se</span>
                    </a>
                `;

                APP.component.Alert.customGuiaDoVoto(title, msg, htmlForm);

            }
            $('.prioridades').closest('.wrap').removeClass('active');
            $('.cidadania').closest('.wrap').removeClass('active');
        });

        if (sessoes) {
            $(_this).closest('.wrap').find('.etapa').find('.sessoes').slideUp(200, function() {
                APP.controller.Home.setTimeline();
                $('.candidatos').slick('unslick');
                $('.meus-candidatos').closest('.wrap').removeClass('active');
                //if (APP.component.Utils.getLogin()) {

                //}
            });
        } else  {
            APP.controller.MeusCandidatosHome.setSlickCandidatos();
            $(_this).closest('.wrap').find('.etapa').find('.sessoes').slideDown(200, function() {
                $(this).closest('.wrap').addClass('active');
                APP.controller.Home.setTimeline();
            });

            //Verifica se tem 2 turno
            if(!$('#estado :selected').data('segundo-turno')) {
                $('.candidatos').slick("slickSetOption", "swipe", false);
            }
        }

    },

    /*
    |--------------------------------------------------------------------------
    | Cidade Nota 10
    |--------------------------------------------------------------------------
    */
    setCidadaniaNota10 : function () {

        APP.controller.CidadaniaNota10.init();

    },

    /*
    |--------------------------------------------------------------------------
    | Minhas Prioridades
    |--------------------------------------------------------------------------
    */
    setMinhasPrioridades : function () {

        APP.controller.MinhasPrioridades.init();

    },

    /*
    |--------------------------------------------------------------------------
    | Meus Candidatos
    |--------------------------------------------------------------------------
    */
    setMeusCandidatos : function () {

        APP.controller.MeusCandidatosHome.init();

    },

    /*
    |--------------------------------------------------------------------------
    | Meu Voto
    |--------------------------------------------------------------------------
    */
    getCandidatosEscolhidos : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/MeuVoto/BuscarVotosST',
            beforeSend: function () {
            },
            success: function (result) {
                APP.controller.Home.setButtomMeuVoto(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
            }
        }).promise().done(function () {
            APP.controller.MeuVoto.setClickCandidato();
        });

    },

    setButtomMeuVoto : function (result) {

        if (result !== null && result.resultado !== null && result.resultado.length > 0) {
            $('.cola-voto').addClass('active');
        } else {
            $('body').on('click', '.cola-voto', function(event) {
                event.preventDefault();
            });
        }

    },

};