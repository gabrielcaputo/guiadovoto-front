/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/
APP.controller.MeuVoto = {

    init : function () {

        this.setup();
        this.meuVoto();
        this.setClickInfos();
        this.buscaResultadoGovernador();
        this.setClickClose();
    },

    setup : function () {

        APP.component.Compartilhar.init();

    },

    meuVoto: function () {
        this.getCandidatosEscolhidos();
        this.getCargoEtapaList();
        
    },

    buscaResultadoGovernador : function () {

        $.ajax({
            type: "GET",
            url: '/SegundoTurno/BuscarCandidatos?cargo=governador',
            
            success: function (result) {
                console.log(result.erro);
            },
            error: function (result) {
                $('div.item:first-child').remove();
            },
            complete: function (result) {
                console.log(result);
            }
        });

    },

    getCandidatosEscolhidos : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/MeuVoto/BuscarVotosST',
            beforeSend: function () {
            },
            success: function (result) {
                APP.controller.MeuVoto.setCandidatosEscolhidos(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
            }
        }).promise().done(function () {
            APP.controller.MeuVoto.bind();
        });

    },

    setCandidatosEscolhidos : function (result) {

        $(result.resultado).each(function () {

            var cargoResult = properCase(this.cargo).replace(' ', '');
            var item = $('.lista-candidatos').find('[data-cargo="'+cargoResult+'"]');

            //Info Reduzida
            item.find('.info-reduzida').find('.nome').text((this.candidato.nome).toLowerCase());
            item.find('.info-reduzida').find('.numero span').text(this.candidato.numero);
            var html = `
            
                <div class="info-candidato">
                    <div class="photo"><span style="background-image: url(${this.candidato.foto})"></span></div>
                    <div class="candidato">
                        <div class="name">
                            <span class="nome">${(this.candidato.nome).toLowerCase()}</span>
                            <span class="numero">${this.candidato.numero}</span>
                        </div>
                        <div class="partido">
                            <span class="sigla">${this.candidato.sigla}</span>
                            <span class="nome-partido">(${(this.candidato.partido).toLowerCase()})</span>
                        </div>
                    </div>
                    <div class="afinidade">
                        <span class="texto">Afinidade</span>
                        <span class="${this.candidato.avaliado == false ? 'nao-avaliado' : 'porcentagem'}">${this.candidato.avaliado == false ? 'CANDIDATO NÃO AVALIADO' : this.candidato.afinidade+'%'}</span>
                    </div>
                </div>
                <div class="actions">
                    <a href="#" class="btn excluir"><span>Excluir</span></a>
                    <a href="#" class="btn ver-dados" data-id="${this.candidato.id}" data-afinidade="${this.candidato.avaliado == false ? 'naoavaliado' : this.candidato.afinidade}"><span>perfil do candidato</span></a>
                </div>

            `;
                        

            item.find('.wrap').removeClass('no-item');
            item.find('.wrap').html(html);

        });

    },

    limpaInfosNoItem : function (_cargo) {

        var html = `
            <div class="info">
                <p class="texto">Para avaliar candidatos a outro(s) cargo(s), clique no botão abaixo.</p>
                <a href="#" class="btn avaliar"><span>Avaliar Cargo</span></a>
            </div>
        `;

        $('.lista-candidatos').find('[data-cargo="'+_cargo+'"]').find('.wrap').html(html);
        $('.lista-candidatos').find('[data-cargo="'+_cargo+'"]').find('.wrap').addClass('no-item');
        $('.lista-candidatos').find('[data-cargo="'+_cargo+'"]').find('.info-reduzida .candidato .nome').text('');
        $('.lista-candidatos').find('[data-cargo="'+_cargo+'"]').find('.info-reduzida .numero span').text('');
        $('.lista-candidatos').find('[data-cargo="'+_cargo+'"]').find('.wrap').slideToggle();
        $('.lista-candidatos').find('[data-cargo="'+_cargo+'"]').removeClass('active');
    },

    setClickInfos : function () {
        $('body').on('click', '#candidatoAfinidade .info', function(event) {
            event.preventDefault();

            var text = $(this).data('texto');
            var img = $(this).data('img');
            var title = $(this).data('title');

            APP.component.Alert.modalInformacoes(title, text, false, 'voce-sabia')
        });
    },

    /*
    |--------------------------------------------------------------------------
    | INTERACAO
    |--------------------------------------------------------------------------
    */
    setClickCandidato : function () {

        $('.lista-candidatos .item .info-reduzida').on('click', function (event) {

            event.preventDefault();

            $(this).closest('.item').find('.wrap').slideToggle();
            $(this).closest('.item').toggleClass('active');

        });

    },

    setClickVerDados : function () {

       $('.ver-dados').on('click', function (event) {

            event.preventDefault();

            var idCandidato = $(this).data('id');
            var afinidade = $(this).data('afinidade');
            var cargo = unCamelCase($(this).parents('.item').data('cargo'));

            $('body').addClass('lockScroll');
            $('section#meu-voto').addClass('pushLeft');
            $('#candidatoAfinidade').addClass('active');

            $('#candidatoAfinidade').scrollTop(0);
            
            APP.controller.GrauDeAfinidade.buscarInfosCandidato(idCandidato, afinidade, true, cargo);
            
            $("#candidatoAfinidade label").hide();
       });

    },

    setClickVoltar : function () {

        $('body').on('click', '#candidatoAfinidade .voltar', function(event) {
            event.preventDefault();
            $('body').removeClass('lockScroll');
            $('#candidatoAfinidade').removeClass('active');
            $('section#meu-voto').removeClass('pushLeft');
        });
    },

    setClickExcluir : function () {

        $('.excluir').on('click', function (event) {

            event.preventDefault();

            var cargoClick = $(this).closest('.item').data('cargo');
            APP.controller.MeuVoto.setRemoverCargo(cargoClick);            
       });

    },

    bind : function () {

        APP.controller.MeuVoto.setClickCandidato();
        APP.controller.MeuVoto.setClickExcluir();
        APP.controller.MeuVoto.setClickVerDados();
        APP.controller.MeuVoto.setClickVoltar();

    },

    setRemoverCargo : function (_cargo) {

        var data = {
            'cargo': _cargo,
        }
        $.ajax({
            url: '/SegundoTurno/RemoverVoto/',
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                // $('body').addClass('lockScroll');
                APP.component.Loading.show();
            }
        })
        .done(function(response) {
            APP.controller.MeuVoto.limpaInfosNoItem(_cargo);
            APP.controller.MeuVoto.getCargoEtapaList();
        })
        .always(function() {
            APP.component.Loading.hide();
        })
        .fail(function(response) {
        })
    },

        /*
    |--------------------------------------------------------------------------
    | Selecionar Cargos
    |--------------------------------------------------------------------------
    */

    getCargoEtapaList : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/MeusCandidatos/BuscarEtapa',
            beforeSend: function () {
            },
            success: function (result) {
                APP.controller.MeuVoto.setCargoEtapa(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
            }
        }).promise().done(function () {
        });

    },

    setCargoEtapa : function (result) {

        $(result).each(function (index) {
            
            var etapa = this.etapa;
            var cargoEtapa = properCase(this.candidato).replace(' ', '');
            APP.controller.MeuVoto.setRulesEtapa(etapa, cargoEtapa, index+1);

        });

    },

    setRulesEtapa : function (_etapa, _cargoEtapa, _index) {

        var avaliarCargo = $('[data-cargo='+_cargoEtapa+']');
        
        switch(_etapa) {
            case 1:
                avaliarCargo.find('.wrap a.avaliar').attr('href', '/MeusCandidatos/Candidatos2018/'+_cargoEtapa);
                break;
            case 2:
                avaliarCargo.find('.wrap a.avaliar').attr('href', '/MeusCandidatos/AvaliarSelecionados/'+_cargoEtapa);
                break;
            case 3:
                avaliarCargo.find('.wrap a.avaliar').attr('href', '/MeusCandidatos/Candidatos2018/'+_cargoEtapa);
                break;
            case 4:
                avaliarCargo.find('.wrap a.avaliar').attr('href', '/MeusCandidatos/Candidatos2018/'+_cargoEtapa);
                break;
            default:
                
        }

    },

};