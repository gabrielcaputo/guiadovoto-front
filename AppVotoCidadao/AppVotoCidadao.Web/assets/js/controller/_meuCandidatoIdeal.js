﻿/*
|--------------------------------------------------------------------------
| Controller Sobre
|--------------------------------------------------------------------------
*/
var filtros;
var user = APP.component.user.get();
APP.controller.MeuCandidatoIdeal = {

    init: function () {
        APP.component.Loading.show();

        this.handleBack();
        this.getContentPerfis();
        this.setScrollPerfis();
        this.setSelecionarPartido();
        this.setActionFiltros();
        this.setActionInfos();
        this.setActionCopySenador();
        this.setActionNextCargo();
        this.setClickOkModal();
    },

    handleBack: function () {
        $('.back').off();
        $('body').on('click', '.back', function (event) {
            event.preventDefault();
            if ($('.steps').slick('slickCurrentSlide') > 1) {
                $('.steps').slick('slickGoTo', 1)
            } else {
                window.location.href = "/Home";
            }
        });

        $('body').on('click', '.editar', function (event) {
            event.preventDefault();
            $('.steps').slick('slickGoTo', 1)
            $('#meu-candidato-ideal').data('aparece', true);
        });
    },

    verifyEstadosContinue: function () {
        if ($('select#estado').val() != "") {
            $('#selecioneEstado a.continuar').addClass('active')
        } else {
            $('#selecioneEstado a.continuar').removeClass('active')
        }
    },

    setListEstados: function () {
        $('.triggerEstado, .triggerEstadoClone').on('click', function (event) {

            event.preventDefault();

            $(this).toggleClass('disabled');

            $('#estados').toggleClass('active');
            $('section').toggleClass('blur');

            $('#estados li').removeClass('active');

            if ($('#estado').val() != '') {
                $('#estados li[value=' + $('#estado').val() + ']').addClass('active');
            }

        });

        $('body').on('click', '#estados a.close', function (event) {
            event.preventDefault();
            $('#estados').toggleClass('active');
            $('section').toggleClass('blur');
        });

        $('body #estados').on('click', 'li', function (event) {
            event.preventDefault();

            //$('#estados li').removeClass('active');
            //$(this).toggleClass('active');

            var estado = $(this).attr('value');

            $('#estado, #estadoClone').val(estado);
            $('.set-estado').trigger("click");

            //if ($('.steps').slick('slickCurrentSlide') < 2) {
            //    $('.steps').slick('slickNext')
            //}

            APP.controller.MeuCandidatoIdeal.verifyEstadosContinue();
            APP.controller.MeuCandidatoIdeal.saveEstado(parseInt($(this).attr('data-id')));

        });

        //$('#selecioneEstado').on('click', '.continuar', function(event) {

        //    event.preventDefault();

        //    var aparece = $('#meu-candidato-ideal').data('aparece');
        //    if(!aparece) {
        //        APP.component.Alert.meuCandidatoIdeal();
        //    }

        //    $('.steps').slick('slickGoTo', 2);
        //    $('#perfis .perfis').slick('slickGoTo', 0, true);
        //    $('nav#perfisTabs ul').slick('slickGoTo', 0, true);
        //});

        APP.controller.MeuCandidatoIdeal.verifyEstadosContinue();
    },

    setEstado: function () {
        $('body').on('click', '.set-estado', function (event) {

            event.preventDefault();

            var aparece = $('#meu-candidato-ideal').data('aparece');

            if (!aparece) {
                APP.component.Alert.meuCandidatoIdeal();
            }

            $('.triggerEstado').trigger("click");

        });
    },

    setScrollPerfis: function () {
        $(window).scroll(function () {
            var height = Math.round($('section#meu-candidato-ideal h1').outerHeight() + $('nav#perfisTabs h2').outerHeight() + ($('#meu-candidato-ideal').hasClass('editarPerfil') ? $('nav#perfisTabs .estadoClone').outerHeight() + 50 : 0));
            if ($(window).scrollTop() > height) {
                $('nav#perfisTabs').addClass('fixed')
            } else {
                $('nav#perfisTabs').removeClass('fixed')
            };

        });
    },

    setSlickPosition: function () {
        $('.perfis').slick('setPosition');
        setTimeout(function () { $('.steps').slick('setPosition'); }, 10);
    },

    setActionFiltros: function () {
        $('nav#actionFiltros').on('click', 'a.clean', function (event) {
            event.preventDefault();
            var index = $('#perfis .perfis').slick('slickCurrentSlide');
            $('#perfis .perfis .perfil:eq(' + index + ')').find('input').prop('checked', false);
            $('#perfis .perfis .perfil:eq(' + index + ') .faixaEtaria .wrap')[0].noUiSlider.reset();
            if ($('html').scrollTop() >= 140) {
                $('html, body').animate({
                    scrollTop: 140
                })
            }

            $.ajax({
                type: "DELETE",
                data: { "candidatoIdeal": $('#perfis .perfis .perfil:eq(' + $('#perfis .perfis').slick('slickCurrentSlide') + ')').attr('data-id') },
                dataType: 'json',
                url: "/MeuCandidatoIdeal/Limpar",
                beforeSend: function () {
                    APP.component.Loading.show();
                },
                success: function (result) {
                },
                error: function (result) {
                    // APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro", 'verify-question', 'OK');
                },
                complete: function (result) {
                    APP.component.Loading.hide();
                }
            });
        });

        $('nav#actionFiltros').on('click', 'a.next', function (event) {
            event.preventDefault();
            var total = $('#perfis .perfis').slick('getSlick').slideCount;
            var index = $('#perfis .perfis').slick('slickCurrentSlide');
            // if (index + 1 < total) {
            //     $('#perfis .perfis').slick('slickNext');
            // } else {
            // }

            APP.controller.MeuCandidatoIdeal.savePerfil(true);
            // $('html, body').animate({
            //     scrollTop: 0
            // })
            // $('.steps').slick('slickNext');
        });
    },

    setSlickMain: function (initial) {
        if (initial === 3) {
            $('div#bgBreve, h1').addClass('active');
        }
        $('.steps').slick({
            arrows: false,
            infinite: false,
            adaptiveHeight: true,
            touchMove: false,
            swipe: false,
            initialSlide: initial,
        });

        $('.steps').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            if (nextSlide === 2 && event.target.className.indexOf('steps') >= 0) {
                $('nav#perfisTabs, nav#actionFiltros').addClass('active')
            } else if (nextSlide != 2 && event.target.className.indexOf('steps') >= 0) {
                $('nav#perfisTabs, nav#actionFiltros').removeClass('active')
            }

            if (nextSlide <= 2 && event.target.className.indexOf('steps') >= 0) {
                $('div#bgBreve, div#bgTutorial, h1').removeClass('active');
            } else if (nextSlide > 2 && event.target.className.indexOf('steps') >= 0) {
                $('div#bgBreve, h1').addClass('active');
            }

        });

        $('.steps').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            if (nextSlide === 2 && event.target.className.indexOf('steps') >= 0) {
                $('html, body').animate({
                    scrollTop: 0
                })
                APP.controller.MeuCandidatoIdeal.setSlickPosition();
            }
        });
    },

    setSlickTutorial: function () {
        $('body').on('click', '.comecar', function (event) {
            event.preventDefault();
            $('.steps').slick('slickGoTo', 1);
        });
        $('#tutorial .tutorial').slick({
            arrows: false,
            dots: true,
            infinite: false,
            adaptiveHeight: false,
        });
    },

    setSlickPerfisTabs: function () {
        $('nav#perfisTabs ul').slick({
            arrows: false,
            slidesToShow: 3,
            adaptiveHeight: true,
            centerMode: true,
            variableWidth: true,
            infinite: false,
            mobileFirst: true,
            asNavFor: '#perfis .perfis',
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 6,
                        variableWidth: false,
                        centerMode: false,
                        infinite: false,
                    }
                },
            ],
        });

        $('nav#perfisTabs ul').on('click', 'a', function (event) {
            event.preventDefault();
            $('#perfis .perfis').slick('slickGoTo', $(this).parent().index())
        });
    },

    setSlickPerfis: function () {
        $('#perfis .perfis').slick({
            arrows: false,
            infinite: false,
            adaptiveHeight: true,
            asNavFor: 'nav#perfisTabs ul',
        });

        $('#perfis .perfis').on('afterChange', function (event, slick, currentSlide) {

            if (currentSlide + 1 === $('#perfis .perfis').slick('getSlick').slideCount) {
                // $('nav#actionFiltros a').last().html('Finalizar');
            } else {
                // $('nav#actionFiltros a').last().html('Aplicar');
            }
            APP.controller.MeuCandidatoIdeal.setSlickPosition();
        });

        $('#perfis .perfis').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            APP.controller.MeuCandidatoIdeal.savePerfil();

            if (currentSlide != nextSlide) {
                if ($('html').scrollTop() >= 140) {
                    $('html, body').animate({
                        scrollTop: 140
                    })
                }
            }
        });
    },

    maxLimitPartido: function (element) {
        if (element.find('.partido input:checked').length >= 10) {
            element.find('.partido .labels').addClass('over');
        } else {
            element.find('.partido .labels').removeClass('over');
        }
    },

    setSelecionarPartido: function () {
        var sP = $('#selecionarPartidos');
        $('body').on('click', '.triggerPartido', function (event) {
            event.preventDefault();
            var perfil = $(this).parents('.perfil').attr('id');

            $('section').addClass('blur');

            sP.attr('data-perfil', perfil)
            sP.addClass('active')
            sP.find('div.wrap').scrollTop(0)

            sP.find('input').prop('checked', false);
            if ($(this).next().find('input:checked').length > 0) {
                $(this).next().find('input:checked').each(function (index, el) {
                    var target = $(this).attr('id').split('_')[$(this).attr('id').split('_').length - 1];
                    sP.find('input#p_' + target).prop('checked', true);

                });
            }

            if (sP.find('ul input:checked').length > 0) {
                sP.find('.desc a').addClass('active');
            } else {
                sP.find('.desc a').removeClass('active');
            }
            sP.find('.desc a span.num').html(sP.find('input:checked').length)
        });
        $('body').on('click', '.morePartido', function (event) {
            event.preventDefault();
            $(this).parents('.wrap').find('.triggerPartido').click();
        });

        $('body').on('click', '#selecionarPartidos ul input', function (event) {
            var perfil = sP.attr('data-perfil');
            var target = $(this).attr('id').replace('p_', '');
            var state = $(this).is(':checked');
            var total = sP.find('ul input').length;

            // Check de todos os partidos
            if ($(this).attr('id') === 'p_todos' && state === true) {
                sP.find('ul input').prop('checked', true);
            } else if ($(this).attr('id') === 'p_todos' && state === false) {
                sP.find('ul input').prop('checked', false);
            }

            if (sP.find('ul input:checked').length < total) {
                sP.find('ul input#p_todos').prop('checked', false);
            }

            // Verifica os inputs checked e adiciona a contagem no botão de continuar
            if (sP.find('ul input:not(#p_todos):checked').length > 0) {
                sP.find('.desc a').addClass('active');
            } else {
                sP.find('.desc a').removeClass('active');
            }
            sP.find('.desc a span.num').html(sP.find('ul input:not(#p_todos):checked').length)

        });

        $('body').on('click', '#selecionarPartidos a.close', function (event) {
            event.preventDefault();

            $('section').removeClass('blur');
            sP.removeClass('active');

        });

        $('body').on('click', '#selecionarPartidos a.ok', function (event) {
            event.preventDefault();
            var perfil = sP.attr('data-perfil');

            $('.perfil#' + perfil + ' .partido').find('input').prop('checked', false)

            sP.find('input:checked').each(function (index, el) {
                var target = $(this).attr('id').replace('p_', '');
                $('.perfil#' + perfil).find('input#p_' + perfil + '_' + target).prop('checked', true)
            });

            $('section').removeClass('blur');
            sP.removeClass('active');

            APP.controller.MeuCandidatoIdeal.setSlickPosition();
            APP.controller.MeuCandidatoIdeal.maxLimitPartido($('.perfil#' + perfil));

        });

        $('body').on('change', '#perfis .partido input', function (event) {
            APP.controller.MeuCandidatoIdeal.setSlickPosition();
            APP.controller.MeuCandidatoIdeal.maxLimitPartido($(this).parents('.perfil'));
        });
    },

    getResultadosSeEuFosseUmPolitico: function () {
        $.get('/SeEuFosseUmPolitico/Resultado', function (data) {
            if (data.partidos != null) {
                for (var i = 0; i <= 4; i++) {
                    $('div.partido').find('input[id$="_' + data.partidos[i].sigla + '"]').prop('checked', true)
                }
                //$('#selecionarPartidos .desc p').show();

                $('#selecionarPartidos .desc h1').text('De acordo com suas escolhas na seção “Se eu fosse um político”, esses são os partidos que votaram de forma semelhante a você.');
                $('#selecionarPartidos .desc p').html('Você pode editar esta lista de partidos quando quiser. <br><br> Atualmente existem 35 partidos registrados no Brasil. Pode haver mais de um deles representando determinada ideologia ou forma de enxergar a sociedade. É importante conhecer bem as propostas dos partidos e identificar aqueles que mais se alinham às suas próprias expectativas para o nosso país.');

            }
        });
    },

    getContentPerfis: function () {
        APP.component.Loading.show();
        $.get('/MeuCandidatoIdeal/BuscarParametros', function (data) {
            APP.controller.MeuCandidatoIdeal.setContentPerfis(data);
            APP.component.Loading.hide();
            if (!data.preenchido) {
                APP.controller.MeuCandidatoIdeal.getResultadosSeEuFosseUmPolitico();
            }
        });
    },

    setContentPerfis: function (_data) {
        var removeAcentos = APP.component.RemoveAcentos.setRemoveAcentos;

        filtros = _data;
        

        $.each(_data.estados, function (index, val) {
            $('div#estados ul').append(
                `<li data-id="${val.id}" value="${val.sigla}">${val.sigla} 
                    <span>${val.nome}</span>
                    <span class="option-turno" style="display:block; margin-left: 0; text-transform: uppercase; font-size: 12px;">
                        ${val.segundoTurno ? `2º turno para presidente e governador` : `2º turno para presidente`}
                    </span>
                </li>`
            );
            $('div#selecioneEstado select#estado').append(`<option data-id="${val.id}" ${val.selecionado ? `selected` : ``} value="${val.sigla}">${val.sigla} - ${val.nome}</option>`);
            $('div.estadoClone select#estadoClone').append(`<option data-id="${val.id}" ${val.selecionado ? `selected` : ``} value="${val.sigla}">${val.sigla} - ${val.nome}</option>`);
        });

        APP.controller.MeuCandidatoIdeal.setListEstados();
        APP.controller.MeuCandidatoIdeal.setEstado();

        $.each(_data.candidatos, function (index, val) {
            
            var perfil = val.cargo;
            var perfilSlug = removeAcentos(val.cargo).substring(0, 10).toLowerCase();

            $('nav#perfisTabs ul').append(`<li><a data-perfil="${pascalCase(perfil.toLowerCase())}" href="#${perfilSlug}">${perfil}</a></li>`);
            $('.step#perfis .perfis').append(`<div class="perfil" id="${perfilSlug}" data-id="${val.cargo}"><div class="wrap"></div></div>`);

            var perfilElement = $('.step#perfis .perfis').find('#' + perfilSlug + ' .wrap')


            // ########## Terminologias ##########
            perfilElement.append(`
                <p>As terminologias aqui utilizadas foram baseadas no banco de dados oficial do TSE.</p>
                <p>Se alguma das características abaixo for indiferente para definir o perfil do seu candidato ideal, basta não selecionar. Lembre-se: esta é apenas uma ferramenta para facilitar suas buscas.</p>
            `)


            // ########## Partido ##########
            perfilElement.append(`
            <div class="partido">
                <div class="wrap">
                    <a href="#" class="triggerPartido">Partidos</a>
                    <div class="labels">
                        ${val.partidos.map(e =>
                    `<input data-id="${e.id}" ${e.selecionado ? `checked` : ``} id="p_${perfilSlug}_${e.sigla.replace('/', '')}" type="checkbox">
                            <label for="p_${perfilSlug}_${e.sigla.replace('/', '')}">${e.sigla}</label>`
                ).join('')}
                        <a href="#" class="morePartido">+</a>
                    </div>
            </div>`)

            APP.controller.MeuCandidatoIdeal.maxLimitPartido(perfilElement);

            if ($('#selecionarPartidos').find('ul li').length === 0) {

                $('#selecionarPartidos').find('ul').append(`
                <li class="todos">
                    <input id="p_todos" type="checkbox">
                    <label for="p_todos"><strong>Todos os partidos<strong></label>
                </li>`);

                $('#selecionarPartidos').find('ul').append(`${val.partidos.map(e =>
                    `<li>
                        <input data-id="${e.id}" id="p_${e.sigla.replace('/', '')}" type="checkbox">
                        <label for="p_${e.sigla.replace('/', '')}"><strong class="sigla">${e.sigla}</strong> ${properCase(e.nome.toLowerCase())}</label>
                    </li>`
                ).join('')}`)
            }


            // ########## Faixa Etária ##########
            var dicasFaixaEtaria = val.dicas[0];
            
            var cargoFaixaEtaria = APP.component.RemoveAcentos.init(val.cargo.toLowerCase());
            var bgImgFaixaEtaria = APP.component.RemoveAcentos.init(dicasFaixaEtaria.parametro.toLowerCase().replace('/', ''));

            perfilElement.append(`
            <div class="faixaEtaria">
                <h3>
                    Faixa etária
                    <a href="#" class="info" data-info="faixaEtaria" data-texto='${dicasFaixaEtaria.texto}' data-img="bg-${cargoFaixaEtaria}-${bgImgFaixaEtaria.replace('--', '-')}">
                        <img src="/assets/img/meucandidatoideal/icon-info.svg">
                    </a>
                </h3>
                <div class="wrap"></div>
            </div>`)

            var rangeSlider = perfilElement.find('.faixaEtaria .wrap')[0];
            var faixaEtariaList = val.idade;
            var faixaEtariaListSelecionado = [];

            var range = {};
            $.each(faixaEtariaList, function (index, val) {

                if (val.selecionado) {
                    faixaEtariaListSelecionado.push(index)
                }

                if (faixaEtariaList.length === index + 1) {
                    noUiSlider.create(rangeSlider, {
                        start: [0, faixaEtariaList.length - 1],
                        step: 1,
                        connect: true,
                        margin: 1,
                        pips: {
                            mode: 'steps',
                            density: 1000,
                        },
                        range: {
                            'min': 0,
                            'max': faixaEtariaList.length - 1,
                        },
                    });
                    $.each(faixaEtariaList, function (index, val) {
                        perfilElement.find('.faixaEtaria').find('.noUi-pips .noUi-value:eq(' + index + ')').html(val.nome)
                    });

                    if (faixaEtariaListSelecionado.length > 0) {
                        var faixaEtariaListSelecionadoRange = [parseInt(faixaEtariaListSelecionado[0]), parseInt(faixaEtariaListSelecionado[faixaEtariaListSelecionado.length - 1])];
                        rangeSlider.noUiSlider.set(faixaEtariaListSelecionadoRange);
                    }
                }
            });

            // ########## Sexo ##########
            var dicasSexo = val.dicas[1];
            var cargoSexo = APP.component.RemoveAcentos.init(val.cargo.toLowerCase());
            var bgImgSexo = APP.component.RemoveAcentos.init(dicasSexo.parametro.toLowerCase().replace('/', ''));

            perfilElement.append(`
            <div class="sexo">
                <h3>
                    Sexo
                    <a href="#" class="info" data-info="sexo" data-texto='${dicasSexo.texto}' data-img="bg-${cargoSexo}-${bgImgSexo.replace('--', '-')}">
                        <img src="/assets/img/meucandidatoideal/icon-info.svg">
                    </a>
                </h3>
                <div class="wrap">
                ${val.sexo.map(e =>
                    `<input data-id="${e.id}" ${e.selecionado ? `checked` : ``} id="s_${perfilSlug}_${e.nome.substring(0, 3).toLowerCase()}" type="checkbox" name="s_${perfilSlug}">
                    <label for="s_${perfilSlug}_${e.nome.substring(0, 3).toLowerCase()}"><img src="/assets/img/meucandidatoideal/icon-${e.nome.substring(0, 3).toLowerCase()}.png" />${e.nome}</label>`
                ).join('')}
                </div>
            </div>`)


            // ########## Grau/Instrução ##########
            var dicasGrauInstrucao = val.dicas[2];
            var cargoGrauInstrucao = APP.component.RemoveAcentos.init(val.cargo.toLowerCase());
            var bgImgGrauInstrucao = APP.component.RemoveAcentos.init(dicasGrauInstrucao.parametro.toLowerCase().replace('/', ''));

            perfilElement.append(`
            <div class="grauInstrucao">
                <h3>
                    Grau/Instrução
                    <a href="#" class="info" data-info="grauInstrucao" data-texto='${dicasGrauInstrucao.texto}' data-img="bg-${cargoGrauInstrucao}-${bgImgGrauInstrucao.replace('--', '-')}">
                        <img src="/assets/img/meucandidatoideal/icon-info.svg">
                    </a>
                </h3>
                <div class="wrap">
                    ${val.grauInstrucao.map(e =>
                    `<input data-id="${e.id}" ${e.selecionado ? `checked` : ``} id="gi_${perfilSlug}_${removeAcentos(e.nome).toLowerCase()}" type="checkbox">
                        <label for="gi_${perfilSlug}_${removeAcentos(e.nome).toLowerCase()}">${sentenceCase(e.nome.toLowerCase())}</label>`
                ).join('')}
                </div>
                <small>* No banco de dados do TSE, o grau máximo de instrução de um candidato é "Ensino Superior" (completo ou incompleto). Portanto, caso você tenha escolhido entre as opções "Pós-graduação", "Mestrado", "Doutorado" ou "Pós-doutorado", os candidatos apresentados estarão marcados como "Ensino Superior".</small>
            </div>`)


            // ########## Estado Civil ##########
            perfilElement.append(`
            <div class="estadoCivil">
                <h3>
                    Estado Civil
                </h3>
                <div class="wrap">
                    ${val.estadoCivil.map(e =>
                    `<input data-id="${e.id}" ${e.selecionado ? `checked` : ``} id="ec_${perfilSlug}_${removeAcentos(e.nome).substring(0, 3).toLowerCase()}" type="checkbox">
                        <label for="ec_${perfilSlug}_${removeAcentos(e.nome).substring(0, 3).toLowerCase()}">${sentenceCase(e.nome.toLowerCase())}</label>`
                ).join('')}
                </div>
            </div>`)


            // ########## Cor/Raça ##########
            var dicasCorRaca = val.dicas[3];
            var cargoCorRaca = APP.component.RemoveAcentos.init(val.cargo.toLowerCase());
            var bgImgCorRaca = APP.component.RemoveAcentos.init(dicasCorRaca.parametro.toLowerCase().replace('/', ''));

            perfilElement.append(`
            <div class="corRaca">
                <h3>
                    Cor/Raça
                    <a href="#" class="info" data-info="corRaca" data-texto='${dicasCorRaca.texto}' data-img="bg-${cargoCorRaca}-${bgImgCorRaca.replace('--', '-')}">
                        <img src="/assets/img/meucandidatoideal/icon-info.svg">
                    </a>
                </h3>
                <div class="wrap">
                    ${val.corRaca.map(e =>
                    `<input data-id="${e.id}" ${e.selecionado ? `checked` : ``} id="cr_${perfilSlug}_${removeAcentos(e.nome).substring(0, 3).toLowerCase()}" type="checkbox">
                        <label for="cr_${perfilSlug}_${removeAcentos(e.nome).substring(0, 3).toLowerCase()}">${e.nome}</label>`
                ).join('')}
                </div>
            </div>`)


            // ########## Botão pra replicar os dados entre um Senador e outro ##########
            if (perfilSlug.indexOf('senador') >= 0) {
                perfilElement.append(`<a href="#" class="copySenador">Copiar os dados para o outro Senador</a>`)
            }

            if (index < _data.candidatos.length - 1) {
                perfilElement.append(`<a href="#" class="proximoCargo">Próximo Cargo</a>`)
            }


            if (index === _data.candidatos.length - 1) {
                if (typeof APP.component.Utils.getUrlParameter('cargo') != "undefined" && _data.preenchido) {
                    var cargo = APP.component.Utils.getUrlParameter('cargo');
                    
                    $('#meu-candidato-ideal h1, #bgTutorial, div#bgBreve').css('transition', 'none');
                    $('#meu-candidato-ideal h1, #bgTutorial, div#bgBreve').removeClass('active');
                    $('#perfisTabs, #actionFiltros').addClass('active');
                    $('#meu-candidato-ideal').addClass('editarPerfil');

                } else {
                    var cargo = '';
                }

                APP.controller.MeuCandidatoIdeal.verifyEstadosContinue();
                APP.controller.MeuCandidatoIdeal.setSlickTutorial();
                APP.controller.MeuCandidatoIdeal.setSlickMain((cargo.length > 0 && _data.preenchido ? 2 : (_data.preenchido ? 3 : 0)));
                APP.controller.MeuCandidatoIdeal.setSlickPerfis();
                APP.controller.MeuCandidatoIdeal.setSlickPerfisTabs();

                if (typeof APP.component.Utils.getUrlParameter('cargo') != "undefined") {
                    $('a[data-perfil="' + cargo + '"]').click()
                }

                $('section#meu-candidato-ideal').addClass('loaded');
                APP.component.Loading.hide();

                // if (_data.preenchido) {
                //     APP.controller.MeuCandidatoIdeal.getResultadosSeEuFosseUmPolitico();
                // }
            }

            // ########## Modal Meu Candidato ##########
            $('#meu-candidato-ideal').data('aparece', _data.preenchido);
        });

    },

    setActionCopySenador: function () {
        $('body').on('click', '.copySenador', function (event) {
            event.preventDefault();

            var current = $(this).parents('.perfil');
            var sibling = current.siblings('[id*="senador"]')

            sibling.find('input').prop('checked', false);
            sibling.find('.faixaEtaria .wrap')[0].noUiSlider.reset();

            // Partidos
            current.find('.partido input:checked').each(function (index, el) {
                sibling.find('.partido input[data-id="' + $(this).attr('data-id') + '"]').prop('checked', true);
            });

            // Faixa Etária
            var faixaEtariaSelected = current.find('.faixaEtaria .wrap')[0].noUiSlider.get();
            sibling.find('.faixaEtaria .wrap')[0].noUiSlider.set(faixaEtariaSelected);

            // Sexo
            current.find('.sexo input:checked').each(function (index, el) {
                sibling.find('.sexo input[data-id="' + $(this).attr('data-id') + '"]').prop('checked', true);
            });

            // Grau/Instrução
            current.find('.grauInstrucao input:checked').each(function (index, el) {
                sibling.find('.grauInstrucao input[data-id="' + $(this).attr('data-id') + '"]').prop('checked', true);
            });

            // Estado Civil
            current.find('.estadoCivil input:checked').each(function (index, el) {
                sibling.find('.estadoCivil input[data-id="' + $(this).attr('data-id') + '"]').prop('checked', true);
            });

            // Cor/Raça
            current.find('.corRaca input:checked').each(function (index, el) {
                sibling.find('.corRaca input[data-id="' + $(this).attr('data-id') + '"]').prop('checked', true);
            });

            $('#perfis .perfis').slick('slickGoTo', sibling.index());

        });
    },

    setActionNextCargo: function () {
        $('body').on('click', '.proximoCargo', function (event) {
            $('#perfis .perfis').slick('slickNext');
        });
    },

    setActionInfos: function () {
        $('body').on('click', '.info', function (event) {
            event.preventDefault();

            var text = $(this).data('texto');
            var img = $(this).data('img');

            APP.component.Alert.modalInformacoes('curiosidade', text, '/assets/img/meucandidatoideal/' + img + '.png', 'meu-candidato')
        });
    },

    saveEstado: function (_data) {
        APP.component.Loading.show();
        $.post('/MeuCandidatoIdeal/SalvarEstado', { "estado": _data }, function (data, textStatus, xhr) {
            APP.component.Loading.hide();
        });
    },

    savePerfil: function (proximaFase) {
        var current = $('#perfis .perfis .perfil:eq(' + $('#perfis .perfis').slick('slickCurrentSlide') + ')');
        var _data = {
            "cargo": current.attr('data-id'),
            "corRaca": [],
            "estadoCivil": [],
            "grauInstrucao": [],
            "idade": [],
            "partidos": [],
            "sexo": []
        }

        // Partidos
        current.find('.partido input:checked').each(function (index, el) {
            _data.partidos.push($(this).attr('data-id'));
        });

        // Faixa Etária
        var faixaEtariaSelected = current.find('.faixaEtaria .wrap')[0].noUiSlider.get();
        $.each(filtros.candidatos[current.index()].idade, function (index, val) {
            var cur = index;
            var min = parseInt(faixaEtariaSelected[0]);
            var max = parseInt(faixaEtariaSelected[1]);

            if (cur >= min && cur <= max) {
                _data.idade.push(val.id);
            }
        });

        // Sexo
        current.find('.sexo input:checked').each(function (index, el) {
            _data.sexo.push(parseInt($(this).attr('data-id')));
        });

        // Grau/Instrução
        current.find('.grauInstrucao input:checked').each(function (index, el) {
            _data.grauInstrucao.push(parseInt($(this).attr('data-id')));
        });

        // Estado Civil
        current.find('.estadoCivil input:checked').each(function (index, el) {
            _data.estadoCivil.push(parseInt($(this).attr('data-id')));
        });

        // Cor/Raça
        current.find('.corRaca input:checked').each(function (index, el) {
            _data.corRaca.push(parseInt($(this).attr('data-id')));
        });

        $.ajax({
            type: "POST",
            data: _data,
            dataType: 'json',
            url: "/MeuCandidatoIdeal/Salvar",
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
            },
            error: function (result) {
                // APP.component.Alert.customAlert(result.responseJSON.mensagem, "Erro", 'verify-question', 'OK');

            },
            complete: function (result) {
                APP.component.Loading.hide();

                if (proximaFase &&
                    typeof localStorage.porcentagemMP != "undefined" &&
                    typeof JSON.parse(localStorage.porcentagemMP)[user] != "undefined" &&
                    JSON.parse(localStorage.porcentagemMP)[user].indexOf('meucandidatoideal') < 0 ||
                    proximaFase &&
                    typeof localStorage.porcentagemMP === "undefined") {

                   

                    APP.component.LocalStorage.setPorcentagemMinhasPrioridades('meucandidatoideal');
                    setTimeout(function () {
                        window.location = '/MeusCandidatos/Candidatos2018/Presidente';
                    }, 10);

                } else if (proximaFase &&
                    typeof localStorage.porcentagemMP != "undefined" &&
                    typeof JSON.parse(localStorage.porcentagemMP)[user] != "undefined" &&
                    JSON.parse(localStorage.porcentagemMP)[user].indexOf('meucandidatoideal') >= 0) {

                    

                    APP.component.LocalStorage.setPorcentagemMinhasPrioridades('meucandidatoideal');
                    setTimeout(function () {
                        
                        window.location = '/MeusCandidatos/Candidatos2018/' + pascalCase($('nav#perfisTabs .slick-current a').html().toLowerCase());
                    }, 10);
                }

            }
        });
    },

    setClickOkModal: function () {

        $('.aceito').on('click', function (event) {

            event.preventDefault();
            if ($('.modal:visible').length > 1) {
                $('.modal#meu-candidato-ideal, #mask').fadeOut();
            } else {
                $('.modal, #mask').fadeOut();
            }

        });

    },

};
