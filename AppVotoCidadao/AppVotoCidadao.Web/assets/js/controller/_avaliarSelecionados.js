/*
|--------------------------------------------------------------------------
| Controller 
|--------------------------------------------------------------------------
*/

var cmo = $('section#avaliar-selecionados');
var temas = {};

APP.controller.AvaliarSelecionados = {

    init: function () {

        this.setup();
        this.avaliarSelecionados();

    }, 

    setup : function () {

        //

    },

    avaliarSelecionados : function () {

        this.getUserTerminouEtapa();
        this.scrollCandidato();
        this.scrollableHeaders();
        this.setClickVerResultados();
        this.setClickDadosCandidato();
        this.buscarResultadoCandidatos();

    },

    //Modal Bottom
    getUserTerminouEtapa : function () {
        APP.controller.AvaliarSelecionados.setUserTerminouEtapa(user)
    },

    setUserTerminouEtapa : function (user) {

        var hasEtapa = false;

        if (typeof localStorage.porcentagemMP != "undefined" && typeof JSON.parse(localStorage.porcentagemMP)[user] != "undefined") {

            var etapas = JSON.parse(localStorage.porcentagemMP)[user];
            $(etapas).each(function () {
                if (this == 'oqueeuvalorizo') {
                    hasEtapa = true;
                }
            })

        }

        //Verifica se finalizou "O que eu valorizo"
        if(hasEtapa == true) {
            APP.controller.AvaliarSelecionados.getListaTemas();
            $('#modal-bottom').addClass('active')
        } else {
            APP.component.Alert.alertaOQueEuValorizo();
            APP.controller.AvaliarSelecionados.setAlertaOQueEuValorizo();
        }

    },

    getListaTemas : function () {

        var erro = "";
       
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: "/AvaliarSelecionados/BuscarTemasSelecionados",
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.AvaliarSelecionados.setTemasOQueEuValorizo(result);
            },
            error: function (result) {
                APP.component.Alert.customAlert("Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        }).promise().done(function(result) {
            APP.controller.AvaliarSelecionados.bind();
        });

    },

    setTemasOQueEuValorizo : function (result) {

        temas = result;
       
        $('#modal-bottom').html('');
        $('#modal-bottom').html(`
            <div class="wrap">
                <p>
                    Aqui estão os temas que você escolheu na etapa <strong>“O que eu valorizo”</strong>.
                </p>
                <ul>
                    ${result.map( t =>
                        `<li>
                            <span class="nome">${t.tema}</span>
                            <span class="porcentagem">${APP.controller.OQueEuValorizo.setChangePorcentToText(t.porcentagem)}</span>
                        </li>`
                    ).join('')}
                </ul>
                <p>
                    É hora de avaliar os candidatos selecionados e decidir seu voto com base nesses temas prioritários.
                </p>
                <a href="#" class="btn avancar">Avançar</a>
                <a href="#" class="btn alterar-temas">Alterar temas <br>"O que eu valorizo"</a>
            </div>
        `);

    },

    setAlertaOQueEuValorizo : function () {

        $('#go-to-o-que-eu-valorizo').find('.btn').attr('href', '/OQueEuValorizo?came='+page+'&cargo='+cargo)

    },

    buscarResultadoCandidatos: function() {
        APP.controller.Candidatos2018.buscaResultadoGovernador();
    },

    /*
    |--------------------------------------------------------------------------
    | Temas 
    |--------------------------------------------------------------------------
    */
    getCandidatosSelecionados : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            data: {cargo},
            url: `/SegundoTurno/BuscarCandidatos?cargo=${cargo}`,
            beforeSend: function () {
                APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.AvaliarSelecionados.setCandidatosSelecionados(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide();
            }
        }).promise().done(function(result) {
            APP.controller.AvaliarSelecionados.getRespostasUsuario();
        });

    },

    setCandidatosSelecionados : function (result) {

        $('.lista-candidatos').html(`
            ${result.candidatos.map((c,index) =>
                `
                <div class="item" data-candidato="${c.id}" data-afinidade="${c.avaliado ? c.afinidade : 'naoavaliado'}">
                    <div class="info-candidato">
                        <div class="photo"><span style="background-image: url(${c.foto})"></span></div>
                        <div class="txt">
                            <span class="titulo">Candidato</span><div class="candidato">
                            <span class="nome">${c.nome.toLowerCase()}</span>
                            <span class="numero">${c.numero}</span></div>
                            <div class="partido">
                                <span class="sigla">${c.sigla}</span>
                                <span class="nome-partido">(${c.partido})</span>
                            </div>
                        </div>
                    </div>
                    <div class="wrap">
                        ${temas.map((i, index) =>
                            `
                            <div class="tema" data-tema="${i.id}">
                                <div class="informacoes">
                                    <div class="box">
                                        <span class="title">Tema ${index+1}</span>
                                        <span class="nome-tema">${i.tema}</span>
                                        <span class="rev">
                                            Relevância pra mim: 
                                            <span class="porcentagem" data-porcentagem="${i.porcentagem}">${APP.controller.OQueEuValorizo.setChangePorcentToText(i.porcentagem)}</span>
                                        </span>
                                    </div>
                                </div>
                                ${Array(6).join(0).split(0).map((item, e) => `
                                    <input type="radio" id="candidato-${c.id}-tema-${i.id}-nota-${e}" name="candidato-${c.id}-tema-${i.id}" value="${e}"/>
                                    <label for="candidato-${c.id}-tema-${i.id}-nota-${e}">${e}</label>

                                `).join('')}
                            </div>
                            `                
                        ).join('')}
                    </div>
                </div>
                `                
            ).join('')}
        `);
        
    },

    getRespostasTemas : function () {

        var candidatos = $('.lista-candidatos .item')

        objAvaliarSelecionados = {};
        objAvaliarSelecionados.candidato = [];

        $(candidatos).each(function (e) {

            var index = e;

            var id = $(this).data('candidato');
            var listaTemas = $(this).find('.wrap .tema')

            objAvaliarSelecionados.candidato.push({
                id: id,
                temas: [],
            });

            $(listaTemas).each(function (w) {

                var idTema = $(this).data('tema');
                var porcentagem = $(this).find('.porcentagem').data('porcentagem');
                var resposta = parseInt($(this).find('input[type=radio]:checked').val());

                objAvaliarSelecionados.candidato[index].temas.push({
                    id : idTema,
                    porcentagem : porcentagem,
                    resposta : resposta,
                })

            })

        })

        return objAvaliarSelecionados;

    },

    getRespostasUsuario : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            data: {cargo},
            url: '/AvaliarSelecionados/BuscarMinhasOpcoes',
            beforeSend: function () {
                //APP.component.Loading.show();
            },
            success: function (result) {
                APP.controller.AvaliarSelecionados.setRespostasUsuario(result.opcoes)
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                //APP.component.Loading.hide();
            }
        }).promise().done(function(result) {
            APP.controller.AvaliarSelecionados.bind();
        });

    },

    setRespostasUsuario : function (respostas) {

        var lista = $('.lista-candidatos');
        $(respostas).each(function (e) {
            
            var temas = lista.find("[data-candidato='" + this.idCandidato + "']");
            $(this.temas).each(function (i) {
                temas.find("[data-tema='" + this.idTema + "']").find("input[value='"+ this.resposta +"']").prop("checked",true);
            })

        });

        if ($('.lista-candidatos .tema').length === $('.lista-candidatos .tema input:checked').length) {
            $('.resultados').addClass('active');
        }

    },

    /*
    |--------------------------------------------------------------------------
    | INTERACOES 
    |--------------------------------------------------------------------------
    */
    scrollCandidato : function () {
        $(window).scroll(function(event) {
            $('.lista-candidatos .item').each(function() {
                if ($(window).scrollTop() > $(this).offset().top - 145) {
                    $(this).find('div.info-candidato').addClass('fixed');    
                } else {
                    $(this).find('div.info-candidato').removeClass('fixed');    
                }
            })
        });
    },

    scrollableHeaders : function () {
        $('body > header').addClass('scrollable');

        var lastScrollTop = 0;
        $(window).on('scroll', function() {
            st = $(this).scrollTop();

            // Selecionar Cargo
            var iC = $('.info-candidato');
            if (st > 120) {
                if(st < lastScrollTop) {
                    iC.addClass('scroll');
                }
                else if ((st - lastScrollTop) < 50) {
                    iC.removeClass('scroll');
                }
            } else if (st <= 60) {
                iC.removeClass('scroll');
            }

            lastScrollTop = st;
        });
    },

    setClickDadosCandidato : function () {
        $('body').on('click', '.info-candidato', function(event) {
            event.preventDefault();

            var idCandidato = $(this).parent().data('candidato');
            var afinidade = $(this).parent().data('afinidade');
            var selecionado = '';

            /* Act on the event */
            $('#candidatoAfinidade').scrollTop(0);
            $('body').addClass('lockScroll');
            $('#candidatoAfinidade').addClass('active');
            $("section#avaliar-selecionados").addClass('pushLeft');
            
            APP.controller.Candidatos2018.buscarInfosCandidato(idCandidato, afinidade, selecionado, cargo);
        });

        $('body').on('click', '#candidatoAfinidade .voltar, #candidatoAfinidade .close', function(event) {
            event.preventDefault();
            $('body').removeClass('lockScroll');
            $('#candidatoAfinidade').removeClass('active');
            $("section#avaliar-selecionados").removeClass('pushLeft');            
        });
        
    }, 

    setClickAvancarTemas : function () {

        $('#modal-bottom .avancar').on('click', function (event) {

            event.preventDefault();

            $('#modal-bottom').removeClass('active');
            APP.controller.AvaliarSelecionados.getCandidatosSelecionados();
            

        });

    },

    setClickRefazerTemas : function () {

        $('.alterar-temas').on('click', function (event) {

            event.preventDefault();

            var title = 'Atenção';
            var msg = 'Ao redefinir os temas mais importantes para você, os resultados de algumas atividades serão apagados (exceto os candidatos já enviados para a seção <strong>"Meu Voto"</strong>).';

            var htmlForm = `
                <a href="#" class="btn continuarRefazerTemas">
                    <span>CONTINUAR</span>
                </a>
                <a href="#" class="btn close">
                    <span>CANCELAR</span>
                </a>
            `;

            APP.component.Alert.customGuiaDoVoto(title, msg, htmlForm, false, false, '/assets/img/avaliar-selecionados/bg-alerta-alterartemas.png');

            $('body').off('click', '.continuarRefazerTemas');
            $('body').on('click', '.continuarRefazerTemas', function(event) {
                event.preventDefault();
                var url = '/OQueEuValorizo?came='+page+'&cargo='+cargo;
                APP.controller.OQueEuValorizo.sendRefazerTemas(url);
            });

        });

    },

    setClickResposta : function () {

        $('input[id^=candidato-]').on('change', function (){

            var resp = {
                cargo: cargo,
                idCandidato: $(this).closest('.item').data('candidato'),
                idTema: $(this).closest('.tema').data('tema'),
                porcentagem: $(this).closest('.tema').find('.porcentagem').data('porcentagem'),
                resposta: parseInt($(this).val()),

            }

            APP.controller.AvaliarSelecionados.saveResposta(resp);

            if ($('.tema input:checked').length < $('.tema').length) {
                $('.resultados').removeClass('active');
            } else {
                $('.resultados').addClass('active');
            }

        });

    },

    setClickVerResultados : function () {

        $('.resultados').on('click', function (event) {

            event.preventDefault();

            var validate = $('.tema input:checked').length < $('.tema').length;

            if (validate) {
                APP.controller.AvaliarSelecionados.setVoltarInputFaltando();
            } else {
                //var respostas = APP.controller.AvaliarSelecionados.getRespostasTemas();
                APP.controller.MeusCandidatos.saveCargoEtapa(cargo, 3, '/MeusCandidatos/GrauDeAfinidade/'+cargo+'');
            }

        });

    },

    setVoltarInputFaltando : function () {
        
        $('.tema').each(function (e) {

            if (!$(this).find('input').is(':checked')) {

                if ($(window).width() < 600) {
                    var altura = $(this).offset().top - 140;
                } else {
                    var altura = $(this).offset().top - 70;
                }
                $('html, body').animate({
                    scrollTop: altura, 
                }, 1000, function () {
                    APP.component.Alert.alert('Favor preencher todas as respostas.', 'Atenção!')
                });
                
                return false;
            }
        })

    },

    bind : function () {

        APP.controller.AvaliarSelecionados.setClickAvancarTemas();
        APP.controller.AvaliarSelecionados.setClickRefazerTemas();
        APP.controller.AvaliarSelecionados.setClickResposta();

    },

    /*
    |--------------------------------------------------------------------------
    | SAVEs 
    |--------------------------------------------------------------------------
    */
    saveResposta : function (resposta) {

        $.ajax({
            type: "POST",
            dataType: 'json',
            data: resposta,
            url: '/AvaliarSelecionados/SalvarMinhasOpcoes',
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.component.Loading.hide()
            },
            error: function (result) {
            },
            complete: function (result) {
            }
        })

    },

};