/*
|--------------------------------------------------------------------------
| Controller Sobre
|--------------------------------------------------------------------------
*/

APP.controller.Dicas = {

    init : function () {

        this.setup();
        this.dicas();

    }, 

    setup : function () {

        //

    },

    dicas : function () { 

        this.setFiltro();
        this.setProgressDate();
        this.setContagem();
        this.setLerMais();

        this.getDicasCuriosidades();

    },

    //Date
    setProgressDate : function () {

        var fullDate = new Date();
        var dia = fullDate.getDate();
        var mes = fullDate.getMonth() + 1;
        var ano = fullDate.getFullYear();

        var data = new Date(ano, mes, 0);
        var diasMes = data.getDate();
        var porcentagemMes = (100 * dia) / diasMes;

        $('.progresso .bar li').each(function () {

            var liMes = parseInt($(this).data('mes'));

            if (liMes < mes) {
                $(this).find('span').addClass('active');
                $(this).find('span').css('width', '100%');
            } else if (liMes === mes) {
                $(this).find('span').addClass('active');
                $(this).find('span').css('width', porcentagemMes + '%');
            }

        });

    },

    setContagem : function () {

        var target_date = new Date("october 07, 2018").getTime();
        //var dias, horas, minutos, segundos;
        var dias;
        var regressiva = document.getElementById("regressiva");

            var current_date = new Date().getTime();
            var segundos_f = (target_date - current_date) / 1000;
            dias = parseInt((segundos_f / 86400) + 1);
            document.getElementById('dia').innerHTML = dias;

            //Verifica o Dia para inserir imagem
            if (dias == 100 || dias == 90 || dias == 80 || dias == 60 || dias == 15 || dias == 1) {
                APP.controller.Dicas.setImagemContagem(dias);
            }

            

    },

    setImagemContagem : function (dias) {
        
        //Verifica o Dia para inserir imagem
        var img = '<img src="/assets/img/dicas/bg-timeline-'+dias+'.png" alt="'+dias+' Dias">'
        $('.timeline-img').addClass('timeline-'+dias);
        $('.timeline-img').addClass('timeline-active');
        $('.timeline-img').append(img);

    },

    setFiltro : function () {
        
        $('.filtros input:checkbox').on('change', function () {

            var tipo = $(this).val();

            if ($(this).is(':checked')) {
                $('.item[data-tipo='+tipo+']').each(function () {
                    $(this).show();
                });
            } else {
                $('.item[data-tipo='+tipo+']').each(function () {
                    $(this).hide();
                });
            }
        })

    },

    setLerMais : function () {     

        $('.txt > span').on('click', function () {
            
            $(this).closest('.txt').find('p').toggleClass('active');

            if ($(this).closest('.txt').find('p:first-of-type').hasClass('active')) {
                $(this).text('Fechar');
            } else {
                $(this).text('Ler mais');
            }
        })
    },

    getDicasCuriosidades : function () {

        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/Dicas/buscaDicas',
            beforeSend: function () {
                APP.component.Loading.show()
            },
            success: function (result) {
                APP.controller.Dicas.setInfosDicas(result);
            },
            error: function (result) {
                //APP.component.Alert.customAlert(result.statusText, "Erro");
            },
            complete: function (result) {
                APP.component.Loading.hide()
            }
        });

    },

    setInfosDicas : function (dicas) {

        var html = '';

        $(dicas).each(function () {
            html += '<div class="item" data-tipo="'+this.categoria.toLowerCase()+'">';
            html +=     '<img src="/assets/img/icons/icon-'+this.categoria.toLowerCase()+'.png" />';
            html +=     '<div class="txt">';
            html +=         '<p>';
            html +=             this.texto;
            html +=         '</p>';
            html +=         '<span>Ler mais</span>';
            html +=     '</div>';
            html += '</div>';
        });

        $('.dica-curiosidade').append(html);

        APP.controller.Dicas.putLerMais();
        APP.controller.Dicas.setLerMais();

    },
     
    putLerMais : function () {

        $('.dica-curiosidade .item').each(function (){
           
            var p = $(this).find('p').outerHeight();
            
            if (p > 100) {
                $(this).find('p').css('max-height','100px')
                $(this).find('span').css('display','block');
            }
            
        });

    },

};