/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
*/

APP.controller.MinhasPrioridades = {

    init : function () {

        this.setup();
        this.minhasPrioridades();

    }, 

    setup : function () {

        //

    },

    minhasPrioridades : function () { 

        this.setInfoPrioridades();
        this.setTermosCondicoes();
        this.setDadosNaoRegistrados();

    },

    setSlickPrioridades : function () {

        var configPrioridades = {
            dots: false,
            infinite: true,
            slidesToShow: 3,
            arrows: false,
            fade: false,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        centerMode: false,
                        fade: false,
                        infinite: false,
                        dots: true,
                        centerPadding: '20px',
                    }
                },
            ]
        }
        APP.component.Slick.init('minhas-prioridades', configPrioridades);

    },

    setInfoPrioridades : function () {

        var login = APP.component.Utils.getLogin();
        if (login) {
            this.setProgressoPrioridades();
        } else {
            this.setInfoDegustacao();
        }

    },

    setProgressoPrioridades : function () {

        if (typeof localStorage.porcentagemMP != "undefined" && typeof JSON.parse(localStorage.porcentagemMP)[user] != "undefined") {
            var qnt = JSON.parse(localStorage.porcentagemMP)[user].length;
            if (qnt >= 2) {
                qnt = 3;
            }
            var porcentagemPrioridades = ((100 * qnt) / 3).toFixed(0);
            $('.prioridades .etapa .info .progresso .bar .prog').css('width', porcentagemPrioridades+'%');
            $('.prioridades .etapa .info .progresso .porcentagem').text(porcentagemPrioridades + '%');
        } else {
            $('.prioridades .etapa .info .progresso .porcentagem').text('0%');
        }

    },

    setInfoDegustacao : function () {

         $('.minhas-prioridades .mp-wrap:nth-of-type(2) .item a img').attr('src','/assets/img/home/cover-prioridades-valorizo-pb.png');
        $('.minhas-prioridades .mp-wrap:nth-of-type(2) .item a').attr('href','javascript:;');
        $('body').on('click', '.minhas-prioridades .mp-wrap:nth-of-type(2) .item a, .minhas-prioridades .mp-wrap:nth-of-type(3) .item a', function(event) {
            event.preventDefault();
            APP.component.Alert.customAlert("Para acessar esta seção é necessário efetuar cadastro ou entrar com uma conta válida.", "Atenção!");
        });

    },

    // MODAL
    setDadosNaoRegistrados : function () {

        $('#nao-registrados').on("click", ".leu", function(event) {
            event.preventDefault();
            $('.modal, #mask').fadeOut();
            $('body').removeClass('lockScroll');

            localStorage.setItem('LeuDadosNaoRegistrados', true);
        });

    },

    setTermosCondicoes : function () {

        $('.termos').on('click', function(event) {

            event.preventDefault();
            
            var title = 'TERMOS E POLÍTCAS';

            APP.component.Alert.customTermos(title, '');

        });

    },


};