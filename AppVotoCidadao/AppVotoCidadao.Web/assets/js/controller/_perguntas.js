/*
|--------------------------------------------------------------------------
| Controller Perguntas Frequentes
|--------------------------------------------------------------------------
*/

APP.controller.PerguntasFrequentes = {

    init : function () {

        this.setup();
        this.perguntasFrequentes();
        this.setTermos();

    }, 

    setup : function () {

        //

    },

    perguntasFrequentes : function () {

        this.setAccordion();

    },

    setAccordion : function () {

        $('.perguntas-respostas .pergunta').click(function (event) {

            event.preventDefault();

            $a = $(this);
            $(this).toggleClass('active');
            var position = $(this).offset(),
                top = Math.round(position.top = position.top - 70) + 'px',
                left = Math.round(position.left) + 'px';

            $(this).parent().find('div.resposta').slideToggle(0, function(){
                if ($a.hasClass('active')) {
                    $('html, body').animate({
                        scrollTop: position.top
                    }, 500);

                }
            });

        });

    },

    setTermos : function () {
        $('body').on('click', '.perguntas-respostas .termos', function(event) {
            event.preventDefault();
            $('.modal-termos').fadeIn();
        });
    }

};
